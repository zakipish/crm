const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => ({
	entry: './src/index.tsx',
	devtool: 'source-map',
	resolve: {
		modules: [
			path.resolve(__dirname, 'src'),
			'node_modules'
		],
		extensions: ['.ts', '.tsx', '.js', '.json', '.mjs']
	},
	target: 'web',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'index.js'
	},
	externals: [],
	module: {
		rules: [
			{
				test: /\.(scss|sass)$/,
				use: ['style-loader', 'css-loader', 'sass-loader']
			},
			{
				test: /\.tsx?$/,
				loader: 'awesome-typescript-loader'
			},
			{
				enforce: 'pre',
				test: /\.js$/,
				loader: 'source-map-loader',
				exclude: [/node_modules/, /build/, /__test__/]
			},
			{
				test: /\.mjs$/,
				include: /node_modules/,
				type: 'javascript/auto'
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'fonts/'
					}
				}]
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'assets/'
					}
				}]
			}
		]
	},
	node: {
		fs: 'empty',
		net: 'empty'
	},
	plugins: [
		// new webpack.WatchIgnorePlugin([
		//   /css\.d\.ts$/
		// ]),
		new HtmlWebpackPlugin({
			template: './src/index.html' // Specify the HTML template to use
		}),
		new Dotenv({
			path: path.resolve(__dirname, argv.mode === 'production' ? '.env.production' : '.env'),
		}),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, 'src/assets/'),
            to: path.resolve(__dirname, 'dist/assets'),
        }]),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, 'src/assets/favicons'),
            to: path.resolve(__dirname, 'dist'),
        }])

	],
	devServer: {
		// publicPath: '/',
		// contentBase: path.join(__dirname, 'dist'),
		// historyApiFallback: true,
		// watchContentBase: true,
		compress: true,
		disableHostCheck: true,
		port: 8082
	}
});
