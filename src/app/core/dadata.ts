import Config from './config'

export const findCompany = (query: string) => {
    return request(
        'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
        { query }
    )
}

export const findAddress = (query: string) => {
    return request(
        'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
        { query, count: 10 }
    )
}

export const findBank = (query: string) => {
    return request(
        'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/bank',
        { query, count: 10 }
    )
}

const request = async (uri: string, params?: {}) => {
    uri = uri + (params ? (uri.indexOf('?') === -1 ? '?' : '&') + getQueryString(params) : '');

    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Token ${Config.dadata_api_key}`
    }

    const response = await fetch(uri, {
        headers: headers,
        method: 'GET',
    })

    const json = await response.json()

    if (response.status < 200 || response.status >= 300) return []
    return json.suggestions
}

const getQueryString = (params: {}) => {
    return Object
    .keys(params)
    .map(k => {
        if (Array.isArray(params[k])) {
            return params[k]
            .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
            .join('&')
        }

        return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
    })
    .join('&')
}
