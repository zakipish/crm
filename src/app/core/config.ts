let config = {
    front_domain: process.env.FRONT_DOMAIN,
    api_host: process.env.API_HOST,
    api_url: process.env.API_URL,
    dadata_api_key: process.env.DADATA_API_KEY,
    yk_shopid: process.env.YK_SHOPID,
    yk_scid: process.env.YK_SCID
}

export const setConfig = (key, value) => {
    config[key] = value
}

export default config
