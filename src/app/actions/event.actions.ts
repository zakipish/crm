import types from './_types.actions'

export const loadEvents = (data) => {
    return {
        type: types.EVENTS_LOAD,
        payload: data
    }
}
