import types from './_types.actions'

export function setReqParam(reqParam) {
    return {
        type: types.PLANNER_SET_REQ_PARAM,
        playload: {reqParam}
    }
}

export function setDate(date) {
    return {
        type: types.PLANNER_SET_DATE,
        playload: {date}
    }
}

export function setView(view) {
    return {
        type: types.PLANNER_SET_VIEW,
        playload: {view},
    }
}

export function setLayer(layerKey) {
    return {
        type: types.PLANNER_SET_LAYER,
        playload: {layerKey},
    }
}

export function setGrid(grid) {
    return {
        type: types.PLANNER_SET_GRID,
        playload: {grid},
    }
}

export function setPrices(prices) {
    return {
        type: types.PLANNER_SET_PRICES,
        playload: {prices},
    }
}

export function setGames(games) {
    return {
        type: types.PLANNER_SET_GAMES,
        playload: {games},
    }
}

export function setShowSettingDay(showSettingDay) {
    return {
        type: types.PLANNER_SET_SHOW_SETTING_DAY,
        playload: {showSettingDay},
    }
}

export function setPriceLimit(priceLimit) {
    return {
        type: types.PLANNER_SET_PRICELIMIT,
        playload: {priceLimit},
    }
}
