import types from './_types.actions'

export function clubGet(data) {
    return {
        type: types.CLUB_GET,
        payload: data
    }
}
