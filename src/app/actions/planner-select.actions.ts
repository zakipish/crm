import types from './_types.actions'

export function show() {
    return { type: types.PLANNER_SELECT_SHOW }
}

export function hide() {
    return { type: types.PLANNER_SELECT_HIDE }
}

export function startMove() {
    return { type: types.PLANNER_SELECT_STARTMOVE }
}

export function endMove(endCrood) {
    return { 
        type: types.PLANNER_SELECT_ENDMOVE,
        playload: {endCrood},
    }
}

export function setCrood(crood) {
    return { 
        type: types.PLANNER_SELECT_SET_CROOD,
        playload: {crood},
    }
}

export function setSelectList(list) {
    return { 
        type: types.PLANNER_SELECT_SET_LIST,
        playload: {list},
    }
}

export function setSelectGame(game) {
    return { 
        type: types.PLANNER_SELECT_GAME,
        playload: {game},
    }
}
