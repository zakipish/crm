import * as React from 'react'
import iconSocData from './icon-soc.data'
import iconUxData from './icon-ux.data'
import iconPointersData from './icon-pointers.data'
import iconActionsData from './icon-actions.data'

const data = {
    ...iconSocData,
    ...iconUxData,
    ...iconPointersData,
    ...iconActionsData,
}

export interface IIcon {
    icon: string | IconList
    width?: string
    height?: string
    stroke?: string
    fill?: string
    style?: object
}

class IconComponent extends React.Component<IIcon> {

    getProps(item) {
        let { icon, width, height, stroke, fill, style } = this.props
        
        width = width || item.width || '0px'
        height = height || item.height || '0px'

        stroke = stroke || item.stroke || 'none' 
        fill = fill || item.fill || 'none'
        style = style || item.style || {}
        style = { transition: '0.3s', ...style }
        
        return {
            svgProp: {  width, height },
            pathProp: { style, fill, stroke },
        }
    }

    render() {
        let { icon } = this.props
        let item = data[icon] || {}
        let { viewBox, paths, transform } = item
        let { svgProp, pathProp } = this.getProps(item)
        return (
            <svg viewBox={viewBox} {...svgProp}>
                <g transform={transform}>
                    { paths ? paths.map((el, i) => { return(
                        <path key={i} d={el} {...pathProp} />
                    )}) :  ''}
                </g>
            </svg>
        )
    }
}

enum IconList {
//actions
    ok = 'ok',
    doubleOk = 'doubleOk',
    search = 'search',
    plus = 'plus',
    plusCir = 'plusCir',
    court = 'court',
    purse = 'purse',
    send = 'send',
    logout = 'logout',
    del = 'del',
//pointers
    selectArr = 'selectArr',
    slideLeft = 'slideLeft',
    slideRight = 'slideRight',
    treeArr = 'treeArr',
    dpArrLeft = 'dpArrLeft',
    dpArrRight = 'dpArrRight',
//soc
    instagram = 'instagram',
    vkontakte = 'vkontakte',
    facebook = 'facebook',
    mailru ='mailru',
    odnoklassniki ='odnoklassniki',
    sqFacebook = 'sqFacebook',
    sqGplus = 'sqGplus',
    sqInstagram = 'sqInstagram',
    sqLinkedin = 'sqLinkedin',
    sqPintarest = 'sqPintarest',
    sqTumbler = 'sqTumbler',
    sqOdnoklassniki = 'sqOdnoklassniki',
    sqMailru = 'sqMailru',
    sqVkontakte = 'sqVkontakte',
//ux
    rub = 'rub',
    home = 'home',
    tasks = 'tasks',
    avatar = 'avatar',
    queryCir = 'queryCir',
    pic = 'pic',
    cross = 'cross',
    cirCross = 'cirCross',
    like = 'like',
    comment = 'comment',
    shared = 'shared',
    rocket = 'rocket',
    calendar = 'calendar',
    geo = 'geo',
    geo2 = 'geo2',
    star = 'star',
    link = 'link',
    chat = 'chat',
    eye = 'eye',
    upload = 'upload',
    phone = 'phone',
    mail = 'mail',
    pc = 'pc',
    house = 'house',
    persons = 'persons',
    navEvent = 'navEvent',
    navUsers = 'navUsers',
    personeFill = 'personeFill',
    personeFillPlus = 'personeFillPlus',
    personeBord = 'personeBord',
    edit = 'edit',
    lock = 'lock',
}

export const getSqSocIcon = (slug: string) => {
    return `sq${slug[0].toUpperCase()}${slug.slice(1)}`
}

export { IconList }
export default IconComponent