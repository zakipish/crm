import * as React from 'react'

interface Props {
    title?: string
    children?: React.ReactNodeArray
    error?: string
}

class FromToTime extends React.Component<Props, {}> {
    render() {
        const { children, title, error } = this.props
        return (
            <div className="fromTo --time">
                {title && <div className="fromTo__title">{title}</div>}
                <div className="fromTo__list">
                    <label className="fromTo__item">
                        <div className="fromTo__label">c</div>
                        <div className="fromTo__input">
                            {children[0]}
                        </div>
                    </label>
                    <label className="fromTo__item">
                        <div className="fromTo__input">
                            {children[1]}
                        </div>
                    </label>
                </div>
                <div className="fromTo__list">
                    <label className="fromTo__item">
                        <div className="fromTo__label">до</div>
                        <div className="fromTo__input">
                            {children[2]}
                        </div>
                    </label>
                    <label className="fromTo__item">
                        <div className="fromTo__input">
                            {children[3]}
                        </div>
                    </label>
                </div>
                {error && <span className="--error">{error}</span>}
            </div>
        )
    }
}

export default FromToTime
