import * as React from 'react'
import { connect } from 'react-redux'
import IconComponent from '../icon/icon.component'
import MessageComponent, { MessageType, MessageStatus, IMessage } from './message.component'
import openSocket from 'socket.io-client'
import config from '../../../core/config'
import { reqGet, reqPost } from '../../../core/api'
import User from '../../../models/user'

interface Message {
    id: number
    message: string
    type: number
    own: boolean
    time: string
    date: string
    user: {
        id: number
        name: string
        color: string
    }
}

interface IProps {
    chat_id: number
    playersCount: number
    user: User
    disabled?: boolean
    disabledLink?: React.ReactNode
}

interface IState {
    text: string
    date: string
    dateHide: boolean
    messages: IMessage[]
}

class ChatComponent extends React.Component<IProps, IState> {

    chatElem: React.RefObject<HTMLDivElement> = React.createRef()
    bodyElem: React.RefObject<HTMLDivElement> = React.createRef()
    timerForScroll: NodeJS.Timeout | any
    timerForDateLabel: NodeJS.Timeout | any
    socket: any

    state = {
        messages: [],
        text: '',
        date: '',
        dateHide: true
    }

    componentDidMount(){
        this.bootstrap()
    }

    componentWillUnmount(){
        this.socket.close()
    }

    bootstrap = async () => {
        const { chat_id } = this.props
        const domain = config.api_host
        this.socket = openSocket(domain, {path: '/ws/socket.io', query: `key=${chat_id}`})
        reqGet(`/chats/${chat_id}/messages`)
            .then(response => {
                const messages = response.data.map(m => this.convertMessage(m))
                this.setState({ messages }, this.scrollChat)
            })
        this.socket.on('message', data => {
            let { messages } = this.state
            messages.push(this.convertMessage(JSON.parse(data)))
            this.setState({ messages }, this.scrollChat)
        })
    }

    convertMessage(message: Message) {
        const { user } = this.props
        const type = message.type == 1 ?
            user && message.user.id == user.id ? MessageType.push : MessageType.pull
        :
            MessageType.noti

        const imessage: IMessage = {
            type: type,
            user: {
                name: message.user.name,
                bg: message.user.color ? message.user.color : '414658'
            },
            message: message.message,
            time: message.time,
            date: message.date
        }

        return imessage
    }

    scrollChat = () => {
        let bodyElem = this.bodyElem.current
        if (!bodyElem) return
        bodyElem.scrollTop = bodyElem.scrollHeight
    }

    onChangeText = (e) => {
        this.setState({ text: e.target.value })
    }

    onBodyScroll = () => {
        if (this.state.dateHide) this.setState({dateHide: false})
        clearTimeout(this.timerForScroll)
        clearTimeout(this.timerForDateLabel)
        this.timerForScroll = setTimeout(() => {
            let bodyElem = this.bodyElem.current
            if (!bodyElem) return
            let { children, scrollTop } = this.bodyElem.current
            let findChildIndex = null
            for (let i = 0; i < children.length; i++) {
                const el: any = children[i]
                if (el.offsetTop <= scrollTop) {
                    findChildIndex = i
                } else {
                    break
                }
            }
            let findChild = this.state.messages[findChildIndex]
            if (findChild) {
                if (findChild.date !== this.state.date) {
                    this.setState({date: findChild.date, dateHide: false})
                }
            }
        }, 10)
        this.timerForDateLabel = setTimeout(() => {
            this.setState({dateHide: true})
        }, 1000)
    }

    onKeyDown = (e: any) => {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault()

            const { text } = this.state
            this.sendMessage(text)
            this.setState({ text: '' })
        }

        return true
    }

    sendMessage = (message: string) => {
        const { chat_id } = this.props
        const data = {
            message,
            type: 1
        }
        reqPost(`/chats/${chat_id}/messages`, JSON.stringify(data))
    }

    render() {
        const { messages, dateHide, text } = this.state
        const { playersCount, disabled, disabledLink } = this.props

        return (
            <div className="chat" ref={this.chatElem}>
                <div className="chat__header">
                    <div className="chat__title">Чат для участников ({playersCount})</div>
                    <div className={`chat__date ${dateHide ? '_hide' : '_show'}`}>{this.state.date}</div>
                </div>
                {disabled ? (
                    <div className="chat__disabled">
                        <IconComponent icon="lock"/>
                        {disabledLink}
                    </div>
                ) : (
                    <>
                    {!messages ? (
                        <div className="chat__empty">
                            <IconComponent icon="chat" fill="rgba(0, 0, 0, 0.14)" />
                            <span>В данном чате пока нет сообщений</span>
                        </div>
                    ) : (
                        <div
                            className="chat__body"
                            ref={this.bodyElem}
                            onScroll={this.onBodyScroll}
                        >
                            { messages.map((el, i) => { return(
                                <MessageComponent key={i} {...el}/>
                            )})}
                        </div>
                    )}
                    </>
                )}
                <div className="chat__footer">
                    {/*
                    <div className="chat__typing">
                        <span>Антон Иванов, Иван Годлывоа ывдлао</span>... печатает
                    </div>
                    */}
                    <div className="chat__textarea">
                        <div className="_autoResize">
                            <div className="_shadow">{this.state.text}</div>
                            <textarea
                                placeholder="Введите сообщение"
                                onChange={this.onChangeText}
                                onKeyDown={this.onKeyDown}
                                value={text}
                            ></textarea>
                        </div>
                    </div>
                    <div className="chat__sendBtn" onClick={() => this.sendMessage(text)}>
                        <IconComponent icon="send" fill="#414658"/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user
})

export default connect(mapStateToProps)(ChatComponent)
