import * as React from 'react'

interface Props {
    image?: string
    text?: string
}

class DisabledChat extends React.Component<Props, {}> {
    render() {
        let { image, text, children } = this.props
        return (
            <div className="disChat">
                {image && (
                    <img src={image} alt="#"/>
                )}
                {text && (
                    <div className="disChat__text">{text}</div>
                )}
                {children && (
                    <div className="disChat__btn">{children}</div>
                )}
            </div>
        )
    }
}

export default DisabledChat