import * as React from 'react'
import moment from 'moment'

import BlockDatepicker, { BlockDatepickerProps } from './block.datepicker'

interface IProps extends BlockDatepickerProps {
    hide: boolean
    refWrap?: React.RefObject<HTMLDivElement>
    onToggle: (state?: boolean) => void
}

class PopupDatepicker extends React.Component<IProps, {}> {

    refPopup: React.RefObject<HTMLDivElement> = React.createRef()

    componentDidMount() {
        this.onResize()
        document.addEventListener('mousedown', this.onClickOutside)
    }
    
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.hide !== this.props.hide) {
            this.onResize()
        }
    }

    onResize = () => {
        setTimeout(() => {
            let refPopup = this.refPopup.current
            if (refPopup) {
                let page: any = document.querySelector('.page')
                let x = 0, y = 0, { hide } = this.props
                if (hide) {
                    page.style = null
                } else {
                    let { innerWidth, innerHeight } = window
                    let rect = refPopup.getBoundingClientRect()
                    x = innerWidth - rect.right
                    y = innerHeight - rect.bottom
                    x = x >= 0 ? 0 : x - 10
                    y = y >= 0 ? 0 : y - 30
                    let addH = page.clientHeight - y
                    if (y < 0) {
                        page.style.height = `${addH}px`
                        window.scroll({ top: addH, behavior: 'smooth' })
                    }
                }
                refPopup.style.transform = `translate(${x}px, 0px)`
                refPopup.classList.toggle('--hide', hide)
            }
        }, 300)
    }

    onClickOutside = (e) => {
        let { hide, refWrap, onToggle } = this.props
        let outSideRef = refWrap ? refWrap.current : this.refPopup.current
        let containState = outSideRef.contains(e.target)
        if(!hide && !containState) onToggle(false)
    }

    render() {
        let { hide, refWrap, onToggle, ...rest } = this.props
        rest.format = rest.format ? rest.format :  'DD-MM-YYYY'
        return (
            <div className="dp-popup --hide" ref={this.refPopup}>
                <BlockDatepicker {...rest}/>
            </div>
        )
    }
}

export default PopupDatepicker