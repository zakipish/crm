import * as React from 'react'
import moment from 'moment'
import BlockDatepicker from './block.datepicker'

interface IProps {
    date: moment.Moment | string
    onChange: (date: moment.Moment) => void
}

interface IState {}

class Datepicker extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {}

        this.onChange = this.onChange.bind(this)
    }

    onChange(date) {
        this.props.onChange(date)
    }

    render() {
        let { date } = this.props
        return (
            <div className="dp">
                <BlockDatepicker date={date} onChange={this.onChange}/>
            </div>
        )
    }
}

export default Datepicker