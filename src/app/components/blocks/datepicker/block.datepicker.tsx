import * as React from 'react'
import moment from 'moment'
import IconComponent, { IconList } from '../icon/icon.component'

export interface BlockDatepickerProps {
    date: moment.Moment | string
    disablePast?: boolean
    onChange: (date: moment.Moment) => void
    format?: string
}

interface IState {
    layer: DPLayers
    addMonth: number
    addYear: number
}

enum DPLayers {
    day = 'day',
    month = 'month',
}

class BlockDatepicker extends React.Component<BlockDatepickerProps, IState> {
    
    selectDate: moment.Moment
    renderDate: moment.Moment
    nowDate: moment.Moment
    viewDate: moment.Moment

    state = {
        layer: DPLayers.day,
        addMonth: 0,
        addYear: 0,
    }

    toggleView() {
        let { layer } = this.state
        if (layer === DPLayers.day) {
            layer = DPLayers.month
        } else {
            layer = DPLayers.day
        }
        this.setState({layer})
    }
    
    initDate() {
        let { date, format } = this.props
        let { addMonth, addYear } = this.state
        if (date && date !== 'Invalid date') {
            this.selectDate = moment(date, format)
        } else {
            this.selectDate = moment()
        }
        this.nowDate = moment()
        this.viewDate = moment(this.selectDate)
            .add(addMonth, 'month')
            .add(addYear, 'year')
        this.renderDate = moment(this.viewDate)
            .startOf('month')
            .startOf('week')
    }

    initRenderDate = () => {
        let { addMonth, addYear } = this.state
        this.renderDate = moment(this.viewDate)
            .add(addMonth, 'month')
            .add(addYear, 'year')
    }

    namesWeek() {
        let outElem = []
        for (let i = 1; i < 8; i++) {
            let val = moment.weekdaysShort(i)
            let className = '_cell --week'
            outElem.push(<div key={i} className={className}>{val}</div>)
        }
        return outElem
    }

    daysMonth() {
        let outElem = []
        let weekLen = this.selectDate.daysInMonth() / 7 
        for (let i = 0; i <= weekLen; i++) {
            outElem.push(
                <div key={i} className="_row">
                    {this.daysWeek()}
                </div>
            )
        }
        return outElem
    }

    daysWeek() {
        let outElem = []
        let { disablePast } = this.props
        for (let i = 0; i < 7; i++) {
            let date = moment(this.renderDate)
            
            let className = '_cell'

            let otherDay = date.month() !== this.viewDate.month()
            let nowState = this.checkNow(date).day
            let pastState = disablePast ? this.checkPast(date) : false
            let selectState = this.checkSelect(date).day
            
            className += otherDay ? ' --otherDay' : ''
            className += nowState ? ' --now' : ''
            className += pastState ? ' --past' : ''
            className += selectState ? ' --select' : ''

            let onClick = (otherDay || pastState) ? null : () => this.setDate(date)

            outElem.push(
                <div 
                    key={i} 
                    className={className} 
                    onClick={onClick}
                >{date.date()}</div>
            )
            
            this.renderDate.add(1, 'day')
        }
        return outElem
    }

    monthsYear() {
        let outElem = []
        let { disablePast } = this.props
        let date = moment(this.viewDate).startOf('year').endOf('month')
        for (let i = 0; i < 12; i++) {
            let val = moment.monthsShort(i)
            
            let className = '_cell --month'

            let nowState = this.checkNow(date).month
            let pastState = disablePast ? this.checkPast(date) : false
            let selectState = this.checkSelect(date).month
            
            className += nowState ? ' --now' : ''
            className += pastState ? ' --past' : ''
            className += selectState ? ' --select' : ''

            let d = moment(date)
            
            let onClick = !pastState ? () => this.setMonthView(d) : null
            
            outElem.push(
                <div 
                    key={i} 
                    className={className}
                    onClick={onClick}
                >{val}</div>
            )

            date.add(1, 'month')
        }
        return outElem
    }

    checkPast = (date: moment.Moment) => {
        let now = this.nowDate.format('YYYY-MM-DD')
        let check = date.format('YYYY-MM-DD')
        return moment(check).isBefore(now)
    }

    checkNow = (date: moment.Moment) => {
        let yearState = date.year() === this.nowDate.year()
        let monthState = date.month() === this.nowDate.month()
        let dayState = date.date() === this.nowDate.date()
        return {
            year: (yearState),
            month: (yearState && monthState),
            day: (yearState && monthState && dayState),
        }
    }

    checkSelect = (date: moment.Moment) => {
        let yearState = date.year() === this.selectDate.year()
        let monthState = date.month() === this.selectDate.month()
        let dayState = date.date() === this.selectDate.date()
        return {
            year: (yearState),
            month: (yearState && monthState),
            day: (yearState && monthState && dayState),
        }
    }

    setDate(date) {
        let { onChange, disablePast } = this.props
        let pastState = disablePast ? this.checkPast(date) : false
        if (!pastState) {
            this.setState({ addMonth: 0, addYear: 0 }, () => {
                onChange(date)
            })
        }
    }

    setMonthView = (date) => {
        let offsetM = date.month() - this.selectDate.month()
        this.setState({ addMonth: offsetM, layer: DPLayers.day })
    }
    
    onClickArr = (i) => () => {
        let { layer, addMonth, addYear } = this.state
        if (layer === DPLayers.day) addMonth += i
        if (layer === DPLayers.month) addYear += i
        this.setState({ addMonth, addYear })
    }

    get className() {
        let className = 'dp-block'
        let { layer } = this.state
        if (layer === DPLayers.day) className += ' --dayLayer'
        if (layer === DPLayers.month) className += ' --monthLayer'
        return className
    }

    render() {
        this.initDate()
        let month = this.viewDate.month()
        let monthStr = moment.months(month)
        let year = this.viewDate.year()
        return (
            <div className={this.className}>
                <div className="_top">
                    <div className="_top__arr" onClick={this.onClickArr(-1)}>
                        <IconComponent icon={IconList.dpArrLeft}/>
                    </div>
                    <div onClick={() => this.toggleView()}>
                        <div className="_top__tit --dayLayer">{monthStr} {year}</div>
                        <div className="_top__tit --monthLayer">{year}</div>
                    </div>
                    <div className="_top__arr" onClick={this.onClickArr(1)}>
                        <IconComponent icon={IconList.dpArrRight}/>
                    </div>
                </div>
                <div className="_body">
                    <div className="_days">
                        <div className="_row">
                            {this.namesWeek()}
                        </div>
                        {this.daysMonth()}
                    </div>
                    <div className="_months">
                        {this.monthsYear()}
                    </div>
                </div>
            </div>
        )
    }
}

export default BlockDatepicker