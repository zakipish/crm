import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from './icon/icon.component'

interface Props {
	title?: string
	className?: string
	type?: string
	children?: string
	icon?: IconList | string
	color?: ('white' | 'green' | 'blue' | 'orange')
	disabled?: boolean
	onClick?: (e?) => void
	to?: string
}

class Button extends React.Component<Props, {}> {

    static defaultProps = {
        color: 'blue'
    }

	renderBtn = () => {
		let { children, icon } = this.props
		return <React.Fragment>
			{icon && (
                <div className="button__icon">
                    <IconComponent icon={icon}/>
                </div>
			)}
			{children && (
				<div className="button__text">{children}</div>
			)}
		</React.Fragment>
	}

	get className() {
		let str = 'button'
		let { className, color, disabled } = this.props

		str += className ? ` ${className}` : ''
		str += disabled === true ? ` --disable` : ''
		str += ` --${color}`

		return str
	}

	render() {
		let { type, disabled, onClick, to } = this.props
		if (disabled === true) {
			onClick = null
			to = to ? '#' : to
		}
		return (
			to ? (
				<Link to={to} className={this.className} onClick={onClick}>
					{this.renderBtn()}
				</Link>
			) : (
				<button className={this.className} onClick={onClick} type={type}>
					{this.renderBtn()}
				</button>
			)
		)
	}
}

export default Button