import * as React from 'react'

export interface IChartState {
    width?: number
}

export interface IChartPoint {
    name: string
    [line: string]: number | string
}

class ChartComponent<P, S extends IChartState = IChartState> extends React.Component<P, S> {

    refChartWrap: React.RefObject<HTMLDivElement> = React.createRef()

    componentDidMount() {
        this.resize()
        window.addEventListener('resize', this.resize)
    }
    
    componentWillMount() {
        window.removeEventListener('resize', this.resize)
    }

    resize = () => {
        setTimeout(() => {
            let refChartWrap = this.refChartWrap.current
            if (refChartWrap) {
                let w = refChartWrap.clientWidth
                this.setState({width: w})
            }
        }, 100);
    }

}

export default ChartComponent