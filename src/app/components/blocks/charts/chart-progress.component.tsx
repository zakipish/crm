import * as React from 'react'
import { PieChart, Pie } from 'recharts'

import ChartComponent, { IChartState } from './chart.component'

interface IProps {
    percent: number
    color: string
    num?: string
    subtitle?: string
    lineState?: boolean
}

interface IState extends IChartState {}

class ChartProgressComponent extends ChartComponent<IProps, IState> {

    constructor(props) {
        super(props)
        this.state = {
            width: 890,
        }
    }

    getDepth() {
        let { lineState } = this.props
        let { width } = this.state
        if (lineState) {
            return {
                innerRadius: width * 0.35,
                outerRadius: width * 0.4,
                strokeWidth: "20px",
                strokeLinejoin: "round"
            }
        }
        return {
            outerRadius: width * 0.4 
        }
    }

    render() {
        let { percent, color, num, subtitle } = this.props
        let { width } = this.state
        let end = percent * 3.6
        let data = [{name: 'm', p1: 100}]
        return (
            <div className="chart-pregress" ref={this.refChartWrap}>
                <PieChart width={width} height={width}>
                    <Pie 
                        dataKey="p1" data={data} 
                        startAngle={0} endAngle={360}
                        innerRadius={width * 0.35} outerRadius={width * 0.4} 
                        fill="#E8E8E8"
                    ></Pie>
                    <Pie 
                        dataKey="p1" data={data} 
                        startAngle={0} endAngle={end}
                        {...this.getDepth()}
                        stroke={color}
                        fill={color}
                    ></Pie>
                </PieChart>
                <div className="chart-pregress__text">
                    {num ? <div className="chart-pregress__num">{num}</div> : ''}
                    {subtitle ? <div className="chart-pregress__subtit">{subtitle}</div> : ''}
                </div>
            </div>
        )
    }
}

export default ChartProgressComponent