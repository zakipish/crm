import * as React from 'react'
import { ComposedChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, Line } from 'recharts';

import ChartComponent, { IChartState, IChartPoint } from './chart.component'

interface IProps {
    data: IChartPoint[]
    height: number
}

interface IState extends IChartState {}

class CartLinerComponent extends ChartComponent<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {
            width: 890,
        }
    }

    render() {
        let { data, height } = this.props
        let { width } = this.state
        return (
            <div className="chart-liner" ref={this.refChartWrap}>
                <ComposedChart 
                    width={width} height={height} 
                    data={data}
                    margin={{top: 50, right: 20, bottom: 20, left: -20}}
                >
                    <CartesianGrid stroke='#eee'/>
                    <XAxis dataKey="name" /> <YAxis />
                    <Legend/> <Tooltip/>

                    <Bar dataKey='booked' barSize={20} name="Часов забронировано (%)" fill='#5CFF59' isAnimationActive/>

                    <Line type='monotone' dataKey='pending' name="Игр набирается" stroke='#0088FE' isAnimationActive/>

                    <Line type='monotone' dataKey='complete' name="Оплаченных игр" stroke='#FFBB28' isAnimationActive/>
                </ComposedChart>
            </div>
        )
    }
}

export default CartLinerComponent
