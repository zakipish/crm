import * as React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import IconComponent, { IconList } from './icon/icon.component'

interface Props {
    editTo: string
    image: string
    title: string
    siteUrl?: string
    interval: {
        from: moment.Moment
        to: moment.Moment
    }
    price: string | number
    paramsTo?: string
    params?: Array<{
        color?: string
        icon?: string
        tit: string
        val: string
    }>
}

class Profile extends React.Component<Props, {}> {

    static defaultProps = {
        editTo: '#',
        image: '#',
        title: '-',
        interval: {
            from: moment(),
            to: moment(),
        },
        paramsTo: '#',
        price: '0',
    }

    render() {
        let { editTo, image, title, siteUrl, interval, price, paramsTo, params } = this.props
        let dateStr = interval.from.format('DD MMMM, hh:mm') + interval.to.format(' - hh:mm')
        let dayStr = interval.from.format('dddd')

        return (
            <div className="profile">
                <Link to={editTo} className="profile__edit">
                    <IconComponent icon={IconList.edit}/>
                </Link>
                <div 
                    className="profile__image"
                    style={{backgroundImage: `url(${image})`}}
                ></div>
                <div className="profile__name">{title}</div>
                {siteUrl && (
                    <a 
                        className="profile__site"
                        href={siteUrl} 
                        target="_blank"
                    >Смотреть на сайте</a>)}
                <hr/>
                {(params && params.length) && (
                    <Link to={paramsTo} className="profile__params">
                        <Link to={paramsTo} className="profile__paramsLink">См.все</Link>
                        {params.map((param, i) => 
                            <div 
                                key={i}
                                className={`profileParam ${param.color ? `--${param.color}` : ''}`} 
                            >
                                <div className="profileParam__icon">
                                    {param.icon && <IconComponent icon={param.icon}/>}
                                </div>
                                <div className="profileParam__tit">{param.tit || '-'}</div>
                                <div className="profileParam__val">{param.val || '-'}</div>
                            </div>
                        )}
                    </Link>
                )}
                <hr/>
                <div className="profileLine">
                    <div className="profileLine__tit">{dateStr}</div>
                    <div className="profileLine__val">{dayStr}</div>
                </div>
                <hr/>
                <div className="profilePrice">
                    <div className="profilePrice__tit">Цена</div>
                    <div className="profilePrice__val">
                        <IconComponent icon={IconList.rub}/>
                        <span>{price}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile
