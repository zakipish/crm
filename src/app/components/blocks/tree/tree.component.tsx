import * as React from 'react'
import IconComponent from '../icon/icon.component'
import * as _ from "lodash"

export interface ITreeProps {
    filter?: string
    listTree: any
    selected: number[]
    onSelect: (item: ITreeItem) => void
}

export interface ITreeItem {
    id: number
    name: string
    label: string
    items?: ITreeItem[]
    [x: string]: any
}

class TreeComponent extends React.Component<ITreeProps, {}> {

    levelElem: React.ReactFragment[];
    levelBg = ['orange', 'green', 'yellow', 'blue'];
    showItemsId: string[]

    constructor(props) {
        super(props)
    }

    onSelect(item) {
        this.props.onSelect(item)
    }

    renderLevel(list, level) {
        let { selected } = this.props
        let arrElem = []
        let listElem = []
        let bg = this.levelBg[level] || this.levelBg[this.levelBg.length - 1]
        if (list && list.length) {
            listElem.push( list.map(el => { 
                let selectedState = selected.includes(el.id)
                return !el.hide ? (
                    <div key={el.id}>
                        <div 
                            className={`_text --a${level} --${bg} ${selectedState ? '--act' : ''}`}
                            onClick={() => this.onSelect(el)}
                        >
                            <span>{el.name}</span>
                        </div>
                        {this.renderLevel(el.items, level + 1)}
                    </div>
                ) : null
            }))
            if (listElem && listElem.length) {
                arrElem.push(
                    <div key={`_arr${level}`} className={`_arr --a${level} --${bg}`}>
                        <IconComponent icon="treeArr"/>
                    </div>
                )
            }
        }
        return(
            <div className={`_level --a${level} --${bg}`}>
                {arrElem} 
                {listElem}
            </div>
        )
    }

    filterList() {
        let { listTree, filter } = this.props
        let copyList
        if (filter && filter.length) {
            this.showItemsId = []
            copyList = _.cloneDeep(listTree)
            copyList = this.preFilter(copyList, filter.toLowerCase())
            copyList = this.postFilter(copyList)
        } else {
            copyList = listTree
        }
        return copyList
    }

    preFilter(list, filter) {
        return list.map(el => {
            el.hide = true
            let findOfName = el.name.toLowerCase().indexOf(filter)
            if (findOfName >= 0) {
                this.showItemsId = [
                    ...this.showItemsId, 
                    ...el.label.split('.')
                ]
            }
            el.items = this.preFilter(el.items, filter)
            return el
        })
    }
    postFilter(list) {
        return list.map(el => {
            let findOfId = this.showItemsId.includes(el.id)
            if (findOfId) {
                el.hide = false
            }
            el.items = this.postFilter(el.items)
            return el
        })
    }

    render() {
        let listTree = this.filterList()
        return (
            <div className="tree">
                {this.renderLevel(listTree, 0)}
            </div>
        )
    }
}

export default TreeComponent
