import * as React from 'react'
import OutsideClickComponent, { IOutsideClickProps, IOutsideClickState } from '../../_shared/outside-click.component'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'
import FieldComponent from '../../forms/field.component'
import TreeComponent, { ITreeItem } from './tree.component'

interface IProps extends IOutsideClickProps {
    name?: string
    placeholder?: string
    listTree: any
    selected: number[]
    onSelect: (item: ITreeItem) => void
}

interface IState extends IOutsideClickState {
    filter: string
}

class TreeSelectcomponent extends OutsideClickComponent<IProps, IState> {

    constructor(props) {
        super(props)
        this.state = {
            hide: true,
            filter: null,
        }
        this.onHide = this.onHide.bind(this)
        this.onShow = this.onShow.bind(this)
        this.onToggle = this.onToggle.bind(this)
        this.onSearch = this.onSearch.bind(this)
        this.onKeyPress = this.onKeyPress.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
        window.addEventListener('keydown', this.onKeyPress)
    }
    
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
        window.removeEventListener('keydown', this.onKeyPress)
    }

    onKeyPress(e) {
        let status = !this.state.hide
        if (status) {
            if (e.key === 'Escape') {
                e.preventDefault()
                this.onHide()
            }
        }
    }

    onHide() {
        if (!this.state.hide) this.setState({hide: true})
    }
    
    onShow() {
        if (this.state.hide) this.setState({hide: false})
    }

    onToggle() {
        this.setState({hide: !this.state.hide})
    }

    onSearch(e) {
        this.setState({filter: e.target.value})
    }

    render() {
        let { name, placeholder, listTree, selected, onSelect } = this.props
        let { filter } = this.state
        return (
            <div 
                className={`fieldSelect${this.hide ? ' --hide' : ' --show'}`}
                ref={this.outsideRef} 
                onClick={this.onShow}
            >
                <div className="fieldSelect__btn" onClick={this.onHide}>
                    <IconComponent icon={IconList.selectArr}/>
                </div>
                <div className="fieldSelect__input">
                    <FieldComponent 
                        name={name}
                        placeholder={placeholder}
                        value={filter}
                        onChange={this.onSearch}
                    />
                </div>
                <div className="fieldSelect__list">
                    <div className="fieldSelect__scroll">
                        <TreeComponent 
                            listTree={listTree}
                            selected={selected}
                            onSelect={onSelect}
                            filter={filter}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default TreeSelectcomponent
