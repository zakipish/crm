import * as _ from 'lodash'

export const onChangeSelected = (selectedIn, item) => {
    let selected = _.cloneDeep(selectedIn)
    let seletcItem = selected.find(el => el === item.id)
    if (seletcItem) {
        selected = removeChild(selected, item.items)
    } else {
        selected = addParent(selected, item)
    }
    selected = toggleOne(selected, item.id)
    return selected
}

const removeChild = (selected, items) => {
    items.forEach(el => {
        let index = selected.findIndex(sp => sp === el.id)
        if (index >= 0) {
            selected.splice(index, 1)
        }
        if (el.items) {
            removeChild(selected, el.items)
        }
    })
    return selected
}

const addParent = (selected, item) => {
    let ids = getParentLabel(item.label).split('.')
    ids.forEach(id => {
        id = +id
        let index = selected.findIndex(sp => sp === id)
        if (index < 0) {
            selected.push(id)
        }
    });
    return selected
}

const getParentLabel = (label) => {
    let lastDotIndex = label.lastIndexOf('.')
    return label.slice(0, lastDotIndex)
}

const toggleOne = (selected, id) => {
    let index = selected.findIndex(sp => sp === id)
    if (index >= 0) {
        selected.splice(index, 1)
    } else {
        selected.push(id)
    }
    return selected
}
