import * as React from 'react'
import IconComponent from './icon/icon.component';

interface Props {
    title?: string
    value: string
    onChange?: (value: string) => void
    disabled?: boolean
}

class PriceBlock extends React.Component<Props, {}> {

    static defaultProps = {
        value: '0',
        onChange: (value) => {},
    }

    render() {
        let { title, value, onChange, disabled } = this.props
        return (
            <div className="priceBlock">
                {title && <div className="priceBlock__title">{title}</div>}
                <div className="priceBlock__price">
                    <IconComponent icon="rub"/>
                    <input 
                        type="text" 
                        value={value} 
                        onChange={(e) => onChange(e.target.value)}
                        disabled={disabled}
                    />
                </div>
            </div>
        )
    }
}

export default PriceBlock
