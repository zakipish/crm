import * as React from 'react'
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import IconComponent from '../icon/icon.component'
import UploadComponent from '../../forms/upload.component'
import {reqPost} from "../../../core/api";
import {react} from "babel-types";

interface IProps {
    className?: string
    id: string
    title: string
    list: {order:number,url:string,id?:number}[]
    onChange?: Function
    demo?: boolean
    scroll?: boolean
    onAdd?: Function
    url?:string
    onRemove?: Function
}

class GalleryComponent extends React.Component<IProps, {list: {order:number,url:string,id?:number}[]}> {
    state={
        list:[]
    }
    onUpload = (arrFiles,files)=>{
        let { list, onChange, onAdd } = this.props
        if (onChange) {
            let newImages = [...list, ...arrFiles]
            let newList = onChange(newImages)
            this.setState({list:newList})
        }
        if (onAdd) onAdd(files) 
    }

    componentDidMount(): void {
        this.setState({list:this.props.list})
    }

    componentDidUpdate(prevProps: any): void {
        if (prevProps.list != this.props.list)
            this.setState({ list: this.props.list })
    }


    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            list: arrayMove(this.state.list, oldIndex, newIndex),
        });
       let form = new FormData();
        form.append('action', 'update_images_order');
        form.append('_method', 'PATCH');
        this.state.list.forEach(image => {
            form.append("images[]", image.url);
            // @ts-ignore
            form.append("image_ids[]", image.id.toString());
        });
        reqPost(this.props.url, form);
    };

    onRemove =(i)=> {
        let { onChange, onRemove } = this.props
        let {list} = this.state
        let id = list[i].id;
        if (id && onRemove) onRemove(id)
        if (onChange) {
            list.splice(i, 1)
            onChange(list)
        }
    }

    render() {
        let { className, id, title, demo, scroll, onAdd } = this.props
        let { list} = this.state
        return (
            <div className={`gallery ${className ? className : ''}`}>
                <div className="_title">{title}</div>
                {!demo ?
                    <div className="_subtitle">
                        <div className="_subtitle__icon">
                            <IconComponent icon="ok"/>
                        </div>
                        <span>Свободное ранжирование производится с помощью мышки</span>
                    </div>
                : ''}
                    <SortableImages id={id} onRemove={this.onRemove}scroll={scroll} onUpload={this.onUpload} onSortEnd={this.onSortEnd} axis='xy' list={list} demo={demo}/>
            </div>

        )
    }
}
class Image extends React.Component<{id:any,onRemove:any,scroll:any,onUpload,list:any, demo:any},{}>{
    render(){
        const {id,onRemove,scroll,onUpload,list,demo} = this.props
        return(<div className={`_wrap ${scroll ? '--scroll' : ''}`}>
            {!demo ?
                <div className="_item">
                    <UploadComponent
                        id={id}
                        onUpload={onUpload}
                        title="Загрузить фото"
                        size={{width: 119, height: 113, view: false}}
                        multiple={true}
                    />
                </div>
                : ''}
            {list.map((el, i) =>
                <SortabelImageBlock onRemove={onRemove} disabled={demo} no={i} index={i} key={i}
                                    demo={demo} el={el}/>)}
        </div>)
    }
}

class ImageBlock extends React.Component<{ demo: boolean, el: any, key: any, onRemove: any,index:number,no:number }, {}> {
    render() {
        const {demo, el, key, onRemove,index,no} = this.props;
        return (
            <div className="_item gallery_drag">
                {!demo ?
                    <div className="_cross" onClick={()=>{onRemove(no)}}>
                        <IconComponent icon="cross"/>
                    </div>
                    : ''}
                <div className="_img">
                    <img src={el.url} alt="#"/>
                </div>
            </div>
        )
    }
}
const SortableImages/*GalleryComponent*/= SortableContainer(Image);
const SortabelImageBlock= SortableElement(ImageBlock);

export default GalleryComponent
