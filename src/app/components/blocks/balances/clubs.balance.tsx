import * as React from 'react'

interface IProps {
    list: Array<{
        id: number
        img?: string
        title: string
        balance: string
    }>
    onClickUp: (id: number) => void
}

class ClubsBalance extends React.Component<IProps, {}> {
    render() {
        let { list, onClickUp } = this.props
        return (
            <div>
                {list && (list.map((item, i) => (
                    <div key={i} className="balance-club">
                        <div className={`balance-club__img ${item.img ? '--img' : '--icon'}`}>
                            <img src={item.img || '/assets/images/icon-club.svg'} alt="#"/>
                        </div>
                        <div className="balance-club__tit">{item.title}</div>
                        <div className="balance-club__right">
                            <div className="balance-club__val">{item.balance}</div>
                            <div 
                                className="balance-club__btn"
                                onClick={() => onClickUp(+item.id)}
                            >Пополнить</div>
                        </div>
                    </div>
                )))}
            </div>
        )
    }
}

export default ClubsBalance
