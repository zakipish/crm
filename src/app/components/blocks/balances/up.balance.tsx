import * as React from 'react'
import config from '../../../core/config'

import FieldComponent from '../../forms/field.component'

interface IProps {
    clubId: number
}

interface IState {
    balance: string
}

class UpBalance extends React.Component<IProps, IState> {
    state: IState = {
        balance: '1000'
    }

    handleChangeBalance = (e: any) => {
        const value = `${+e.target.value}` || ''
        this.setState({ balance: value })
    }

    render() {
        const { clubId } = this.props
        const { balance } = this.state

        return (
            <div>
                <form action="https://demomoney.yandex.ru/eshop.xml" method="post" target="_blank"> 
                    <input name="customerNumber" type="hidden" value={clubId}/>
                    <input name="shopId" type="hidden" value={config.yk_shopid}/>
                    <input name="scid" type="hidden" value={config.yk_scid}/>
                    <div className="formRow">
                        <div className="formRow__item">
                            <label className="fieldInput">
                                <div className="_input">
                                    <input name="sum" placeholder="1000" type="text" value={balance} onChange={this.handleChangeBalance} required={true}/>
                                </div>
                            </label>
                        </div>
                        <div className="formRow__item --auto">
                            <button className="formBtn" type="submit">Ok</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default UpBalance
