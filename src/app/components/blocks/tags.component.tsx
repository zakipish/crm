import * as React from 'react'
import IconComponent from './icon/icon.component'
import { ITreeItem } from './tree/tree.component'

interface IProps {
    listTree: ITree[]
    selected: number[]
    onRemove?: (item: ITreeItem) => void
}

export class ITree {
    id: any
    name: string
    label: string
    items: ITree[]

    constructor(obj) {
        if (obj) {
            this.id = obj.id
            this.name = obj.name
            this.label = obj.label
        }
        this.items = []
    }
}

class TagsComponent extends React.Component<IProps, {}> {

    tagsRef: React.RefObject<HTMLDivElement>;
    itemsElem: React.ReactFragment[];
    levelBg = ['orange', 'green', 'yellow', 'blue'];

    constructor(props) {
        super(props)
        this.tagsRef = React.createRef()
        this.resize = this.resize.bind(this)
    }

    componentDidMount() {
        window.addEventListener('resize', this.resize)
        this.resize()
    }

    componentWillMount() {
        window.removeEventListener('resize', this.resize)
    }

    componentWillReceiveProps() {
        this.resize()
    }

    onRemove(item) {
        this.props.onRemove(item)
    }

    resize() {
        setTimeout(() => {
            let ulElems = this.tagsRef.current.querySelectorAll('ul')
            ulElems.forEach((ul, i) => {
                let tmpTop = 0
                let liElems = ul.querySelectorAll('li')
                liElems.forEach((li, j) => {
                    let liTop = li.offsetTop
                    li.classList.remove('--last-row')
                    // li.classList.remove('--first-row')
                    if (j === 0) {
                        // li.classList.add('--first-row')
                        tmpTop = li.offsetTop
                    } else {
                        if (tmpTop < liTop) {
                            // li.classList.add('--first-row')
                            liElems[j - 1].classList.add('--last-row')
                            tmpTop = liTop
                        }
                    }
                })
            })
        }, 100)
    }

    getRenderItems(items) {
        this.itemsElem = []
        this.runRenderItems(items, 1)
        return this.itemsElem
    }

    runRenderItems(items, level) {
        let { selected, onRemove } = this.props
        if (items) {
            items.forEach(el => {
                let bg = this.levelBg[level] || this.levelBg[this.levelBg.length - 1]
                let selectedState = selected.includes(el.id)
                if(selectedState) {
                    this.itemsElem.push(
                        <li key={el.id}className={`--${bg}`}>
                            <div>
                                <span>{el.name}</span>
                                {onRemove ?
                                    <i onClick={() => this.onRemove(el)}>
                                        <IconComponent icon="cirCross"/>
                                    </i>
                                : ''}
                            </div>
                        </li>
                    )
                }
                if (el.items) {
                    this.runRenderItems(el.items, level + 1)
                }
            })
        }
    }

    render() {
        let { listTree, selected, onRemove } = this.props
        return (
            <div className="tags" ref={this.tagsRef}>
                {listTree.map(el => {
                    let selectedState = selected.includes(el.id)
                    return(
                        <ul key={el.id}>
                            {selectedState ?
                                <li className={`--${this.levelBg[0]}`}>
                                    <div>
                                        <span>{el.name}</span>
                                        {onRemove ?
                                            <i onClick={() => this.onRemove(el)}>
                                                <IconComponent icon="cirCross"/>
                                            </i>
                                        : ''}
                                    </div>
                                </li>
                            : ''}
                            {this.getRenderItems(el.items)}
                        </ul>
                    )
                })}
            </div>
        )
    }
}

export default TagsComponent
