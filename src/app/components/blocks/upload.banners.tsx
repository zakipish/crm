import * as React from 'react'

import {
  SortableContainer,
  SortableElement,
  arrayMove,
} from 'react-sortable-hoc'

import IconComponent from './icon/icon.component'
import Image from '../../models/image'
import { upload } from '../../services/image.service'

interface Props {
  title?: string
  value: Image[]
  onChange: (value: Image[]) => void
}

interface State {
  indexView: number,
  saving: boolean
}

interface BannerProps {
  banner: Image
  onRemove: () => void
}

interface BannersContainerProps {
  banners: Image[]
  onRemove: (i: number) => () => void
}

class Banner extends React.Component<BannerProps> {
  render() {
    const { banner, onRemove } = this.props

    return <div
      className="uploadBanner__thumb"
      style={{backgroundImage: `url(${banner.url})`}}
    >
      <div
        className="uploadBanner__thumbCross"
        onClick={onRemove}
      >
        <IconComponent icon="cross"/>
      </div>
    </div>
  }
}
const SortableItem = SortableElement(Banner)

class BannersContainer extends React.Component<BannersContainerProps> {
  render() {
    const { banners, onRemove } = this.props
    const sortedBanners = banners.sort((a, b) => a.order - b.order)

    return <div className="--sortable">
      {sortedBanners.map((banner, i) => <SortableItem
        key={`banner-${i}`}
        index={i}
        banner={banner}
        onRemove={onRemove(i)}
      />)}
    </div>
  }
}
const SortableList = SortableContainer(BannersContainer)

class UploadBanners extends React.Component<Props, State> {
    len = 0

    state: State = {
      indexView: 0,
      saving: false
    }

    componentDidUpdate() {
        /*
        let len = this.props.value ? this.props.value.length : 0
        if (this.len !== len) {
            this.len = len
            this.setState({indexView: (len - 1)})
        }
         */
    }

    uploadImage = (e) => {
      let { files } = e.target
      let { onChange, value } = this.props
      let file = files[0]
      upload(file)
        .then(async newImage => {
          if (value == null) {
            value = []
          }

          const order = value.length > 0 ? (value.map(value => value.order)[value.length] + 1) : 1
          newImage.order = order

          value.push(newImage)
          this.setState({
            saving: true
          })
          await onChange(value)
          this.setState({
            saving: false
          })
        })
    }

    setViewIndex = (indexView) => async () => {
      this.setState({ indexView })
    }

    removeItem = (indexView) => async () => {
      let { value, onChange } = this.props
      console.log(indexView)
      value.splice(indexView, 1)
      this.setState({
        saving: true
      })
      console.log('ri-saving')
      await onChange(value)
      console.log('ri-saved')
      this.setState({
        saving: false
      })
    }

    onSortEnd = async ({ oldIndex, newIndex }) => {
      const { value, onChange } = this.props
      let newValue = arrayMove(value, oldIndex, newIndex).map(({ order, ...rest }, i) => ({ order: i, ...rest }))
      this.setState({
        saving: true
      })
      console.log('sort-saving')
      await onChange(newValue)
      console.log('sort-saved')
      this.setState({
        saving: false
      })
    }

    render() {
      let { indexView, saving } = this.state
      let { value, title } = this.props
      let isValue = (value && value.length) ? true : false
      console.log({ value, indexView })
      return <div className="uploadBanner">
        <div className="uploadBanner__preview">
          <div className="uploadBanner__slideList">
            {isValue && value.map((banner, i) => <div
              key={i}
              className={`uploadBanner__slide ${i === indexView ? '--show' : '--hide'}`}
              style={{backgroundImage: `url(${banner.url})`}}
            >
              <div
                className="uploadBanner__slideTitle"
                dangerouslySetInnerHTML={{__html: title || ''}}
              ></div>
            </div>)}
          </div>
          <div className="uploadBanner__thumbList">
            {isValue && <SortableList
              banners={value}
              onSortEnd={this.onSortEnd}
              onRemove={this.removeItem}
              axis="x"
            />}
            <label className="uploadBanner__thumb">
              <span>Загрузить фото</span>
              <input type="file" onChange={this.uploadImage}/>
            </label>
          </div>
        </div>
        {saving && <div className="uploadBanner__saving">
        </div>}
      </div>
    }
}

export default UploadBanners
