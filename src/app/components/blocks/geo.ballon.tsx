import * as React from 'react'

interface Props {
    children: string
}

class GeoBallon extends React.Component<Props, {}> {

    static defaultProps = {
        children: '...'
    }

    render() {
        let { children } = this.props
        return (
            <div className="geoBallon">
                <div className="geoBallon__icon">
                    <img src="/assets/images/geo-ballon-pic.svg" alt="#"/>
                    <span>Место</span>
                </div>
                <div className="geoBallon__text">{children}</div>
            </div>
        )
    }
}

export default GeoBallon