import * as React from 'react'

interface IProps {
    list: Array<{
        key: string
        value: string
    }>
}

class CheckList extends React.Component<IProps, {}> {
    render() {
        let { list } = this.props
        return (
            <div className="list-check">
                {list.map((el, i) => { return(
                    <div key={i} className="_item">
                        <div className="_item__key">{el.key}</div>
                        <div className="_item__val">{el.value}</div>
                    </div>
                )})}
            </div>
        )
    }
}

export default CheckList