import * as React from 'react'
import { Switch } from 'react-router-dom'
import { History } from 'history'

import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    title?: string
    nav: ITabNavItem[]
    history: History
    activeIndex?: number
}

export interface ITabNavItem {
    label?: string
    icon?: IconList | string 
    url?: string
    onClick?: Function
}

class TabSectionComponent extends React.Component<IProps, {}> {

    onClickNav(el: ITabNavItem) {
        if (el.url) this.props.history.push(el.url)
        if (el.onClick) el.onClick()
    }

    getLinkClass(el: ITabNavItem, i: number): string {
        let activeState = ''
        if (el.url) {
            let currentPath = this.props.history.location.pathname 
            if (el.url === currentPath) {
                activeState += ' --active'
            }
        } else if (this.props.activeIndex >= 0) {
            if (i === this.props.activeIndex) {
                activeState += ' --active'
            }
        }
        return activeState
    }

    render() {
        let { title, nav, children } = this.props
        return (
            <div className="tab-section">
                {title ? <div className="tab-section_title">{title}</div> : ''}
                <div className="tab-section__nav">
                    <ul>
                        {nav.map((el, i) => { return(
                            <li 
                                key={i} 
                                className={this.getLinkClass(el, i)} 
                                onClick={() => this.onClickNav(el)}
                            >
                                {el.icon ? <IconComponent icon={el.icon}/> : ''}
                                {el.label ? <span>{el.label}</span> : ''}
                            </li>
                        )})}
                    </ul>
                </div>
                <div className="tab-section__cnt">
                    <Switch>
                        {children}
                    </Switch>
                </div>
            </div>
        )
    }
}

export default TabSectionComponent
