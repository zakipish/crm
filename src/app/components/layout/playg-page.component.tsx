import * as React from 'react'
import Service from '../../models/service'
import Equipment from '../../models/equipment'

import { DataSportsTree, DataSportsList } from '../../_data/sports.data'

import { IconList } from '../blocks/icon/icon.component'
import PriceListComponent, { IPriceListItem } from '../blocks/price-list.component'
import SectionComponent from './section.component';
import ClubHeaderComponent from './club-header.component'
import GalleryComponent from '../blocks/gallery/gallery.component'
import TagsComponent, { ITree } from '../blocks/tags.component'
import ToggleSectionComponent from './toggle-section.component'

interface IProps {
    id: number
    title: string
    subtit: string
    params?: Array<{
        key: string
        value: string
    }>
    descriptions?: string
    images?: {order:number,url:string}[]
    selectedSports?: number[]
    services?: Service[]
    equipments?: Equipment[]
    sports?: ITree[]
    prices?: Array<{
        title: string
        icon?: IconList | string
        list: IPriceListItem[]
    }>
    logo?: string
}

class PlaygPageComponent extends React.Component<IProps, {}> {
    render() {
        let { id, title, subtit, params, descriptions, images, sports, selectedSports, prices, logo, services, equipments } = this.props
        let urlEditClub = `/playground/edit/${id}`
        return (
            <SectionComponent>
                <ClubHeaderComponent
                    subtit={subtit}
                    title={title}
                    btns={[{label: 'Редактировать', url: urlEditClub, bg: 'blue'}]}
                    img={logo}
                    params={params}
                />
                <hr/>
                {descriptions ?
                    <div className="text-area-section">
                        <div className="section__title">О зале</div>
                        <div
                            className="text-area-section__content"
                            dangerouslySetInnerHTML={{__html: descriptions}}
                        ></div>
                    </div>
                : ''}
                {images ?
                    <GalleryComponent title="Фото зала" id="up3" list={images} demo scroll/>
                : ''}
                {sports ?
                    <div className="formRow formRow--wrap">
                        <div className="formRow__title">
                            <span>Виды спорта</span>
                            <ul className="levelList">
                                <li className="--orange">1 уровень</li>
                                <li className="--green">2 уровень</li>
                                <li className="--yellow">3 уровень</li>
                                <li className="--blue">4 уровень</li>
                            </ul>
                        </div>
                        <div className="formRow__item --col12">
                            <TagsComponent listTree={sports} selected={selectedSports}/>
                        </div>
                    </div>
                : ''}
                {prices ? prices.map((el, i) => { return(
                    <ToggleSectionComponent key={i} title={el.title} icon={el.icon} scroll>
                        <PriceListComponent list={el.list}/>
                    </ToggleSectionComponent>
                )}) : ''}
                {services ?
                    <ToggleSectionComponent
                        title="Услуги"
                        icon={IconList.house}
                        scroll
                    >
                        <PriceListComponent
                            list={services.map(service => ({ title: service.name, params: [`${service.price} руб.`, `За ${service.unit}`] }))}
                        />
                    </ToggleSectionComponent>
                : ''}

                {equipments ?
                    <ToggleSectionComponent
                        title="Инвентарь"
                        icon={IconList.house}
                        scroll
                    >
                        <PriceListComponent
                            list={equipments.map(equipment => ({ title: equipment.name, params: [`${equipment.price} руб.`, `За ${equipment.unit}`] }))}
                        />
                    </ToggleSectionComponent>
                : ''}
            </SectionComponent>
        )
    }
}

export default PlaygPageComponent
