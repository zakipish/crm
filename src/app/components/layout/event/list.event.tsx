import * as React from 'react'
import TabLinkPage from '../page/tab-link.page';

interface Props {
    top?: React.ReactNode
    children: React.ReactNode
}

class ListEvent extends React.Component<Props, {}> {
    render() {
        let { top, children } = this.props
        return (
            <div className="listEvent">
                {top && (
                    <div className="listEvent__top">
                        {top}
                    </div>
                )}
                <div className="listEvent__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default ListEvent