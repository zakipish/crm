import * as React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'

moment.locale('ru')

interface Props {
    to?: string
    image: string
    date: moment.Moment
    title: string
    justLen: number
    likeLen: number
    price: number
}

class CardEvent extends React.Component<Props, {}> {

    static defaultProps = {
        to: '#',
        image: '#',
        date: moment(),
        title: '-',
        justLen: 0,
        likeLen: 0,
        price: 0,
    }

    render() {
        let { to, image, date, title, justLen, likeLen, price } = this.props
        return (
            <div className="cardEvent">
                <div 
                    className="cardEvent__img"
                    style={{backgroundImage: `url(${image})`}}
                >
                    <div className="cardEvent__date">
                        <i>{date.format('DD')}</i>
                        <span>{date.format('MMM')}</span>
                    </div>
                </div>
                <div className="cardEvent__tit">{title}</div>
                <div className="cardEvent__paramList">
                    <div className="cardEvent__param">
                        <IconComponent icon={IconList.personeFill}/>
                        <span>{justLen} идут</span>
                    </div>
                    <div className="cardEvent__param">
                        <IconComponent icon={IconList.personeBord}/>
                        <span>{likeLen} возможно</span>
                    </div>
                </div>
                <div className="cardEvent__bottom">
                    <div className="cardEvent__price">{price} руб</div>
                    <Link to={to} className="cardEvent__link">Подробнее</Link>
                </div>
            </div>
        )
    }
}

export default CardEvent
