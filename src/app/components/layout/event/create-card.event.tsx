import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'

interface Props {
    to?: string
    onClick?: (e?) => void
    children: string
}

class CreateCardEvent extends React.Component<Props, {}> {

    static defaultProps = {
        to: '#',
        onClick: (e?) => {},
        children: 'Добавить'
    }

    render() {
        let { to, onClick, children } = this.props
        return (
            <Link 
                to={to} 
                className="createCardEvent"
                onClick={onClick}
            >
                <IconComponent icon={IconList.plus}/>
                <span>{children}</span>
            </Link>
        )
    }
}

export default CreateCardEvent