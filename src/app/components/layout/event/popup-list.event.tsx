import * as React from 'react'

interface Props {}
interface State {
    hide: boolean
}

class PopupListEvent extends React.Component<Props, State> {

    static _this

	constructor(props) {
		super(props)
		this.state = {
			hide: true
		}
		PopupListEvent._this = this
	}

	static toggle = () => {
		PopupListEvent._this.onToggle()
    }

	static show = () => {
		PopupListEvent._this.onToggle(false)
	}

	static hide = () => {
		PopupListEvent._this.onToggle(true)
	}

	onToggle = (hide = !this.state.hide) => {
		this.setState({hide})
	}

    render() {
        let { hide } = this.state
        let { children } = this.props
        return (
            <div className={`popupListEvent ${hide ? ' --hide' : ' --show'}`}>
                <div className="popupListEvent__bg" onClick={() => this.onToggle(true)}></div>
                {children}
            </div>
        )
    }
}

export default PopupListEvent
