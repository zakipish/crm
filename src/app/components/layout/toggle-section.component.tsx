import * as React from 'react'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    title: string
    icon?: IconList | string
    scroll?: boolean
}

interface IState {
    hide: boolean
    cntHeight: number
}

class ToggleSectionComponent extends React.Component<IProps, IState> {

    refContent: React.RefObject<HTMLDivElement>;

    constructor(props: IProps) {
        super(props)
        this.state = {
            hide: true,
            cntHeight: 10000,
        }
        this.refContent = React.createRef()
        this.setMaxHeightCnt = this.setMaxHeightCnt.bind(this)
    }

    componentDidMount() {
        window.addEventListener('resize', this.setMaxHeightCnt)
        this.setMaxHeightCnt()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setMaxHeightCnt)
    }

    setMaxHeightCnt() {
        setTimeout(() => {
            let { clientHeight } = this.refContent.current
            this.setState({cntHeight : clientHeight})
        }, 100);
    }

    toggleHide() {
        this.setState({hide: !this.state.hide}, this.setMaxHeightCnt)
    }

    render() {
        let { title, icon, children, scroll } = this.props
        let { hide, cntHeight } = this.state
        return (
            <div className={`toggle-section ${hide ? '--hide' : ''}`}>
                <div className="_top" onClick={() => this.toggleHide()}>
                    <div className="_top__title">
                        {icon ? <IconComponent icon={icon}/> : ''}
                        <span>{title}</span>
                    </div>
                    <div className="_top__arr">
                        <IconComponent icon={IconList.selectArr}/>
                    </div>
                </div>
                <div className="_cnt" style={{maxHeight: cntHeight + 'px'}}>
                    <div className="_cnt__wrap" ref={this.refContent}>
                        {scroll ?
                            <div className="_cnt__scroll">
                                {children}
                            </div>
                        : children }
                    </div>
                </div>
            </div>
        )
    }
}

export default ToggleSectionComponent
