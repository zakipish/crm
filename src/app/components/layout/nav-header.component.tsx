import * as React from 'react'
import { Link, NavLink } from 'react-router-dom'
import Club from '../../models/club'
import Loader from './loader'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import PopupListEvent from './event/popup-list.event'

interface IProps {
    data: Club[]
    loading: boolean
    onClickEvent?: () => void
    onClickItem?: () => void
}

class NavHeaderComponent extends React.Component<IProps, {}> {

    colorDots = ['#FF6D4A', '#4DA1FF', '#FFD012', '#96C66B', '#FC577A', '#7F17C7', '#00BCD4', '#4072EE', '#F2B6CB', '#4A4A4A']
    colorDotCounter = 0

    getColor = () => {
        this.colorDotCounter++
        if (!this.colorDots[this.colorDotCounter]) {
            this.colorDotCounter = 0
        }
        return this.colorDots[this.colorDotCounter]
    }

    render() {
        let { onClickItem, onClickEvent, loading, data } = this.props
        return (
            <div className="nav-header">
                { loading &&
                    <Loader state={loading}></Loader>
                }
                { !loading ? (
                    <React.Fragment>
                        {data.map((club, i) => { 
                            let { id, name, playgrounds, logo } = club
                            let playgState = (playgrounds && playgrounds.length) ? true : false
                            let logoUrl = logo ? logo.url : "/assets/images/icon-club.svg"
                            return(
                                <div key={i} className="_item" onClick={onClickItem}>
                                    <Link to={`/club/${id}`} className="_itemHead">
                                        <div className={`_itemHead__icon ${logo?"--transparent-background":null}`}>
                                            <img className={logo ? "_img" : "_icon"} src={logoUrl} alt="#"/>
                                        </div>
                                        <div className="_itemHead__tit">{name}</div>
                                    </Link>
                                    { playgState ? (
                                        <div className="_itemList">
                                            <div className="_itemList__tit">Залы</div>
                                            { playgrounds.map((playg, j) => { 
                                                let { id, name } = playg
                                                let num = j + 1
                                                let stateStyle = { backgroundColor: this.getColor() }
                                                return(
                                                    <NavLink
                                                        to={`/playground/dashbord/${id}`} 
                                                        key={j}
                                                        activeClassName="--active"
                                                        className="_itemLink"
                                                        title={playg.name}
                                                    >
                                                        <div className="_itemLink__state" style={stateStyle}></div>
                                                        <div className="_itemLink__name">{`Зал №${j + 1}`}</div>
                                                        { num && <div className="_itemLink__num">{num}</div> }
                                                    </NavLink>
                                                )
                                            }) }
                                        </div> 
                                    ) : (null)}
                                </div>
                            )
                        })}
                    </React.Fragment>
                ) : (null)}
                <div className="nav-header-link">
                    <div 
                        className="nav-header-link__item"
                        onClick={onClickEvent}
                    >
                        <div className="nav-header-link__icon">
                            <IconComponent icon={IconList.navEvent}/>
                        </div>
                        <div className="nav-header-link__text">Кипиш</div>
                    </div>
                    <div className="nav-header-link__item">
                        <div className="nav-header-link__icon">
                            <IconComponent icon={IconList.navUsers}/>
                        </div>
                        <div className="nav-header-link__text">Пользователи</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavHeaderComponent
