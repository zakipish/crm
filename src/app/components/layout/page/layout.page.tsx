import * as React from 'react'
import HeaderComponent from '../header.component'
import EventListScreen from '../../../screens/event/event-list.screen'

interface IProps {
    className?: string
}

class LayoutPage extends React.Component<IProps, {}> {
    
    render() {
        let { children, className } = this.props;
        return (
            <div className={`page ${className ? className : ''}`}>
                <HeaderComponent/>
                <EventListScreen/>
                <div className="page__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default LayoutPage