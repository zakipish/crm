import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    className?: string
    title?: string
    hide?: boolean
    onToggle?: (hide: boolean) => void
    style?: Object
}

class Popup extends React.Component<IProps, {}> {

    get className() {
        let str = 'popup'
        let { className, hide } = this.props
        str += className ? ` ${className}` : ''
        str += hide ? ` --hide` : ''
        return str
    }

    render() {
        let { title, style, children, onToggle } = this.props
        return (
            <div className={this.className}>
                <div className="popup__bg" onClick={() => onToggle(true)}></div>
                <div className="popup__content" style={style}>
                    <div className={`popup__top ${title ? ' --tit' : ''}`}>
                        {title && (
                            <div className="popup__title">{title}</div>
                        )}
                        <div className="popup__cross" onClick={() => onToggle(true)}>
                            <IconComponent icon="cross"/>
                        </div>
                    </div>
                    <div className="popup__body">
                        {children}
                    </div>
                </div>
            </div>
        )
    }
}

export default Popup