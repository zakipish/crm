import * as React from 'react'

interface IProps {
    className?: string
}

class SectionComponent extends React.Component<IProps, {}> {
    render() {
        let { children, className } = this.props
        return (
            <div className={`section ${className ? className : ''}`}>
                {children}
            </div>
        )
    }
}

export default SectionComponent