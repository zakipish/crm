import * as React from 'react'
import { connect } from 'react-redux'

import Club from '../../models/club'
import { reqGet } from '../../core/api'
import { logout } from '../../services/auth.service'

import IconComponent, { IconList } from '../blocks/icon/icon.component'
import NavHeaderComponent from './nav-header.component'
import Popup from './popup'
import UpBalance from '../blocks/balances/up.balance'
import ClubsBalance from '../blocks/balances/clubs.balance'
import PopupListEvent from './event/popup-list.event'

interface IProps {
    user: any
}

interface IState {
    clubs: Club[]
    loadingClubs: boolean
    hideNav: boolean
    hideSearch: boolean
    hideBalanceUp: boolean
    hideClubsBalance: boolean
}

class HeaderComponent extends React.Component<IProps, IState> {

    searchRef: React.RefObject<HTMLLabelElement> = React.createRef()
    clubId: number = 0

    state = {
        clubs: null,
        loadingClubs: true,
        hideNav: true,
        hideSearch: true,
        hideBalanceUp: true,
        hideClubsBalance: true,
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.clickOutSideMobileSearch);
        this.bootstrap()
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.clickOutSideMobileSearch);
    }

    bootstrap = async () => {
        let res = await reqGet('/clubs')
        this.setState({loadingClubs: false, clubs: res.data})
    }

    toggleMobileNav = () => {
        this.setState({hideNav: !this.state.hideNav})
    }

    onClickEventNav = () => {
        this.toggleMobileNav()
        PopupListEvent.toggle()
    }

    toggleMobileSearch = () => {
        this.setState({hideSearch: !this.state.hideSearch})
    }

    toggleBalanceUp = (id = 0) => {
        this.clubId = id
        this.setState({hideBalanceUp: !this.state.hideBalanceUp})
    }

    toggleClubsBalance = () => {
        this.setState({hideClubsBalance: !this.state.hideClubsBalance})
    }

    clickOutSideMobileSearch = (e) => {
        if(!this.state.hideSearch) {
            let selectRef = this.searchRef.current
            if (!selectRef.contains(e.target)) {
                this.toggleMobileSearch()
            }
        }
    }

    onClickLogout = () => {
        logout()
    }

    render() {
        let { clubs, loadingClubs, hideNav, hideSearch, hideBalanceUp, hideClubsBalance } = this.state
        let { name } = this.props.user
        return (
            <div className="header">
                <div className="_top">
                    <div className="_top__btn --rightBord desktopHide"onClick={this.toggleMobileNav}>
                        <img src="/assets/images/menu.png" alt="#"/>
                    </div>
                    <label
                        ref={this.searchRef} 
                        className={`_top__search ${hideSearch ? '--hide' : ''}`}
                    >
                        <div className="_cross" onClick={this.toggleMobileSearch}>
                            <IconComponent icon={IconList.cross}/>
                        </div>
                        <input type="text" placeholder="Поиск имён, контактов, игры и др..."/>
                        <span onClick={() => this.toggleMobileSearch()}>
                            <IconComponent icon={IconList.search}/>
                        </span>
                    </label>
                    <div className="_top__user">{name ? name : ''}</div>
                    <div 
                        className="_top__btn --leftBord"
                        onClick={this.onClickLogout}
                    >
                        <IconComponent icon={IconList.logout}/>
                    </div>
                </div>
                <div className={`_nav ${hideNav ? '--hide' : ''}`}>
                    <div className="_cross desktopHide" onClick={this.toggleMobileNav}>
                        <IconComponent icon={IconList.cross}/>
                    </div>
                    <NavHeaderComponent 
                        data={clubs}
                        loading={loadingClubs}
                        onClickEvent={this.onClickEventNav}
                        onClickItem={this.toggleMobileNav}
                    />
                    {!loadingClubs && (
                        <div className="_nav__footer">
                            {(clubs.length > 1) && (
                                <div className="_balance">
                                    <div 
                                        className="_balance__btn --orange --fill" 
                                        onClick={this.toggleClubsBalance}
                                    >Баланс</div>
                                </div>
                            )}
                            {(clubs.length === 1) && (
                                <div className="_balance">
                                    <div className="_balance__top">
                                        <div className="_balance__tit">Баланс</div>
                                        <div className="_balance__val">{clubs[0].balance}</div>
                                    </div>
                                    <div 
                                        className="_balance__btn --blue" 
                                        onClick={() => this.toggleBalanceUp(clubs[0].id)}
                                    >Пополнить</div>
                                </div>
                            )}
                            <Popup
                                hide={hideClubsBalance}
                                onToggle={this.toggleClubsBalance}
                                style={{width: '330px'}}
                            >
                                <ClubsBalance
                                    list={clubs.map(club => ({
                                        id: club.id,
                                        title: club.name,
                                        img: club.logo ? club.logo.url : null,
                                        balance: club.balance,
                                    }))}
                                    onClickUp={(id) => this.toggleBalanceUp(id)}
                                />
                            </Popup>
                            <Popup
                                hide={hideBalanceUp}
                                onToggle={() => this.toggleBalanceUp()}
                                style={{width: '315px'}}
                                title="Кошелек ZaKipish"
                            >
                                <UpBalance clubId={this.clubId}/>
                            </Popup>
                            <div className="_info">
                                <div className="_info__top">
                                    <div className="_info__logo">
                                        <img src="/assets/images/auth-logo-xs.png" alt="#"/>
                                    </div>
                                    <a href="#" target="_target" className="_info__link">Нужна помощь?</a>
                                </div>
                                <a href="call:88005504107" target="_target" className="_info__phone">8-800-550-41-07</a>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user,
})

export default connect(mapStateToProps)(HeaderComponent)
