import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    title: string
    subtit?: string
    img?: string
    hideImg?: boolean
    datetime?: string
}

class TopHeaderComponent extends React.Component<IProps, {}> {
    render() {
        let { title, subtit, img, hideImg, datetime } = this.props
        return (
            <div className="dashbord-header">
                <div className="_top">
                    {!hideImg ? 
                        <div className="_img">
                            {img ? 
                                <img src={img} alt="#"/> 
                            :
                                <IconComponent icon="pic"/>
                            }
                        </div>
                    : ''}
                    <div>
                        {subtit ? <div className="_subtit">{subtit}</div> : ''}
                        <div className="_tit">{title}</div>
                    </div>
                </div>
                {datetime ? 
                    <div className="_bot">
                        <div className="_datetime">{datetime}</div>
                    </div>
                : ''}
            </div>
        )
    }
}

export default TopHeaderComponent