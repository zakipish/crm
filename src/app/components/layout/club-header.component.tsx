import * as React from 'react'
import { Link } from 'react-router-dom'

import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    title: string
    className?: string
    img?: string
    subtit?: string
    soc?: Array<{
        icon: string | IconList
        url: string
    }>
    btns?: Array<{
        label: string
        url: string
        bg?: string
    }>
    params?: Array<{
        key: string
        value: string
    }>
}
class ClubHeaderComponent extends React.Component<IProps, {}> {

    renderBtns() {
        let { btns } = this.props
        if (btns) {
            return(
                <div className="_btns">
                    {btns.map((el, i) => { return(
                        <Link 
                            key={i} 
                            to={el.url} 
                            className={`_btn ${el.bg ? `--${el.bg}` : ''}`}
                        >{el.label}</Link>
                    )})}
                </div>
            )
        } 
        return null
    }

    renderParams() {
        let { params } = this.props
        if (params) {
            return(
                <ul className="_params">
                    {params.map((el, i) => { return(
                        <li key={i}>
                            <span>{el.key}</span>: <b>{el.value}</b>
                        </li>
                    )})}
                </ul>
            )
        } 
        return null
    }

    renderSoc() {
        let { soc } = this.props
        if (soc) {
            return(
                <div className="_soc">
                    {soc.map((el, i) => { return(
                        <a key={i} href={el.url} target="_blank">
                            <IconComponent icon={el.icon}/>
                        </a>
                    )})}
                </div>
            )
        } 
        return null
    }

    render() {
        let { title, className, img, subtit, btns, params, soc } = this.props
        return (
            <div className={`club-header ${className ? className : ''}`}>
                <div className={`_img ${img?"--transparent-background":null}`}>
                    {img ? 
                        <img src={img} alt="#"/> 

                    :
                        <img className={"_icon"} src={"/assets/images/icon-club.svg"} alt="#"/>
                    }
                </div>
                <div className="_text">
                    <div className="_subtit">{subtit}</div>
                    <div className="_title">{title}</div>
                </div>
                {(btns || params || soc) ?
                    <div className="_right">
                        {this.renderBtns()}
                        {this.renderParams()}
                        {this.renderSoc()}
                    </div>
                : ''}
            </div>
        )
    }
}

export default ClubHeaderComponent