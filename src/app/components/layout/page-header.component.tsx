import * as React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
    title: string
    subtitle?: string
    status?: string
    buttons?: Array<{
        label: string
        url: string
        bg?: string
        onClick?: Function
    }>
}

class PageHeaderComponent extends React.Component<IProps, {}> {

    onClick = (el) => (e) => {
        if (el.onClick) {
            e.preventDefault()
            el.onClick()
        }
    }

    render() {
        let { title, subtitle, buttons, status, children } = this.props
        return (
            <div className="pageHeader">
                <div className="_text">
                    <div className="_title">{title}</div>
                    {subtitle && <div className="_subtitle">{subtitle}</div>}
                </div>
                {status && <div className="_status">{status}</div>}
                {children && <div className="_children">{children}</div> }
                {buttons ?
                    <div className="_btns">
                        {buttons.map((el, i) => { return(
                            <Link
                                to={el.url}
                                key={i}
                                className={`_btn ${el.bg ? `--${el.bg}` : ''}`}
                                onClick={this.onClick(el)}
                            >{el.label
                            }</Link>
                        )})}
                    </div>
                : ''}
            </div>
        )
    }
}

export default PageHeaderComponent
