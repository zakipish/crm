import * as React from 'react'
import { Router } from 'react-router'
import { YMaps, Map, GeoObject, Clusterer, Placemark, ZoomControl, geocode } from 'react-yandex-maps'

interface SectionMapProps {
    point: Point
    cardClassName?: string
    parentRef: React.RefObject<HTMLDivElement>
    pointUrl?: string
    zoom?: number
    onChange?: (point: Point, address: string) => void
}

interface Point {
    lat: number
    lon: number
}

interface State {
    loading: boolean
}

class MapComponent extends React.Component<SectionMapProps, State> {
    ymaps: any

    state: State = {
        loading: true
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ loading: false })
        }, 100)
    }

    handleClick = (e: any) => {
        const { onChange } = this.props
        const coords = e.get('coords')

        const point = {
            lat: +coords[0].toPrecision(6),
            lon: +coords[1].toPrecision(6),
        }

        if (typeof onChange === 'function') {
            this.getAddress(coords)
            .then(address => {
                onChange(point, address)
            })
        }
    }

    getAddress = async (coords: number[]) => {
        if (this.ymaps) {
            return this.ymaps.geocode(coords)
            .then(res => {
                return res.geoObjects.get(0).getAddressLine()
            })
        }

        return ''
    }

    onMapLoaded = (ymaps: any) => {
        this.ymaps = ymaps
    }

    render() {
        const { loading } = this.state
        const { pointUrl, point, parentRef, zoom } = this.props

        if (loading || !point)
            return null

        const coordinates = [point.lat, point.lon]

        const { clientWidth, clientHeight } = parentRef.current
        console.log(coordinates)

        return (
            <YMaps>
                <Map
                    onLoad={this.onMapLoaded}
                    state={{ center: coordinates, zoom: zoom || 10 }}
                    width={clientWidth} height={clientHeight}
                    onClick={this.handleClick}
                    modules={['geocode']}
                >
                    <Placemark
                        geometry={{
                            type: 'Point',
                            coordinates: coordinates
                        }}
                        options={{
                            iconLayout: 'default#image',
                            // iconImageHref: pointUrl || '/assets/images/geopoint-event.png',
                            iconImageHref: '/assets/images/geopoint-event.png',
                            iconImageSize: [50, 61],
                            iconOffset: [-10, -20]
                        }}

                        //modules={['geoObject.addon.balloon', 'geoObject.addon.hint']}
                    />

                    {/*
                    <GeoObject
                        geometry={{
                            type: 'Point',
                            coordinates: geo.coordinates,
                        }}
                        options={{
                            iconLayout: 'default#image',
                            iconImageHref: '/assets/images/geopoint.png',
                            iconImageSize: [79, 97],
                            iconOffset: [-43, -55]
                        }}
                    />
                    */}
                </Map>
            </YMaps>
        )
    }
}

export default MapComponent
