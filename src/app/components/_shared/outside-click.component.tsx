import * as React from 'react'

export interface IOutsideClickProps {
    hide?: boolean
    onToggle?: Function
}

export interface IOutsideClickState {
    hide?: boolean
}

class OutsideClickComponent<P extends IOutsideClickProps, S extends IOutsideClickState> extends React.Component<P, S> {

    outsideRef: React.RefObject<HTMLDivElement>;

    constructor(props) {
        super(props)
        this.onClickOutside = this.onClickOutside.bind(this)
        this.outsideRef = React.createRef()
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
    }

    get hide(): boolean {
        return this.props.hide !== undefined ? this.props.hide : this.state.hide
    }

    checkContains(e): boolean {
        let selectRef = this.outsideRef.current
        return !selectRef.contains(e.target)
    }

    onToggle() {
        if (this.props.onToggle) this.props.onToggle()
    }

    onClickOutside(e) {
        if(!this.hide && this.checkContains(e)) {
            this.onToggle()
        }
    }
    
}

export default OutsideClickComponent