import * as React from 'react'
import { reqPost, reqPatch } from "../../core/api"
import Service from "../../models/service"
import Equipment from "../../models/equipment";
interface IEditPageProps {
}

export interface IEditPageState {
    item: any,
    loading: boolean,
    services?:any,
    equipments?:any,
    options?: any
    min_time_inteval?:boolean
}

class EditPageComponent<P extends IEditPageProps, S extends IEditPageState> extends React.Component<P, S> {

    changeField = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { item } = this.state
        const { name, value } = e.target

        item[name] = value
        this.setState({ item })
    }

    selectSelect = (name: string, value: string) => {
        let { item, min_time_inteval } = this.state
        if (min_time_inteval === false){
            this.setState({min_time_inteval:true});
        }
        item[name] = value
        this.setState({ item })
    }

    //TODO: Сделать эту штуку общей для всех
    optionSelect = (name: string, value: string) => {
        let { item } = this.state
        if (name==="services"){
            item[name].push(new Service({service_id:parseInt(value),price:0,unit:0,payable:true}))
        }else if(name==="equipments"){
            item[name].push(new Equipment({equipment_id:parseInt(value),price:0,unit:0}))
        }
        this.setState({ item })
    }

    onUploadImage(url, name, arrFiles, files) {
        const src = arrFiles ? arrFiles[0] : '#'
        const { item } = this.state
        item[name] = { url: src }
        this.setState({ item })
        this.asyncUploadImage(files[0], name, url)
    }

    asyncUploadImage = async (file, name, url) => {
        let form = new FormData()
        form.append('_method', 'PATCH')
        form.append(name, file)

        try {
            await reqPost(url, form)
        } catch (e) {
            console.log(e.message)
        }
    }

    asyncDeleteImage = (url: string, name: string) => async (id: number) => {
        let form = new FormData()

        form.append('action', `delete_${name}`)
        form.append('_method', 'PATCH')
        form.append('id', id.toString())

        reqPost(url, form)
        .then(response => {
            let { item } = this.state
            item[name] = null
            this.setState({ item })
        })
        .catch(e => console.log(e.message))
    }

    onChangeGallery(arrFiles, name) {
        let { item } = this.state
        item[name] = arrFiles.map(file => ( typeof file === 'string' ? { url: file } : file ))
        this.setState({ item })
        return item[name]
    }

    loadFiles = (url) => async (files) => {
        let form = new FormData()
        form.append('_method', 'PATCH')
        for (let i = 0; i < files.length; i++) {
            form.append('image[]', files[i])
        }
        reqPost(url, form).catch(e => console.log(e.message))
    }

    deleteUploadedImage = (url) => async (id: number) => {
        let form = new FormData()
        form.append('action', 'delete_image')
        form.append('_method', 'PATCH')
        form.append('id', id.toString())
        reqPost(url, form).catch(e => console.log(e.message))
    }

    get imagesCollection() {
        const { item } = this.state

        return item.images.map(image => ({
            order: image.order,
            url: image.url,
            id: image.id
        })).sort((a,b)=>a.order - b.order)
    }

}

export default EditPageComponent
