import * as React from 'react'
import { sendSetPrice } from '../../services/planner-select.service'

import CheckboxControl from '../forms/checkbox.control'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    onHide: () => void
}

interface IState {
    price: number
    state: string
}

class SelectPricePlanner extends React.Component<IProps, IState> {
    
    state = {
        price: 0,
        state: 'open',
    }

    componentDidMount() {
        window.addEventListener('keydown', this.onKeyPress)
    }
    
    componentWillUnmount() {
        window.removeEventListener('keydown', this.onKeyPress)
    }

    onKeyPress = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            this.onSave()
        }
    }

    onSave = () => {
        let { price, state } = this.state
        sendSetPrice(price, state)
    }

    changePrice = (e) => {
        this.setState({price: +e.target.value})
    }

    changeState = (e) => {
        let { checked } = e.target
        let state = checked ? 'closed' : 'open'
        this.setState({state})
    }

    render() {
        let { onHide } = this.props
        let { price, state } = this.state
        return (
            <div className="select-price-popup">
                <div className="_cross" onClick={() => onHide()}>
                    <IconComponent icon="cross"/>
                </div>
                <div className="_title">Создайте цену</div>
                <div className="_body">
                    <div className="_input">
                        <input type="number" value={price} onChange={this.changePrice}/>
                        <div className="_input__label">руб.</div>
                    </div>
                    <div className="_btn" onClick={this.onSave}>ok</div>
                </div>
                <CheckboxControl 
                    title="Блокировать расписание"
                    name="blockPlannig"
                    // checked={state === 'open'}
                    color="white"
                    onChange={this.changeState}
                />
            </div>
        )
    }
}

export default SelectPricePlanner