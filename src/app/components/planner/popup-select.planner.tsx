import * as React from 'react'
import { connect } from 'react-redux'

import * as PlannerSelectService from '../../services/planner-select.service'
import { PlannerSelectState } from '../../reducers/planner-select.reducer'
import { PlannerLayers } from '../../reducers/planner.reducer'

import SelectPricePlanner from './select-price.planner'
import SelectGamePlanner from './select-game.planner'

interface IProps {
    select: PlannerSelectState
    layerKey: PlannerLayers | string
    refScroll: React.RefObject<HTMLDivElement>
    onHide?: () => void
}

class PopupSelectPlanner extends React.Component<IProps, {}> {

    x: number = 0
    y: number = 0
    timerSetClassName
    
    refElem: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props) {
        super(props)
        this.state = {
            price: 0
        }
        this.onKeyPress = this.onKeyPress.bind(this)
        this.setClassName = this.setClassName.bind(this)
    }

    componentDidMount() {
        window.addEventListener('keydown', this.onKeyPress)
        window.addEventListener('resize', this.setClassName)
    }
    
    componentWillUnmount() {
        window.removeEventListener('keydown', this.onKeyPress)
        window.removeEventListener('resize', this.setClassName)
    }

    onKeyPress(e) {
        if (this.props.select.hide === false) {
            if (e.key === 'Escape') {
                e.preventDefault()
                PlannerSelectService.onHide()
            }
        }
    }

    onHide() {
        PlannerSelectService.onHide()
    }

    setClassName() {
        this.timerSetClassName = setTimeout(() => {
            clearTimeout(this.timerSetClassName)
            this.setReverseClass()
            this.setShowClass()
        }, 100)
    }

    setReverseClass() {
        let refElem = this.refElem.current
        let { outXState, outYState } = this.checkBordOut()
        refElem.classList.toggle('--reversPosX', outXState)
        refElem.classList.toggle('--reversPosY', outYState)
    }

    setShowClass() {
        let refElem = this.refElem.current
        let { hide, list, endMoveState } = this.props.select
        let showState = !hide && endMoveState && !!list.length
        refElem.classList.toggle('--show', showState)
        refElem.classList.toggle('--hide', !showState)
    }
    

    checkBordOut() {
        let outXState = false
        let outYState = false

        let refScroll = this.props.refScroll.current
        if (refScroll) {
            let scrollOffsetX = refScroll.scrollLeft + refScroll.clientWidth
            let scrollOffsetY = refScroll.scrollTop + refScroll.clientHeight
            
            let refElem = this.refElem.current
            let elemOffsetX = this.x + refElem.clientWidth
            let elemOffsetY = this.y + refElem.clientHeight
            
            if ( scrollOffsetX < elemOffsetX ) outXState = true
            if ( scrollOffsetY < elemOffsetY ) outYState = true
        }

        return { outXState, outYState }
    }

    get style() {
        let { endCrood } = this.props.select
        if (endCrood) {
            this.x = endCrood.x
            this.y = endCrood.y
        }
        return { left: `${this.x}px`, top: `${this.y}px` }
    }

    render() {
        this.setClassName()
        let { layerKey } = this.props
        return  (
            <div 
                ref={this.refElem} 
                className={`select-popup-planner  --${layerKey}`} 
                style={this.style}
            >
                { layerKey === PlannerLayers.price ? 
                    <SelectPricePlanner onHide={() => this.onHide()}/> 
                : null }
                { layerKey === PlannerLayers.game ? 
                    <SelectGamePlanner onHide={() => this.onHide()}/> 
                : null }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    select: state.plannerSelectState,
    layerKey: state.plannerState.layerKey,
})

export default connect(mapStateToProps)(PopupSelectPlanner)