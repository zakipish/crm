import * as React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import * as PlannerService from '../../services/planner.service'

import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    date: moment.Moment
    showSettingDay: number
}

interface IState {}

interface IDay {
    num: number
    str: string
    now: boolean
    oth: boolean
}

class TableTopDatePlanner extends React.Component<IProps, IState> {

    leftDay: number
    nowDate: moment.Moment
    date: moment.Moment

    constructor(props: IProps) {
        super(props)
        this.state = {}
        this.leftDay = null
        this.nowDate = moment()
    }
    
    renderItems(): React.ReactFragment {
        let itemsElem = []
        let { showSettingDay, date } = this.props
        this.date = moment(date)
        this.setLeftDay()
        for (let i = 0; i < 7; i++) {
            let addDay = i === 0 ? 0 : 1
            let item = this.getDay(addDay)
            let className = '_item'
            className += item.oth ? ' --other' : ''
            className += item.now ? ' --now' : ''
            let btnClassName = '_item__btn'
            btnClassName += showSettingDay === i ? ' --active' : ''
            itemsElem.push(
                <div key={i} className={className}>
                    <div className="_item__wrap">
                        <span 
                            className={btnClassName}
                            onClick={() => this.onToggleSetting(i)}
                        >...</span>
                        <span className="_item__tit">{item.num}</span>
                        <span className="_item__sub">{item.str}</span>
                    </div>
                </div>
            )
        }
        return itemsElem
    }

    onToggleSetting(index) {
        PlannerService.toggleSettingDay(index)
    }

    setLeftDay() {
        this.leftDay = this.date.daysInMonth() - this.date.date()
    }

    getDay(addDay: number): IDay {
        let date = this.date
        if (addDay > 0) date.add(addDay, 'day')
        return {
            num: date.date(),
            str: moment.weekdays(date.day()),
            now: this.checkNowDate(date),
            oth: this.checkOtherDay(date.date()),
        }
    }

    checkNowDate(date: moment.Moment): boolean {
        let state = false
        let dateState = date.date() === this.nowDate.date()
        let monthState = date.month() === this.nowDate.month()
        let yearState = date.year() === this.nowDate.year()
        if (dateState && monthState && yearState) {
            state = true
        }
        return state
    }

    checkOtherDay(num: number): boolean {
        let state = false
        if (this.leftDay < 7) {
            if (num >= 1 && num <= 7) {
                state = true
            }
        }
        return state
    }

    render() {
        return (
            <div className="pl-table-date">
                {/*<div className="_plus">
                    <div className="_plus__icon">
                        <IconComponent icon="plus"/>
                    </div>
                </div>*/}
                <div className="_wrap">
                    {this.renderItems()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    date: state.plannerState.date,
    showSettingDay: state.plannerState.showSettingDay,
})

export default connect(mapStateToProps)(TableTopDatePlanner)