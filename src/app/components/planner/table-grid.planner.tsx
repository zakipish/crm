import * as React from 'react'
import { connect } from 'react-redux'
import Price from '../../models/price'
import DatePrice from '../../models/datePrice'

import { PlannerGrid, PlannerPriceLimit, PlannerPriceColor, PlannerLayers } from '../../reducers/planner.reducer'

interface IProps {
    grid: PlannerGrid
    prices: DatePrice[]
    priceLimit: PlannerPriceLimit
    colors: PlannerPriceColor[]
    layerKey: PlannerLayers | string
}

interface IState {}

class TableGridPlanner extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {}
    }

    renderRows(): React.ReactFragment {
        let rowsElem = []
        let { rowLen } = this.props.grid
        for (let x = 0; x < rowLen; x++) {
            rowsElem.push(
                <div key={x} className="_row">
                    {this.renderCells(x)}
                </div>
            )
        }
        return rowsElem
    }

    renderCells(x: number): React.ReactFragment {
        let linesElem = []
        let { lineLen } = this.props.grid
        for (let y = 0; y < lineLen; y++) {
            let price = this.getPrice(x, y)
            if (price) {
                let className = '_cell'
                if (price.state) className += ` --${price.state}`
                let style = this.getCellColor(price.value)
                linesElem.push(
                    <div key={y} className={className} style={style}>
                        <span>{this.getDisplay(price.state, price.value)}</span>
                    </div>
                )
            }
        }
        return linesElem
    }

    getDisplay(state, value) {
        switch (state) {
            case 'game':
                return 'Игра'        
            case 'empty':
                return 'Без цены'        
            default:
                return value
        }
    }

    getPrice(x, y): Price {
        let { prices } = this.props
        if (prices[x]) {
            if (prices[x].prices[y]) {
                return prices[x].prices[y]
            }
        }
    }

    getCellColor(price) {
        let backgroundColor = ''
        let { colors } = this.props
        let { from, to } = this.props.priceLimit
        if (price >= from && price <= to) {
            let percent = (price - from) / ((to - from) / 100)
            let colorObj = colors.find((el) => el.percent >= percent)
            if (colorObj) {
                backgroundColor = colorObj.color
            }
        } else if (price < from) {
                backgroundColor = colors[0].color
        } else if (price > to) {
            let colorLast = colors.length - 1
                backgroundColor = colors[colorLast].color
        }
        return {backgroundColor}
    }

    get className() {
        let className = 'pl-table-grid'
        let { layerKey } = this.props
        className += ` --${layerKey}Layer`
        return className
    }

    render() {
        let { prices } = this.props
        let loading = !prices || !prices.length
        return (
            <div className={this.className}>
                { !loading ? (
                    this.renderRows()
                ) : (
                    <div className="_row">
                        <div className="_cell"></div>
                    </div>
                ) }
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    grid: state.plannerState.grid,
    prices: state.plannerState.prices,
    priceLimit: state.plannerState.priceLimit,
    colors: state.plannerState.colors,
    layerKey: state.plannerState.layerKey,
})

export default connect(mapStateToProps)(TableGridPlanner)
