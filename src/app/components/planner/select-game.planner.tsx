import * as React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { reqGet } from '../../core/api'
import { minToStr } from './planner.util'
import User from '../../models/user'
import Game from '../../models/game'
import * as PlannerGameService from '../../services/planner-game.service'
import CheckboxControl from '../forms/checkbox.control'
import FieldComponent from '../forms/field.component'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import FieldSelectcomponent from '../forms/field-select.component'

interface IProps {
    game: Game
    reqParam: any
    onHide: () => void
}

interface IState {
    users: User[]
    [x: string]: any
}

export interface SendGameForm {
    sport_id?: number
    description: string
    user: {
        id?: number
        phone: string
        name: string
    }
}

class SelectGamePlanner extends React.Component<IProps, IState> {

    oldGameId: number = 0
    oldIsKipish: boolean = false
    oldPrice: string = ''
    oldDuration: string = ''
    agreement: boolean = false

    state = {
        newUser: false,
        users: [],
        phones: {},
        sports: {},
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate() {
        let { game } = this.props
        if (game) {
            let idLike = this.oldGameId !== game.id
            let durationLike = this.oldDuration !== game.duration
            if (idLike || durationLike) {
                this.oldGameId = game.id
                this.oldIsKipish = !!game.is_kipish
                this.oldPrice = game.price
                this.oldDuration = game.duration
            }
        }
    }

    bootstrap = async () => {
        let playgId = this.props.reqParam.id
        let resUsers: any = await reqGet(`/users`)
        let resSports: any = await reqGet(`/playgrounds/${playgId}/sports?root=1`)
        let users = resUsers.data.map(el => new User(el))
        let phones = {}
        let sports = {}
        users.forEach(user => user.phone && (phones[user.id] = user.phone))
        resSports.data.forEach(sport => sports[sport.id] = sport.name)
        this.setState({users, phones, sports})
    }

    onSave = () => {
        if (this.props.game.is_kipish && !this.agreement) {
            return false
        }
        PlannerGameService.saveGame()
    }

    inputPhone = async (inName: string, inValue: string) => {
        let users = []
        let phones = {}
        let res: any = await reqGet(`/users?phone=${inValue}`)
        if (res.data.length > 0) {
            users = res.data.map(el => new User(el))
            users.forEach(user => user.phone && (phones[user.phone] = user.phone))
        } else {
            phones = {[inValue]: `${inValue}`}
            let { game } = this.props
            game.owner_id = null
            game.owner.id = null
            game.owner.phone = inValue
            PlannerGameService.setGame(game)
        }
        this.setState({users, phones})
    }

    selectPhone = (inName: string, inValue: string) => {
        let { users } = this.state
        let { game } = this.props
        let user = users.find(el => el.id === parseInt(inValue))
        // TODO: костыль
        if (!game) {
            return
        }
        game.owner_id = user.id
        game.owner = user
        PlannerGameService.setGame(game)
    }

    changeField = (e) => {
        let { game } = this.props
        let { name, value } = e.target
        game[name] = value
        PlannerGameService.setGame(game)
    }

    selectSelect = (inName: string, inValue: string) => {
        let { game } = this.props
        // TODO: костыль
        if (!game) {
            return
        }
        game[inName] = inValue
        PlannerGameService.setGame(game)
    }

    getDuration(game: Game) {
        let from = game.fromDate
        let to = game.toDate
        let durationDate = moment.duration(to.diff(from))
        let durationMin =  durationDate.hours() * 60 + durationDate.minutes()
        return minToStr(durationMin)
    }

    onToggleKipish = () => {
        let { game } = this.props
        let newGame = !(game.id > 0)
        if (newGame || !this.oldIsKipish) {
            game.is_kipish = !game.is_kipish
            if(!game.is_kipish) {
                game.price = this.oldPrice
            }
            PlannerGameService.setGame(game)
        }
    }

    onHide = () => {
        let { onHide } = this.props
        this.oldGameId = 0
        this.oldIsKipish = false
        this.oldPrice = ''
        this.agreement = false
        onHide()
    }

    getError = (errors, key) => {
        return errors && errors[key] ? true : false
    }

    get game() {
        let { game } = this.props
        if (!game) game = new Game()
        if (!game.owner) {
            game.owner = new User()
        } else {
            game.owner = new User(game.owner)
        }
        if (!game.sport_id) game.sport_id = null
        if (!game.owner_id) game.owner_id = null
        if (!game.interval) game.interval = ['00:00', '00:00']
        if (!game.errors) game.errors = null
        game.duration = this.getDuration(game)
        return game
    }

    render() {
        let game = this.game
        let { phones, sports } = this.state
        let { errors } = game
        console.log(game, this.oldPrice)
        return (
            <div className="select-game-popup">
                <div className="_top">
                    <div className="_top__tit">Создать игру</div>
                    <div
                        className={`_top__btn ${game.is_kipish ? ' --act' : ''}`}
                        onClick={this.onToggleKipish}
                    >Кипиш</div>
                    <div className="_top__cross" onClick={this.onHide}>
                        <IconComponent icon="cross"/>
                    </div>
                </div>
                {!!game ? (
                    <div className="_body">
                        <div className="_layer">
                            <div className="_title">Начало игры</div>
                            <div className="_times">
                                <span>с</span>
                                <FieldComponent name="from" value={game.interval[0]} disabled={true} />
                                <span>до</span>
                                <FieldComponent name="to" value={game.interval[1]} disabled={true} />
                            </div>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Продолжительность"
                                        name="to"
                                        value={game.duration}
                                        disabled={true}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Стоимость"
                                        name="price"
                                        value={this.oldPrice}
                                        disabled={true}
                                        icons={[{icon: IconList.rub}]}
                                    />
                                </div>
                            </div>
                            <div className="_title">Клиент</div>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__item --col12">
                                    <FieldSelectcomponent
                                        title="Номер телефона"
                                        name="phone"
                                        value={game.owner.id}
                                        placeholder="+7 _  _  _  _  _"
                                        options={phones}
                                        onChange={this.selectPhone}
                                        onSearch={this.inputPhone}
                                        error={this.getError(errors, 'owner')}
                                    />
                                </div>
                                <div className="formRow__item --col12">
                                    <FieldComponent
                                        title="ФИО"
                                        name="name"
                                        value={game.owner.fullName}
                                        disabled={game.owner_id != null}
                                        placeholder="Иванов Иван Иванович"
                                        onChange={this.changeField}
                                        error={this.getError(errors, 'owner')}
                                    />
                                </div>
                            </div>
                            {!game.is_kipish && (
                                <React.Fragment>
                                    <div className="_title">Вид спорта</div>
                                    <div className="formRow formRow--wrap">
                                        <div className="formRow__item --col12">
                                            <FieldSelectcomponent
                                                name="sport_id"
                                                value={game.sport_id + ''}
                                                placeholder="Вид спорта"
                                                options={sports}
                                                onChange={this.selectSelect}
                                                error={this.getError(errors, 'sport_id')}
                                            />
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}
                            <div className="_title">Комментарии</div>
                            <FieldComponent
                                name="description"
                                value={game.description}
                                multiline={true}
                                onChange={this.changeField}
                            />
                            {/* <div className="_checkboxs">
                                <CheckboxControl title="Заявка" name="value" defaultChecked={false} color="white" onChange={() => {}} />
                                <CheckboxControl title="Бронирование" name="value" defaultChecked={false} color="white" onChange={() => {}} />
                                <CheckboxControl title="Оплачено" name="value" defaultChecked={false} color="white" onChange={() => {}} />
                            </div> */}
                        </div>
                        {game.is_kipish && (
                            <div className="_layer">
                                <div className="_offer">
                                    <div className="_offer__tit">Кипиш</div>
                                    <div className="_offer__textTit">Коментарии к игре:</div>
                                    <div className="_offer__text">Приготовить зал за 10 минут до начала игры по своей сути рыбатекст является альтернативой традиционному lorem ipsum,который вызывает у некторых людей недоумение при попытках прочитать рыбу текст</div>
                                </div>
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__item --col6">
                                        <div className="_title">Спорт</div>
                                        <FieldSelectcomponent
                                            name="sport_id"
                                            value={game.sport_id + ''}
                                            placeholder="Вид спорта"
                                            options={sports}
                                            onChange={this.selectSelect}
                                            error={this.getError(errors, 'sport_id')}
                                        />
                                    </div>
                                    <div className="formRow__item --col6">
                                        <div className="_title">Цена</div>
                                        <FieldComponent
                                            name="price"
                                            value={game.price}
                                            onChange={this.changeField}
                                            icons={[{
                                                icon: IconList.rub
                                            }]}
                                            error={this.getError(errors, 'price')}
                                        />
                                    </div>
                                </div>
                                <div className="_title">Описание</div>
                                <FieldComponent
                                    name="kipish_description"
                                    value={game.kipish_description}
                                    multiline={true}
                                    onChange={this.changeField}
                                />
                                <div className="_offer">
                                    <div className="_offer__text">Приготовить зал за 10 минут до начала игры по своей сути рыбатекст является альтернативой Оферта Zakipish, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст</div>
                                    <div className="_checkboxs">
                                        <CheckboxControl
                                            title="Даю согласие на списание денег"
                                            name="value"
                                            defaultChecked={this.agreement}
                                            color="white"
                                            onChange={(e) => this.agreement = !!e.target.value}
                                        />
                                    </div>
                                    {this.getError(errors, 'is_kipish') && (
                                        <div className="_offer__text --red">Недостаночно средств</div>
                                    )}
                                </div>
                            </div>
                        )}
                    </div>
                ) : null }
                <div className="_footer">
                    <div className="_btns">
                        <span className="--blue" onClick={this.onSave}>Сохранить</span>
                        <span onClick={this.onHide}>Отмена</span>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    game: state.plannerSelectState.game,
    reqParam: state.plannerState.reqParam,
})

export default connect(mapStateToProps)(SelectGamePlanner)
