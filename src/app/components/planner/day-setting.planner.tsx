import * as React from 'react'
import moment from 'moment'
import { connect } from 'react-redux'
import DatePrice from '../../models/datePrice'

import DayPopupPlanner from './day-popup.planner'

interface IProps {
    date: moment.Moment
    datePrices: DatePrice[]
}

class DaySettingPlanner extends React.Component<IProps, {}> {

    date: moment.Moment

    renderItems(): React.ReactFragment {
        let itemsElem = []
        let { date, datePrices } = this.props
        let btns = document.querySelectorAll('.pl-table-date ._item')
        if (datePrices.length) {
            this.date = moment(date)
            for (let i = 0; i < 7; i++) {
                let datePrice = datePrices[i]
                let allEmpty = this.checkAllEmpty(datePrice.prices)
                let oldDay = this.checkOldDay()
                let viewState = (allEmpty && !oldDay)
                if (btns[i]) {
                    btns[i].classList.toggle('--hideSetting', !viewState)
                }
                itemsElem.push(
                    <div key={i} className="_item">
                        { viewState && <DayPopupPlanner index={i} datePrice={datePrice}/>}
                    </div>
                )
                this.date.add(1, 'd')
            }
        }
        return itemsElem
    }

    checkAllEmpty = (dayPrice) => {
        let len = dayPrice.length
        let allEmpty = len === dayPrice.filter(el => el.state === 'empty').length
        return allEmpty
    }

    checkOldDay = () => {
        let dateStr = this.date.format('YYYY-MM-DD')
        let nowDateStr = moment().format('YYYY-MM-DD')
        let oldDay = moment(dateStr).isBefore(nowDateStr)
        return oldDay
    }

    render() {
        return (
            <div className="pl-day-setting">
                {this.renderItems()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    date: state.plannerState.date,
    datePrices: state.plannerState.prices,
})

export default connect(mapStateToProps)(DaySettingPlanner)
