import * as React from 'react'
import { connect } from 'react-redux'
import DatePrice from '../../models/datePrice'
import { sendDaySchedules } from '../../services/planner-select.service'

interface IProps {
    index: number
    datePrice: DatePrice
    lineLen: number
    showSettingDay: number
}

interface IState {
    data: {
        timeStart: number
        timeEnd: number
    }
}

class DayPopupPlanner extends React.Component<IProps, IState> {

    state = {
        data: {
            timeStart: 0,
            timeEnd: 0,
        }
    }

    getFirstTime = (key) => {
        let { datePrice } = this.props
        let lastPrice = datePrice.prices[0]
        return lastPrice[key]
    }

    getLastTime = (key) => {
        let { datePrice } = this.props
        let lastPrice = datePrice.prices[datePrice.prices.length - 1]
        return lastPrice[key]
    }

    getTimeEndList() {
        
    }

    changeField = (e) => {
        let { data } = this.state
        let { name, value } = e.target
        let isNameStart = (name === 'timeStart')
        let isNameEnd = (name === 'timeEnd')
        let isStartValue = (+value < data.timeStart)
        let isEndValue = (+value > data.timeEnd)
        if (isNameStart && isEndValue) {
            data.timeEnd = value
        }
        if (isNameEnd && isStartValue) {
            data.timeStart = value
        }
        data[name] = value
        this.setState({data})
    }

    onSave = () => {
        let { datePrice } = this.props
        let { date, prices } = datePrice
        let {timeStart, timeEnd} = this.state.data
        let from = prices[timeStart].timeStart
        let to = prices[timeEnd].timeEnd
        sendDaySchedules(date, from, to)
    }

    get className() {
        let className = 'pl-day-popup'
        let { index, showSettingDay } = this.props
        if (index !== showSettingDay) className += ' --hide'
        return className
    }

    render() {
        let { datePrice } = this.props
        let { data } = this.state
        return (
            <div className={this.className}>
                <div className="_title">Режим работы</div>
                <div className="_interval">
                    <div className="_interval__item">
                        <select name="timeStart" value={data.timeStart} onChange={this.changeField}>
                            {datePrice.prices.map((price, i) => (
                                <option key={i} value={i}>{price.timeStart}</option>
                            ))}
                        </select>
                    </div>
                    <div className="_interval__item">
                        <select name="timeEnd" value={data.timeEnd} onChange={this.changeField}>
                            {datePrice.prices.map((price, i) => (
                                <option key={i} value={i}>{price.timeEnd}</option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="_btn" onClick={this.onSave}>Сохранить</div>   
            </div>
        )
    }
}


const mapStateToProps = state => ({
    lineLen: state.plannerState.grid.lineLen,
    showSettingDay: state.plannerState.showSettingDay,
})

export default connect(mapStateToProps)(DayPopupPlanner)

