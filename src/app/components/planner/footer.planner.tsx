import * as React from 'react'
import { connect } from 'react-redux'

import { PlannerPriceColor, PlannerPriceLimit, PlannerLayers } from '../../reducers/planner.reducer'

interface IProps {
    colors: PlannerPriceColor[]
    priceLimit: PlannerPriceLimit
    layerKey: PlannerLayers | string
}

class FooterPlanner extends React.Component<IProps, {}> {

    getColorLineStyle() {
        let { colors } = this.props
        let gradient = ''
        colors.forEach(el => {
            gradient += ` ,${el.color} ${el.percent}%`
        });
        return { 
            background: `linear-gradient(to right${gradient})`
        }
    }

    getColorLineLabels() {
        let { from, to } = this.props.priceLimit
        let percent = (to - from) / 100
        return [
            { percent: 0, label: from },
            this.getLabelObj(percent, 25, from),
            this.getLabelObj(percent, 50, from),
            { percent: 100, label: to },
        ]
    }

    getLabelObj(p1, p2, from) {
        return { percent: p2, label: p1 * p2 + from }
    }
    
    getColorWrapClass() {
        let className = 'pl-footer-color'
        let { layerKey } = this.props
        if (layerKey !== PlannerLayers.price) {
            className += ' --hide'
        }
        return className
    }

    render() {
        let colorLineStyle = this.getColorLineStyle()
        let colorLinelables = this.getColorLineLabels()
        let classNameColorLine = this.getColorWrapClass()
        return (
            <div className="pl-footer">
                <div className={classNameColorLine}>
                    <div 
                        className="_line"
                        style={colorLineStyle}
                    ></div>
                    <ul className="_labels">
                        {colorLinelables.map((el, i) => (
                            <li 
                                key={i}
                                style={{left: el.percent + '%'}}
                            >{el.label}</li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    colors: state.plannerState.colors,
    priceLimit: state.plannerState.priceLimit,
    layerKey: state.plannerState.layerKey,
})

export default connect(mapStateToProps)(FooterPlanner)
