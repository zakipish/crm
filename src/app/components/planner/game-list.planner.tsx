import * as React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import * as PlannerAction from '../../actions/planner.actions'
import { PlannerLayers, PlannerGrid } from '../../reducers/planner.reducer'
import Game from '../../models/game'

import GameItemPlanner from './game-item.planner'


interface IProps {
    games: Game[]
    layerKey: PlannerLayers | string
    grid: PlannerGrid
    date: moment.Moment
    setGames: (games: Game[]) => void
    setGameId: (id: number) => void
}

class GameListPlanner extends React.Component<IProps, {}> {

    weekDay: number = 0;
    timeSection: number = 0;
    durationMin: number = 0;

    constructor(props) {
        super(props)
        this.toggleViewGames = this.toggleViewGames.bind(this)
    }

    renderGames() {
        let gamesElem = []
        let { games } = this.props
        games.forEach((el, i) => {
            let game = new Game(el)
            this.setGameSpace(game)
            let space = this.getGameSpace()
            gamesElem.push(
                <GameItemPlanner 
                    key={i}
                    space={space}
                    color="blue"
                    game={game}
                    index={i}
                    weekDay={this.weekDay}
                    toogleView={this.toggleViewGames}
                />
            )
        })
        return gamesElem
    }

    getGameSpace() {
        let { cellWidth, cellHeight, durationMin } = this.props.grid
        let minuteHeight = cellHeight / durationMin
        return {
            y: this.timeSection * minuteHeight,
            x: this.weekDay * cellWidth,
            width: cellWidth,
            height: this.durationMin * minuteHeight,
        }
    }

    setGameSpace(game: Game) {
        let from = game.fromDate
        let to = game.toDate

        let fromDay = from.date()
        let propsDay = this.props.date.date()

        let monthState = from.month() === this.props.date.month()
        if (!monthState) fromDay += this.props.date.daysInMonth()

        this.weekDay = fromDay - propsDay
        this.timeSection = from.hour() * 60 + from.minute()

        let durationDate = moment.duration(to.diff(from))
        this.durationMin = durationDate.hours() * 60 + durationDate.minutes()
    }
    

    toggleViewGames(index, height) {
        let { games, setGames } = this.props
        
        let { cellHeight, durationMin } = this.props.grid
        let minuteHeight = cellHeight / durationMin

        let game = new Game(games[index])
        let from = game.fromDate
        let to = moment(from)
        let addTo = height * minuteHeight
        to.add(addTo, 'm')
        
        let viewState = games[index].viewState
        let newGames = []
        
        if (viewState === 'show') {
            newGames = games.map((el, i) => {
                el.viewState = null
                return el
            })
        } else {
            newGames = games.map((el, i) => {
                el = new Game(el)
                el.viewState = null
                if (i === index) {
                    el.viewState = 'show'
                } else {
                    let elFrom = el.fromDate
                    let elTo = el.toDate
                    let dayState = from.date() === elFrom.date()
                    if (dayState) {
                        let durationState = moment(elTo).isBetween(from, to)
                        if (durationState) {
                            el.viewState = 'under'
                        }
                    }
                }
                return el
            })
        }
        setGames(newGames)
    }

    get className() {
        let className = 'pl-game-list'
        let { layerKey } = this.props
        if (layerKey !== PlannerLayers.game) {
            className += ' --hide'
        }
        return className
    }

    render() {
        return (
            <div className={this.className}>
                {this.renderGames()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    games: state.plannerState.games,
    layerKey: state.plannerState.layerKey,
    grid: state.plannerState.grid,
    date: state.plannerState.date,
})

const mapDispatchToProps = dispatch => ({
    setGames: (games) => dispatch(PlannerAction.setGames(games)),
})

export default connect(mapStateToProps, mapDispatchToProps)(GameListPlanner)
