import * as React from 'react'
import moment from 'moment'
import { connect } from 'react-redux'

import { minToStr } from './planner.util'
import { PlannerGrid } from '../../reducers/planner.reducer'

interface IProps {
    grid: PlannerGrid
}

interface IState {}

class TableTimePlanner extends React.Component<IProps, IState> {

    cellLen: number

    constructor(props: IProps) {
        super(props)
        this.state = {}
    }

    renderItems(): React.ReactFragment {
        let itemsElem = []
        let { timeStart, lineLen, durationMin } = this.props.grid
        let dateStr = moment().format('YYYY-MM-DD')
        let dateStart = moment(`${dateStr} ${timeStart}`)
        let min = dateStart.hours() * 60 + dateStart.minute()
        for (let i = 0; i < lineLen; i++) {
            itemsElem.push(<div key={i} className="_item">{minToStr(min)}</div>)  
            min += durationMin
        } 
        return itemsElem
    }

    render() {
        return (
            <div className="pl-table-time">
                {this.renderItems()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    grid: state.plannerState.grid
})

export default connect(mapStateToProps)(TableTimePlanner)