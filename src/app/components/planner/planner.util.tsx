export const minToStr = (min) => {
    let h = Math.floor(min / 60) + ''
    let m = min % 60 + ''
    h = h.length < 2 ? `0${h}` : h
    m = m.length < 2 ? `0${m}` : m
    return `${h}:${m}`
}

export const selectedPriceReducer = (resItem, item) => {
    let { durations } = item
    let itemPrice = durations.reduce((resDur, dur) => {
        return resDur + dur.price
    }, 0)
    return resItem + itemPrice
}