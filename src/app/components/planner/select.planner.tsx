import * as React from 'react'
import moment from 'moment'
import { connect } from 'react-redux'
import DatePrice from '../../models/datePrice'
import Price from '../../models/price'

import * as PlannerSelectService from '../../services/planner-select.service'
import * as PlannerService from '../../services/planner.service'
import { PlannerGrid, PlannerLayers } from '../../reducers/planner.reducer'
import { PlannerSelectDay } from '../../reducers/planner-select.reducer'

import PopupSelectPlanner from './popup-select.planner'

interface IProps {
    refScroll: React.RefObject<HTMLDivElement>
    layerKey: PlannerLayers | string
    date: moment.Moment
    prices: DatePrice[]
    hide: boolean
    grid: PlannerGrid
}

export interface SpaceCrood {
    x1: number
    x2: number
    y1: number
    y2: number
}

enum DirectV {
    up = 'up',
    down = 'down',
}

enum DirectH {
    left = 'left',
    right = 'right',
}

class SelectPlanner extends React.Component<IProps, {}> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()
    refBlock: React.RefObject<HTMLDivElement> = React.createRef()
    refPopups: React.RefObject<HTMLDivElement> = React.createRef()

    selectStateMove: boolean

    startCrood = { x: 0, y: 0 }
    endCrood = { x: 0, y: 0 }
    space = { left: 0, top: 0, right: 0, bottom: 0 }
    
    wrapOffset = { x: 0, y: 0 }
    cursorCrood = { x: 0, y: 0 }

    scrollTimer: any
    scrollStateMove: any
    scrollOffset = { x: 0, y: 0 }
    scrollOut = { x: 0, y: 0 }

    directV: DirectV = null
    directH: DirectH = null
    blocking: number = null

    spaceCrood: SpaceCrood

    minBlockHeight = 1

    selectedList: PlannerSelectDay[] = []

    componentDidMount() {
        this.onResize()
        setTimeout(this.onResize, 1500)
        window.addEventListener('keydown', this.onKeyPress)
        window.addEventListener('mouseup', this.onCursorUp)
        window.addEventListener('mousemove', this.setCursorCrood)
        window.addEventListener('resize', this.onResize)
        setTimeout(() => {
            let refScroll = this.props.refScroll.current
            refScroll.addEventListener('mouseenter', this.onCursorEnter)
            refScroll.addEventListener('mouseleave', this.onCursorLeave)
        }, 100)
    }
    
    componentWillUnmount() {
        window.removeEventListener('keydown', this.onKeyPress)
        window.removeEventListener('mouseup', this.onCursorUp)
        window.removeEventListener('mousemove', this.setCursorCrood)
        window.removeEventListener('resize', this.onResize)
    }

    onKeyPress = (e) => {
        if (e.key === 'Escape') {
            e.preventDefault()
            this.hideSelect()
        }
    }

    onResize = () => {
        setTimeout(() => {
            this.setCellSize()
            this.setOffsetCrood()
        }, 100)
    }

    setCellSize = () => {
        let { clientHeight, clientWidth } = document.querySelector('.pl-table-grid ._cell')
        let cellWidth = clientWidth + 1
        let cellHeight = clientHeight + 1
        PlannerService.setCellSize(cellWidth, cellHeight)
    }

    setOffsetCrood = () => {
        let rectWrap = this.refWrap.current.getBoundingClientRect()
        this.wrapOffset.x = rectWrap.left + window.pageXOffset
        this.wrapOffset.y = rectWrap.top + window.pageYOffset
        let rectSc = this.props.refScroll.current.getBoundingClientRect()
        this.scrollOffset.x = rectSc.left + window.pageXOffset
        this.scrollOffset.y = rectSc.top + window.pageYOffset
    }

    onCursorDown = (e) => { 
        let refPopup = this.refPopups.current
        if (refPopup.contains(e.target) === false) {
            this.initSpace(e)
            this.transformSpace()
        }
    }

    onCursorUp = (e) => { 
        this.onSelected()
        if (this.selectStateMove) {
            PlannerSelectService.onEndMove({
                x: this.endCrood.x,
                y: this.endCrood.y,
            })
        }
        this.selectStateMove = false 
        this.scrollStateMove = false
        let refWrap = this.refWrap.current
        if (refWrap.contains(e.target) === false) {
            this.hideSelect()
        }
    }

    onCursorMove = (e) => {
        if (this.selectStateMove) {
            this.setSpace(e)
            this.transformSpace()
            PlannerSelectService.onStartMove()
        }
    }
    
    onCursorLeave = (e) => {
        if (this.selectStateMove) {   
            this.scrollStateMove = true
            this.setScroll()
        }
    }
    
    onCursorEnter = (e) => {
        this.scrollStateMove = false
    }

    initSpace = (e) => {
        let { clientHeight, clientWidth } = this.refWrap.current
        let { scrollTop, scrollLeft } = this.props.refScroll.current
        this.selectStateMove = true 
        this.space.left = this.startCrood.x = e.pageX + scrollLeft - this.wrapOffset.x
        this.space.top = this.startCrood.y = e.pageY + scrollTop - this.wrapOffset.y
        this.space.right = clientWidth - (e.pageX + scrollLeft - this.wrapOffset.x)
        this.space.bottom = clientHeight - (e.pageY + scrollTop - this.wrapOffset.y)

        let oneRow = this.checkOneRow()
        if (oneRow === true) {
            this.space.right = this.startCrood.x = clientWidth
        }
    }

    setSpace = (e) => {
        let { scrollTop, scrollLeft } = this.props.refScroll.current
        let { clientHeight, clientWidth } = this.refWrap.current
        let x = this.endCrood.x = e.pageX + scrollLeft - this.wrapOffset.x
        let y = this.endCrood.y = e.pageY + scrollTop - this.wrapOffset.y
        if ( x > this.startCrood.x ) {
            this.space.right = clientWidth - x
            this.directH = DirectH.left
        } else {
            this.space.left = x
            this.directH = DirectH.right
        }
        if ( y > this.startCrood.y ) {
            this.space.bottom = clientHeight - y
            this.directV = DirectV.down
        } else {
            this.space.top = y
            this.directV = DirectV.up
        }
    }

    transformSpace = () => {
        let refBlock = this.refBlock.current
        refBlock.style.top = this.space.top + 'px'
        refBlock.style.left = this.space.left + 'px'
        refBlock.style.right = this.space.right + 'px'
        refBlock.style.bottom = this.space.bottom + 'px'

        let { clientHeight } = refBlock
        if (clientHeight < this.minBlockHeight) {
            this.hideSelect()
        } else {
            this.showSelect()
        }
    }

    setScroll = () => {
        this.scrollTimer = setInterval(() => {
            if (this.scrollStateMove) {
                this.setScrollOut()
                this.props.refScroll.current.scrollTo(this.scrollOut.x, this.scrollOut.y)
            } else {
                clearImmediate(this.scrollTimer)
            }
        }, 5)
    }

    setScrollOut = () => {
        let { scrollTop, scrollLeft, clientHeight, clientWidth } = this.props.refScroll.current
        this.scrollOut.x = scrollLeft
        this.scrollOut.y = scrollTop
        let step = 1
        if ( this.cursorCrood.x < this.scrollOffset.x ) {
            this.scrollOut.x -= step
        } 
        else if ( this.cursorCrood.x > this.scrollOffset.x + clientWidth ) {
            this.scrollOut.x += step
        }
        if ( this.cursorCrood.y < this.scrollOffset.y ) {
            this.scrollOut.y -= step
        } 
        else if ( this.cursorCrood.y > this.scrollOffset.y + clientHeight ) {
            this.scrollOut.y += step
        }
    }

    setCursorCrood = (e) => {
        this.cursorCrood.x = e.clientX
        this.cursorCrood.y = e.clientY
    }

    hideSelect = () => {
        this.refBlock.current.classList.remove('--show')
        PlannerSelectService.onHide()
    }
    
    showSelect = () => {
        this.onSelectCell()
        this.refBlock.current.classList.add('--show')
        PlannerSelectService.onShow()
    }

    onSelectCell = () => {
        let { cellWidth, cellHeight } = this.props.grid
        let { offsetTop, offsetLeft, offsetHeight, offsetWidth } = this.refBlock.current
        let x1 = Math.floor(offsetLeft / cellWidth)
        let x2 = Math.floor((offsetLeft + offsetWidth) / cellWidth)
        let y1 = Math.floor(offsetTop / cellHeight)
        let y2 = Math.floor((offsetTop + offsetHeight) / cellHeight)
        this.spaceCrood = { x1, x2, y1, y2 }
        this.transformSelectCell()
    }
    
    transformSelectCell = () => {
        this.clearSelectCell()
        let space = this.spaceCrood
        let refWrap = this.refWrap.current
        let rows = refWrap.querySelectorAll('._row')
        for (let x = space.x1; x <= space.x2; x++) {
            let row = rows[x]
            row.classList.add('rowSelect')
            this.addItemSelected(x)
            if (this.directV === DirectV.down) {
                for (let y = space.y2; y >= space.y1; y--) {
                    this.setCell(row, x, y)
                }
            } else if (this.directV === DirectV.up) {
                for (let y = space.y1; y <= space.y2; y++) {
                    this.setCell(row, x, y)
                }
            }
        }
    }
    
    setCell = (row, x, y) => {
        let cell = row.children[y]
        if (!cell) return
        let stateBlock = this.checkBlockingCell(cell)
        if (stateBlock) {            
            this.blocking = y
            this.addDurationSelected()
        } else {
            let { prices } = this.props
            let price = prices[x].prices[y]
            let dateStr = prices[x].date
            
            let oneRow = this.checkOneRow()
            let oldDay = this.checkOldDay(dateStr)
            let notBlocking = this.blocking === null
            let add = (!oneRow || oneRow && notBlocking) && !oldDay
            
            if (add) {
                cell.classList.add('cellSelect')
                this.setDurationSelected(price.timeStart, price.timeEnd, price.value)
            }
        }
    }

    checkOldDay = (dateStr) => {
        let nowDateStr = moment().format('YYYY-MM-DD')
        let oldDay = moment(dateStr).isBefore(nowDateStr)
        return oldDay
    }

    checkBlockingCell = (cell) => {
        let { layerKey } = this.props
        let state = false
        let stateClosed = cell.classList.contains('--closed')
        let stateGame = cell.classList.contains('--game')
        let stateEmpty = cell.classList.contains('--empty')
        if (layerKey === PlannerLayers.price && stateGame) {
            state = true
        }
        if (layerKey === PlannerLayers.game && (stateGame || stateClosed || stateEmpty)) {
            state = true
        }
        return state
    }

    checkOneRow() {
        let state = false
        let { layerKey } = this.props
        if (layerKey === PlannerLayers.game) {
            state = true
        }
        return state
    }

    clearSelectCell = () => {
        this.selectedList = []
        this.blocking = null
        let rows = document.querySelectorAll('.rowSelect')
        rows.forEach(el => el.classList.remove('rowSelect'))
        let cells = document.querySelectorAll('.cellSelect')
        cells.forEach(el => el.classList.remove('cellSelect'))
    }

    addItemSelected = (x) => {
        let date = moment(this.props.date).add(x, 'd').format('YYYY-MM-DD')
        this.selectedList.push({ 
            date, 
            durations: [{ from: null, to: null, price: 0 }] 
        })
    }

    addDurationSelected = (fromIn = null, toIn = null, price = 0) => {
        let lastItem = this.selectedList.length - 1
        let { durations } = this.selectedList[lastItem]
        let lastDuration = durations.length - 1
        let duration = durations[lastDuration]
        let { from, to } = duration
        if (from && to) {
            durations.push({ from: fromIn, to: toIn, price })
        }
    }

    setDurationSelected = (fromIn = null, toIn = null, price = 0) => {
        let lastItem = this.selectedList.length - 1
        let { durations } = this.selectedList[lastItem]
        let lastDuration = durations.length - 1
        let duration = durations[lastDuration]
        let { from, to } = duration
        if (this.directV === DirectV.down) {
            if (from !== null) {
                duration.from = fromIn
            }
            if (to === null) {
                duration.to = toIn
                duration.from = fromIn
            }
        } else {
            if (to !== null) {
                duration.to = toIn
            }
            if (from === null) {
                duration.from = fromIn
                duration.to = toIn
            }
        }
        duration.price += price
    }

    selectedFilter = () => {
        this.selectedList = this.selectedList
            .map(item => { 
                item.durations = item.durations
                    .filter(duration => { 
                        return duration.from !== null && duration.to !== null 
                    })
                return item
            })
            .filter(item => item.durations.length > 0)
    }

    onSelected = () => {
        if (this.selectStateMove) {
            let { clientHeight } = this.refBlock.current
            if (clientHeight > this.minBlockHeight) {
                this.selectedFilter()
                PlannerSelectService.onSetList(this.selectedList, this.spaceCrood)
            }
        }
    }

    render() {
        let { children } = this.props
        return (
            <div 
                className="pl-table-select-wrap" 
                ref={this.refWrap}
                onMouseDown={this.onCursorDown}
                onMouseMove={this.onCursorMove}
            >
                {children}
                <div className="pl-table-select" ref={this.refBlock}></div>
                <div ref={this.refPopups}>
                    <PopupSelectPlanner refScroll={this.props.refScroll}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    date: state.plannerState.date,
    prices: state.plannerState.prices,
    hide: state.plannerSelectState.hide,
    grid: state.plannerState.grid,
    layerKey: state.plannerState.layerKey,
})

export default connect(mapStateToProps)(SelectPlanner)