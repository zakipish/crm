import * as React from 'react'
import * as PlannerGameService from '../../services/planner-game.service'
import Game from '../../models/game'

import IconComponent from '../blocks/icon/icon.component'
import config from "../../core/config";

interface IProps {
    space: any
    color?: string
    game: Game
    index: number
    weekDay: number
    toogleView?: (index: number, height: number) => void
}

export enum GameItemViewStates {
    show = 'show',
    under = 'under',
}

class GameItemPlanner extends React.Component<IProps, {}> {

    maxHeight: number = 0

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()
    refBtns: React.RefObject<HTMLDivElement> = React.createRef()
    refToolTip: React.RefObject<HTMLDivElement> = React.createRef()

    componentDidMount() {
        this.setSize()
        window.addEventListener('resize', this.setSize)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.setSize)
    }

    setSize = () => {
        setTimeout(() => {
            let refWrap = this.refWrap.current
            if (refWrap) {
                let { children } = this.refWrap.current
                this.maxHeight = 0
                Array.from(children).forEach(el => {
                    this.maxHeight += el.clientHeight
                })
                if (this.maxHeight < this.props.space.height) {
                    this.maxHeight = this.props.space.height
                    refWrap.style.height = refWrap.style.maxHeight = this.props.space.height + 'px'
                } else {
                    refWrap.style.maxHeight = this.maxHeight + 'px'
                }
            }
        }, 200)
    }

    onMouseMove = (e) => {
        let { layerX, layerY } = e.nativeEvent
        let refToolTip = this.refToolTip.current
        refToolTip.style.top = layerY + 'px'
        refToolTip.style.left = layerX + 'px'
    }

    onToggleView = (e) => {
        if (!this.refBtns.current.contains(e.target)) {
            let { index, toogleView } = this.props
            if (toogleView) toogleView(index, this.maxHeight)
        }
    }

    onEditShow = () => {
        let { game, space } = this.props
        let crood = { 
            x: space.x, 
            y: (space.y + this.maxHeight), 
        }
        PlannerGameService.showSetGame(game, crood)
    }

    getDurationStr = (game: Game) => {
        let from = game.fromDate
        let to = game.toDate
        let hFrom = this.twoNum(from.hour())
        let mFrom = this.twoNum(from.minute())
        let hTo = this.twoNum(to.hour())
        let mTo = this.twoNum(to.minute())
        return `${hFrom}:${mFrom} - ${hTo}:${mTo}`
    }

    twoNum = (num) => {
        return num <= 9 ? `0${num}` : num
    }

    get className() {
        let className = 'pl-game-item'
        let { color, weekDay, game } = this.props
        if (color) className += ` --${color}`
        if (weekDay > 4) className += ` --toolTipReverse`
        if (game.viewState) {
            className += ` --${game.viewState}`
        } else {
            className += ` --hide`
        } 
        return className
    }

    get style() {
        let { space } = this.props
        return {
            top: space.y + 'px',
            left: space.x + 'px',
            width: space.width + 'px',
            maxHeight: space.height + 'px',
        }
    }

    render() {
        this.setSize()
        let { game } = this.props
        let duration = this.getDurationStr(game)
        return (
            <div className={this.className} style={this.style}>
                {console.log(config)}
                <div 
                    className="_wrap" 
                    ref={this.refWrap}
                    onMouseMove={this.onMouseMove}
                    onClick={this.onToggleView}
                >
                    <div className="_top">
                        <div className="_top__tit">{duration}</div>
                        <div className="_top__sub">
                            <span>{game.price}</span> руб.
                        </div>
                    </div>
                    <div className="_hdr">
                        <div className="_hdr__avatar">
                            <IconComponent icon="avatar"/>
                        </div>
                        { game.owner &&
                            <div className="_hdr__body">
                                <div className="_hdr__tit">{game.owner.name}</div>
                                <div className="_hdr__sub">{game.owner.phone}</div>
                            </div>
                        }
                    </div>
                    <div className="_body">
                        <div className="_body__tit">Коментарии к игре:</div>
                        <div className="_body__text">{game.description}</div>
                    </div>
                    <div className="_btns" ref={this.refBtns}>
                        <div className="_btns__item" onClick={this.onEditShow}>Редактировать</div>
                        <a className="_btns__item" href={config.front_domain + `/game/${game.id}`} target="_blank">Игра</a>
                    </div>
                </div>
                <div className="_toolTip" ref={this.refToolTip}>
                    <span>Игра оплачена</span>
                </div>
            </div>
        )
    }
}

export default GameItemPlanner