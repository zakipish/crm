import * as React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import * as PlannerAction from '../../actions/planner.actions'
import * as PlannerService from '../../services/planner.service'

import { PlannerLayers } from 'src/app/reducers/planner.reducer'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import CheckboxControl from '../forms/checkbox.control'

interface IProps {
    date: moment.Moment
    layerKey: PlannerLayers | string
    onSetLayer: (key) => void
}

interface IDateProps {
    week: number
    year: number
    strMonth: string
}

class TableTopHeadPlanner extends React.Component<IProps, {}> {

    setLayer(key: PlannerLayers | string) {
        this.props.onSetLayer(key)
    }

    setWeek(addWeek) {
        PlannerService.setDate(addWeek, 'w')
    }
    
    setMonth(addMonth) {
        PlannerService.setDate(addMonth, 'M')
    }

    togglePrice() {
        PlannerService.toggleViewPrice()
    }
    
    get dateProps(): IDateProps {
        let { date } = this.props
        return { 
            week: date.week(), 
            year: date.year(), 
            strMonth: moment.months(date.month()),
        }
    }

    getClassNameTab(key): string {
        let className = '_tab__item'
        if (key === this.props.layerKey) {
            className += ' --active'
        }
        return className
    }

    render() {
        let { week, year, strMonth } = this.dateProps
        return (
            <div className="pl-table-head">
                <div className="_month">
                    <div className="_month__tit">{strMonth} {year}</div>
                    <div className="_month__arr" onClick={() => this.setMonth(-1)}>
                        <IconComponent icon={IconList.slideLeft}/>
                    </div>
                    <div className="_month__arr" onClick={() => this.setMonth(1)}>
                        <IconComponent icon={IconList.slideRight}/>
                    </div>
                </div>
                <div className="_week">
                    <div className="_week__arr" onClick={() => this.setWeek(-1)}>
                        <IconComponent icon={IconList.slideLeft}/>
                    </div>
                    <div className="_week__tit">{week} неделя</div>
                    <div className="_week__arr" onClick={() => this.setWeek(1)}>
                        <IconComponent icon={IconList.slideRight}/>
                    </div>
                </div>
                <div className="_wrap">
                    <div className="_option">
                        <CheckboxControl 
                            title="Показать цены"
                            name="viewPrice"
                            defaultChecked={true}
                            color="blue"
                            invert="true"
                            onChange={() => this.togglePrice()}
                        />
                    </div>
                    <div className="_tab">
                        <span 
                            className={this.getClassNameTab('price')}
                            onClick={() => this.setLayer('price')}
                        >Цены</span>
                        <span 
                            className={this.getClassNameTab('game')}
                            onClick={() => this.setLayer('game')}
                        >Игры</span>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    date: state.plannerState.date,
    layerKey: state.plannerState.layerKey,
})

const mapDispatchToProps = dispatch => ({
    onSetLayer: (layerKey) => dispatch(PlannerAction.setLayer(layerKey)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TableTopHeadPlanner)
