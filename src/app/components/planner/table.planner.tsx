import * as React from 'react'
import { connect } from 'react-redux'

import * as PlannerService from '../../services/planner.service'
import * as PlannerGameService from '../../services/planner-game.service'

import { PlannerView } from '../../reducers/planner.reducer'
import TableTopHeadPlanner from './table-top-head.planner'
import TableTopDatePlanner from './table-top-date.planner'
import DaySettingPlanner from './day-setting.planner'
import TableTimePlanner from './table-time.planner'
import TableScrollPlanner from './table-scroll.planner'

interface IProps {
    view: PlannerView
    hideSelect: boolean
}

class TablePlanner extends React.Component<IProps, {}> {

    refTable: React.RefObject<HTMLDivElement>

    constructor(props: IProps) {
        super(props)

        PlannerService.initDate()
        PlannerService.initGridSize({})

        this.refTable = React.createRef()
    }

    componentDidMount() {
        PlannerService.reqPrices()
        PlannerGameService.reqGames()
    }

    getClassName() {
        let classStr = 'pl-table'
        let { view, hideSelect } = this.props
        if (view.hideCellPrice) {
            classStr += ' --hideCellPrice'
        }
        if (hideSelect) {
            classStr += ' --hideSelect'
        }
        return classStr
    }

    render() {
        return (
            <div className={this.getClassName()} ref={this.refTable}>
                <div className="pl-table-top">
                    <TableTopHeadPlanner/>
                    <TableTopDatePlanner/>
                </div>
                <DaySettingPlanner/>
                <TableTimePlanner/>
                <TableScrollPlanner refTable={this.refTable}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    view: state.plannerState.view,
    hideSelect: state.plannerSelectState.hide,
})

export default connect(mapStateToProps)(TablePlanner)