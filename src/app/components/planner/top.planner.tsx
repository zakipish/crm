import * as React from 'react'

interface IProps {
    subtit: string
    title: string
    status: string
}

class TopPlanner extends React.Component<IProps, {}> {
    render() {
        let {subtit, title, status} = this.props
        return (
            <div className="pl-top">
                <div className="_text">
                    <div className="_subtit">{subtit}</div>
                    <div className="_tit">{title}</div>
                </div>
                <div className="_status">{status}</div>
            </div>
        )
    }
}

export default TopPlanner