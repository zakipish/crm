import * as React from 'react'

import TableGridPlanner from './table-grid.planner'
import SelectPlanner from './select.planner'
import GameListPlanner from './game-list.planner'

interface IProps {
    refTable: React.RefObject<HTMLDivElement>
}

interface IState {}

class TableScrollPlanner extends React.Component<IProps, IState> {

    refScroll: React.RefObject<HTMLDivElement>

    constructor(props: IProps) {
        super(props)
        this.state = {}

        this.refScroll = React.createRef()
        this.onScroll = this.onScroll.bind(this)
    }
    
    onScroll(e: React.UIEvent<HTMLDivElement>) {
        let { scrollLeft, scrollTop } = e.currentTarget
        let tableElem = this.props.refTable.current
        let dateWrap: any = tableElem.querySelector('.pl-table-date ._wrap')
        let timeWrap: any = tableElem.querySelector('.pl-table-time')
        let daySettingWrap: any = tableElem.querySelector('.pl-day-setting')
        dateWrap.style.transform = `translateX(-${scrollLeft}px)`
        daySettingWrap.style.transform = `translateX(-${scrollLeft}px)`
        timeWrap.style.transform = `translateY(-${scrollTop}px)`
    }

    render() {
        return (
            <div className="pl-table-scroll" ref={this.refScroll} onScroll={this.onScroll}>
                <SelectPlanner refScroll={this.refScroll}>
                    <TableGridPlanner/>
                    <GameListPlanner/>
                </SelectPlanner>
            </div>
        )
    }
}

export default TableScrollPlanner