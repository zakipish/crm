import * as React from 'react'
import Contact from '../../models/contact'

import { IconList } from '../blocks/icon/icon.component'
import FieldComponent from './field.component'

interface IControlProps {
    type: string
    contact_type_id: number
    onAdd?: () => void
    list: IField[]
}

interface IField {
    name: string
    value: string
    onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
    onRemove?: (e?: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
}

class ContactFieldComponent extends React.Component<IControlProps> {

    getIconsCollection = (field: IField, i: number) => {
        const { list, onAdd } = this.props

        let icons = [
            { icon: IconList.del, bg: 'red', onClick: field.onRemove }
        ]

        if (list.length === i + 1)
            icons.push({ icon: IconList.plusCir, bg: null, onClick: onAdd })

        return icons
    }

    render() {
        const { type, list } = this.props

        switch(type) {
            case 'telefon':
                return (
                    <React.Fragment>
                        {list.map((field, i) =>
                            <FieldComponent
                                key={i}
                                title="Телефон"
                                icons={this.getIconsCollection(field, i)}
                                name={field.name}
                                value={field.value}
                                placeholder="+7 (800) 555 35 35"
                                onChange={field.onChange}
                            />
                       )}
                   </React.Fragment>
                )

            case 'e-mail':
                return (
                    <React.Fragment>
                        {list.map((field, i) =>
                            <FieldComponent
                                key={i}
                                title="E-mail"
                                icons={this.getIconsCollection(field, i)}
                                name={field.name}
                                value={field.value}
                                placeholder="ваш_адрес@почта.ру"
                                onChange={field.onChange}
                            />
                       )}
                   </React.Fragment>
                )

            case 'sajt':
                return (
                    <React.Fragment>
                        {list.map((field, i) =>
                            <FieldComponent
                                key={i}
                                title="Сайт"
                                icons={this.getIconsCollection(field, i)}
                                name={field.name}
                                value={field.value}
                                placeholder="www.ваш_сайт.ру"
                                onChange={field.onChange}
                            />
                       )}
                   </React.Fragment>
                )
        }
    }
}

export default ContactFieldComponent
