import * as React from 'react'

import { findCompany, findAddress, findBank } from '../../core/dadata'
import OutsideClickComponent, { IOutsideClickProps, IOutsideClickState } from '../_shared/outside-click.component'

import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface FieldDadataComponentProps extends IOutsideClickProps {
    type: string,
    onSelect: (response: Object) => void
    placeholder?: string
    value?: string
    field?: string
    title?: string
}

interface FieldDadataComponentState extends IOutsideClickState {
    value: string,
    hide: boolean,
    options: Object[],
    preSelectIndex?: number
}

export const suggestionAddress = 'address'
export const suggestionCompany = 'company'
export const suggestionBank = 'bank'

class FieldDadataComponent extends OutsideClickComponent<FieldDadataComponentProps, FieldDadataComponentState> {
    filterTimeout
    state: FieldDadataComponentState = {
        value: '',
        hide: true,
        options: [],
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
        window.addEventListener('keydown', this.onKeyPress)

        this.bootstrap()
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
        window.removeEventListener('keydown', this.onKeyPress)
    }

    bootstrap = () => {
        const { value } = this.props
        if (value)
            this.setState({ value })
    }

    handleChange = (e: any) => {
        const value = e.target.value
        this.setState({ value }, this.search(value))
    }

    search = (value: string) => () => {
        if (this.filterTimeout != null)
            clearTimeout(this.filterTimeout)

        this.filterTimeout = setTimeout(() => {
            if (this.props.type === suggestionCompany)
                findCompany(value).then(this.updateOptions)
            if (this.props.type === suggestionAddress)
                findAddress(value).then(this.updateOptions)
            if (this.props.type === suggestionBank)
                findBank(value).then(this.updateOptions)
        }, 700)
    }

    updateOptions = (options: Object[]) => {
        this.setState({ options })
    }

    onClickEmpty = () => {
        this.setState({ value: '' })
        this.setHide()
    }

    onSelectByClick = (i: number) => (e: any) => {
        e.stopPropagation()

        const { options } = this.state
        const { onSelect, field } = this.props

        onSelect(options[i])

        if (field) {
            this.setState({ value: options[i]['data'][field] })
        } else {
            this.setState({ value: options[i]['value'] })
        }

        this.setHide()
    }

    onSelectByKey = () => {
        const { options, preSelectIndex } = this.state
        const { onSelect, field } = this.props

        onSelect(options[preSelectIndex])

        if (field) {
            this.setState({ value: options[preSelectIndex]['data'][field] })
        } else {
            this.setState({ value: options[preSelectIndex]['value'] })
        }
        this.setHide()
    }

    onMoveByKey = (destination: number) => {
        let { options, preSelectIndex } = this.state

        if (preSelectIndex == null) {
            if (destination > 0) {
                this.setState({ preSelectIndex: 0 })
            } else {
                this.setState({ preSelectIndex: options.length - 1 })
            }

            return true
        }

        preSelectIndex += destination
        if (preSelectIndex >= options.length)
            preSelectIndex = 0
        else if (preSelectIndex < 0)
            preSelectIndex = options.length - 1

        this.setState({ preSelectIndex })
    }

    onKeyPress = (e) => {
        const { hide } = this.state

        switch(e.key) {
            case 'ArrowDown':
                e.preventDefault()
                this.onMoveByKey(1)
                break
            case 'ArrowUp':
                e.preventDefault()
                this.onMoveByKey(-1)
                break
            case 'Escape':
                e.preventDefault()
                this.setHide()
                break
            case 'Enter':
                e.preventDefault()
                this.onSelectByKey()
                break
        }
    }

    setShow = () => {
        this.setState({ hide: false, preSelectIndex: null })
    }

    setHide = () => {
        this.setState({ hide: true, preSelectIndex: null })
    }

    toggleHide = () => {
        const { hide } = this.state
        this.setState({ hide: !hide })
    }

    onClickOutside(e) {
        if(!this.hide && this.checkContains(e)) {
            this.setHide()
        }
    }

    onClickArrow = (e: any) => {
        e.stopPropagation()
        this.toggleHide()
    }

    get hide() {
        return this.state.hide
    }

    render() {
        const { onSelect, title, value: val, ...rest } = this.props
        const { value, options, preSelectIndex } = this.state
        const optionKeys = Object.keys(options)
        const emptyTitle = 'Пусто'

        return (
            <div
                className={`fieldSelect ${this.hide ? '--hide' : '--show'}`}
                ref={this.outsideRef}
                onClick={this.setShow}
            >
                <div className="fieldSelect__btn" onClick={this.onClickArrow}>
                    <IconComponent icon={IconList.selectArr} />
                </div>
                <div className="fieldSelect__input">
                    <label className="fieldInput">
                        {title && <div className="_tit">{title}</div>}
                        <div className="_input">
                            <input value={value} onChange={this.handleChange} autoComplete="new-password" {...rest} />
                        </div>
                    </label>
                </div>
                <div className="fieldSelect__list">
                    <ul>
                        { optionKeys.length ?
                            optionKeys.map((key, i) => {
                                return <li
                                    key={i}
                                    className={i === preSelectIndex ? '--act' : ''}
                                    onClick={this.onSelectByClick(i)}
                                >
                                    <IconComponent icon={IconList.ok}/>
                                    <div>{options[key].value}</div>
                                </li>
                            })
                        :
                            <li className="--empty" onClick={this.onClickEmpty}>{emptyTitle}</li>
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default FieldDadataComponent
