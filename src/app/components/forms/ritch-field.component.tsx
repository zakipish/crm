import * as React from 'react'
import RichTextEditor, { ButtonGroup, Dropdown, EditorValue } from 'react-rte'
import { EditorState, Modifier } from 'draft-js'

const TOOLBAR_CONFIG = {
    display: ['INLINE_STYLE_BUTTONS'],
    INLINE_STYLE_BUTTONS: [
        { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
        { label: 'Italic', style: 'ITALIC' },
        { label: 'Underline', style: 'UNDERLINE' },
    ],
}

const VARIBLE_OBJ = {
    '%CLUBNAME%': 'Название клуба',
}

const VARIBLE_KEYS = Object.keys(VARIBLE_OBJ)

const VARIBLE_MAP = new Map([
    ['0', { label: 'Переменные' }],
    [VARIBLE_KEYS[0], { label: VARIBLE_OBJ[VARIBLE_KEYS[0]] }],
])

interface IProps {
    title?: string
    name: string
    defaultValue: string
    onChange: Function
}

interface IState {
    value: string
}

class RitchFieldComponent extends React.Component<IProps, IState> {

    state = {
        value: RichTextEditor.createEmptyValue()
    }

    componentDidMount() {
        let { defaultValue } = this.props
        if (!defaultValue) {
            defaultValue = RichTextEditor.createEmptyValue()
        } else if (typeof defaultValue === 'string') {
            defaultValue = RichTextEditor.createValueFromString(defaultValue, 'html')
        }
        this.setState({ value: defaultValue })
    }

    onChange = (value) => {
        this.setState({ value })
        let event = {
            target: {
                name: this.props.name,
                value: value.toString('html'),
            },
        }
        this.props.onChange(event)
    }

    insertVar(editorState, text) {
        let contentState = editorState.getCurrentContent()
        let selectionState = editorState.getSelection()
        contentState = Modifier.insertText(contentState, selectionState, text, editorState.getCurrentInlineStyle())
        let newEditorState = EditorState.push(editorState, contentState, 'insert-characters')
        this.setState({ value: new EditorValue().setEditorState(newEditorState) })
    }

    render() {
        let { title } = this.props
        return (
            <div className="fieldInput">
                <div className="_tit">{title}</div>
                <RichTextEditor
                    toolbarConfig={TOOLBAR_CONFIG}
                    value={this.state.value}
                    onChange={this.onChange}
                    className="_rte"
                    customControls={[
                        (setValue, getValue, editorState) => {
                            return (
                                <ButtonGroup key={1}>
                                    <Dropdown
                                        choices={VARIBLE_MAP}
                                        selectedKey={0}
                                        onChange={value => this.insertVar(editorState, value)}
                                    />
                                </ButtonGroup>
                            )
                        },
                    ]}
                />
            </div>
        )
    }
}

export default RitchFieldComponent
