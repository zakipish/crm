import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    id: string
    title: string
    src?: string
    logo?: any
    onUpload: Function
    onDelete?: (i: number) => void
    size?: {
        width: number
        height: number
        view?: boolean
    }
    multiple?: boolean
}

class UploadComponent extends React.Component<IProps, {}> {

    input: React.RefObject<HTMLInputElement>;

    constructor(props) {
        super(props)
        this.input = React.createRef()
    }

    onChange(e) {
        let { files } = e.target
        let filesLen = files.length
        let filesRes = []
        for (let i = 0; i < filesLen; i++) {
            const file = files[i];
            let reader = new FileReader()
            reader.onloadend = () => {
                filesRes.push(reader.result)
                if (filesRes.length === filesLen) {
                    this.props.onUpload(filesRes,files)
                }
            }
            file && reader.readAsDataURL(file)
        }
    }

    clearUpload() {
        const { logo, onDelete } = this.props
        this.input.current.value = null
        if (logo.id && typeof onDelete === 'function'){
            onDelete(logo.id)
        }
    }

    getStyleSizePreview() {
        let { size } = this.props
        let styleSize = {
            width: '90px',
            height: '90px',
        }
        if (size) {
            styleSize = {
                width: size.width + 'px',
                height: size.height + 'px',
            }
        }
        return styleSize
    }

    checkViewSize() {
        let { size } = this.props
        if (size.view === undefined) size.view = true
    }

    render() {
        let { id, title, size, src, multiple } = this.props
        let stylePreview = this.getStyleSizePreview()
        this.checkViewSize()
        return (
            <div className="upload">
                {(!src || src === '#') ? 
                    <label className="_preview --none" htmlFor={id} style={stylePreview}>
                        <span>{title}</span>
                    </label>
                : 
                    <div>
                        <label className="_preview --exists" htmlFor={id} style={stylePreview}>
                            <img src={src.toString()} alt="#"/>
                        </label>
                        {!multiple ?
                            <div className="_preview__cross" onClick={() => this.clearUpload()}>
                                <IconComponent icon="cross"/>
                            </div>
                        : ''}
                    </div>
                }
                <input 
                    ref={this.input}
                    id={id}
                    type="file" 
                    className="_input" 
                    onChange={(e) => this.onChange(e)}
                    multiple={multiple}
                />
                {size && size.view ? <div className="_size">{size.width}x{size.height} px</div> : ''}
            </div>
        )
    }
}

export default UploadComponent
