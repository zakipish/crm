import * as React from "react";
import IconComponent from '../blocks/icon/icon.component'

interface IRadioControlProps {
    name: string
    type?: string
    value?: string | boolean
    options: {}
    disabled?: boolean
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
    color?: string
}

class RadioControl extends React.Component<IRadioControlProps> {

    get className() {
        let { type, color, disabled } = this.props
        let className = 'checkRadio'
        className += type === 'radio' ? ' --cir' : ' --sq'
        className += color ? ` --${color}` : ''
        className += disabled ? ` --disabled` : ''
        return className
    }

    render() {
        let { type, value, onChange, options, disabled} = this.props
        return (
            Object.keys(options).map((option, i) => (
                <label className={this.className} key={i}>
                    <input
                        type={type ? type : 'radio'}
                        name={name}
                        value={option}
                        checked={value == option}
                        onChange={onChange}
                        disabled={disabled}
                    />
                    <i><IconComponent icon="ok"/></i>
                    <span>{options[option]}</span>
                </label>
            ))
        )
    }
}

export default RadioControl
