import * as React from 'react'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

export interface IFieldProps extends IControlProps {
    title?: string
    icons?: Array<{
        icon: IconList | string
        bg?: string
        onClick?: (Function)
    }>
    error?: boolean
}

interface IControlProps extends React.InputHTMLAttributes<HTMLInputElement | HTMLTextAreaElement> {
    type?: string
    name?: string
    value?: string|number
    placeholder?: string
    multiline?: boolean
    viewOnly?: boolean
    autoComplete?: string
    onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
}


class FieldComponent extends React.Component<IFieldProps, {}> {

    renderIcons(icons) {
        let result = ''
        if (icons) {
            result = icons.map((icon, i) => {
                return(
                    <div className={`_icon ${icon.bg ? `--${icon.bg}` : ''}`} key={i} onClick={icon.onClick}>
                        <IconComponent icon={icon.icon} />
                    </div>
                )
            })
        }
        return result
    }

    get className() {
        let str = 'fieldInput'
        let { error } = this.props
        str += error ? ' --error' : ''
        return str
    }

    render() {
        let { title, icons, multiline, viewOnly, error, ...rest } = this.props
        rest.value = rest.value ? rest.value : ''
        return (
            <label className={this.className}>
                {title && <div className="_tit">{title}</div>}
                {(rest.name || rest.value) ? (
                    <div className="_input">
                        {this.renderIcons(icons)}
                        {viewOnly ? (
                            <div className="_input__value">{rest.value}</div>
                        ) : (
                            !multiline ? (
                                <input {...rest}/>
                            ) : (
                                <textarea {...rest}></textarea>
                            )
                        )}
                    </div>
                ) : (
                    this.renderIcons(icons) 
                )}
            </label>
        )
    }
}

export default FieldComponent
