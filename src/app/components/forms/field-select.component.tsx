import * as React from 'react'
import OutsideClickComponent, { IOutsideClickProps, IOutsideClickState } from '../_shared/outside-click.component'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import FieldComponent from './field.component'

export interface IFieldSelectProps extends IOutsideClickProps {
    title?: string
    name?: string
    value?: string
    options: Object | Array<any>
    placeholder?: string
    onChange: (name: string, value: string | number) => void
    onSearch?: (name: string, value: string | number) => void
    autoHide?: boolean
    emptyTitle?: string
    onClickEmpty?: (name: string, fieldValue: string) => void
    disableKeysSelect?: boolean
    error?: boolean
    forceTitle?: string
}

export interface IFieldSelectState extends IOutsideClickState {
    fieldValue: string
}

class FieldSelectcomponent extends OutsideClickComponent<IFieldSelectProps, IFieldSelectState> {

    oldFilter: string = ''
    numKey: number = 0
    startFind: number = 3

    state = {
        hide: true,
        fieldValue: (this.props.options instanceof Array) ? this.props.value : this.props.options[this.props.value] || ''
    }

    componentDidUpdate(prevProps) {
        let { value, options, name, onChange } = this.props

        let { value: prevValue, options: prevOptions } = prevProps

        if (JSON.stringify(options) != JSON.stringify(prevOptions) && !options[value]) {
            console.log({ options, prevOptions })
            if(!options[value]) {
                onChange(name, null)
            }
        }

        if (value !== prevValue) {
            let fieldValue = (options instanceof Array) ? value : options[value] || ''

            this.setState({fieldValue})
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
        window.addEventListener('keydown', this.onKeyPress)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
        window.removeEventListener('keydown', this.onKeyPress)
    }

    get autoHide() {
        const { autoHide } = this.props

        if (typeof autoHide === 'undefined')
            return true

        return autoHide
    }

    resetTmp() {
        this.numKey = 0
        this.oldFilter = null
    }

    onHide = () => {
        if (!this.state.hide) {
            this.outsideRef.current.querySelector('input').blur()
            let fieldValue = (this.props.options instanceof Array) ? this.props.value : this.props.options[this.props.value] || ''
            this.setState({hide: true, fieldValue})
        }
        this.resetTmp()
    }

    onShow = () => {
        if (this.state.hide) {
            this.outsideRef.current.querySelector('input').focus()
            this.setState({hide: false})
        }
        this.resetTmp()
    }

    onToggle = () => {
        if (this.state.hide) {
            this.onShow()
        } else {
            this.onHide()
        }
    }

    onSelectByClick = (name, key) => {
        const { options } = this.props
        if (options instanceof Array) {
            this.props.onChange(name, options[+key])
        } else {
            this.props.onChange(name, key)
        }

        let fieldValue = (options instanceof Array) ? key ? options[+key] : '' : key ? options[key] : ''

        if (this.autoHide) {
            this.setState({hide: true, fieldValue})
            this.resetTmp()
        } else {
            this.setState({ fieldValue })
        }
    }

    onSelect = (name, key) => {
        const { options } = this.props
        if (options instanceof Array) {
            this.props.onChange(name, options[key])
        } else {
            this.props.onChange(name, key)
        }
        let fieldValue = (options instanceof Array) ? key ? options[+key] : '' : key ? options[key] : ''
        this.setState({ fieldValue })
    }

    onSearch = (e) => {
        let filter = e.target.value
        let filterLen = filter.length
        this.numKey = filterLen > this.startFind ? this.numKey + 1 : 3
        this.oldFilter = filter
        this.setState({fieldValue: filter})
        let { onSearch } = this.props
        if (onSearch) onSearch(e.target.name, e.target.value)
    }

    getOptionKeys = () => {
        let optionKeys = Object.keys(this.props.options)
        if (this.numKey >= this.startFind) {
            optionKeys = this.filterOptions(optionKeys)
        }
        return optionKeys
    }

    filterOptions = (optionKeys) => {
        let { options } = this.props
        let filter = this.oldFilter ? this.oldFilter.toLowerCase() : this.state.fieldValue.toLowerCase()
        optionKeys = optionKeys.filter(key => {
            let value = options[key].toLowerCase()
            return value.indexOf(filter) >= 0 ? true : false
        })
        return optionKeys
    }

    onKeyPress = (e) => {
        const { disableKeysSelect } = this.props
        let status = !this.state.hide
        if (status) {
            if (!disableKeysSelect) {
                if (e.key === 'ArrowDown') {
                    e.preventDefault()
                    this.selectByKey(1)
                }
                else if (e.key === 'ArrowUp') {
                    e.preventDefault()
                    this.selectByKey(-1)
                }
            }

            if (e.key === 'Escape') {
                e.preventDefault()
                this.onHide()
            }
        }
    }
    
    selectByKey = (addIndex) => {
        let { value, name } = this.props
        let optionKeys = this.getOptionKeys()
        let actIndex = optionKeys.indexOf(value)
        let nextIndex = actIndex + addIndex
        let key = optionKeys[nextIndex]
        if (key) this.onSelect(name, key) 
    }

    get className() {
        let str = 'fieldSelect'
        let { error } = this.props
        str += error ? ' --error' : ''
        str += this.hide ? ' --hide' : ' --show'
        return str
    }

    render() {
        let { title, name, value, placeholder, options, emptyTitle, onClickEmpty, error, forceTitle } = this.props
        let { fieldValue } = this.state
        let optionKeys = this.getOptionKeys()
        emptyTitle = emptyTitle ? emptyTitle : 'Пусто'
        return (
            <div
                className={this.className}
                ref={this.outsideRef}
                onClick={this.onShow}
            >
                <div className="fieldSelect__btn" onClick={this.onHide}>
                    <IconComponent icon={IconList.selectArr}/>
                </div>
                <div className="fieldSelect__input">
                    <FieldComponent
                        title={title}
                        type="text"
                        name={name}
                        value={forceTitle || fieldValue}
                        placeholder={placeholder}
                        onChange={this.onSearch}
                        autoComplete="off"
                        error={error}
                    />
                </div>
                <div className="fieldSelect__list">
                    <ul>
                        { optionKeys.length ?
                            optionKeys.map((key, i) => {
                                return <li
                                    key={i}
                                    className={key === value ? '--act' : ''}
                                    onClick={() => this.onSelectByClick(name, key)}
                                >
                                    <IconComponent icon={IconList.ok}/>
                                    <div>{options[key]}</div>
                                </li>
                            })
                        :
                            <li className="--empty" onClick={() => onClickEmpty && onClickEmpty(name, fieldValue)}>{emptyTitle}</li>
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default FieldSelectcomponent
