import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    state: boolean
}

class VerifiField extends React.Component<IProps, {}> {
    render() {
        let { state, children } = this.props
        return (
            <div className={`verifi ${state ? ' --ok' : ' --not'}`}>
                {state ? 
                    <div className="verifi__state">
                        <IconComponent icon="ok"/>
                    </div>
                    : 
                    <div className="verifi__state">!</div>
                }
                <div className="verifi__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default VerifiField