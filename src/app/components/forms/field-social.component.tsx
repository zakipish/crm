import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'
import { ContactSocTypes, ContactSocShorts, ContactSocDomain } from '../../models/contact-type'

interface IProps {
    list?: ISoc[]
    onAdd? (soc: ISoc): void
    onRemove? (slug: string): void
}

interface IState {
    value: string
    soc: ISoc
}

interface ISoc {
    slug: string
    value: string
}

class FieldSocialComponent extends React.Component<IProps, IState> {

    state = {
        value: '',
        soc: null,
    }

    checkLink = (value: string) => {
        let soc = null
        let protocol = (value.includes('https://') || value.includes('http://')) ? true : false
        if (protocol) {
            let slug = null
            ContactSocTypes.forEach(type => {
                if (!slug) {
                    ContactSocDomain[type].forEach(strDomain => {
                        let findDoamin = value.includes(strDomain)
                        if(findDoamin) slug = type
                    })
                } 
            })
            if (slug) {
                let duplicate = this.props.list.find(soc => soc.slug === slug)
                if (!duplicate) {
                    soc = { slug, value }
                }
            }
        } 
        this.setState({soc})
    }

    inputChange = (e) => {
        this.checkLink(e.target.value)
        this.setState({value: e.target.value})
    }

    onAdd = () => {
        let { soc } = this.state
        let { onAdd } = this.props
        if (soc && onAdd) {
            onAdd(soc)
            this.setState({value: ''})
        }
    }

    onRemove = (slug: string) => {
        let { onRemove } = this.props
        if (onRemove) {
            onRemove(slug)
        }
    }

    render() {
        let { list } = this.props
        let { value } = this.state
        return (
            <div className="fieldSoc">
                <div className="_title">Социальные сети</div>
                <div className="_wrap">
                    { list && list.length > 0 ?
                        <div className="_socs">
                            {list.map((el, i) => { return(
                                <div key={i} className="_soc" title={el.value}>
                                    <div className="small-cross" onClick={() => this.onRemove(el.slug)}>
                                        <IconComponent icon="cross"/>
                                    </div>
                                    <span>{ContactSocShorts[el.slug]}</span>
                                </div>
                            )})}
                        </div>
                    : ''}
                    <div className="_input">
                        <input 
                            type="text" 
                            placeholder="http\\..." 
                            value={value} 
                            onChange={this.inputChange}
                        />
                        <div className="_input__btn" onClick={this.onAdd}>Добавить</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FieldSocialComponent