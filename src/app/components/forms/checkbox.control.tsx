import * as React from "react";
import IconComponent from '../blocks/icon/icon.component'

interface ICheckboxControlProps {
    title?: string
    name: string
    value?: string | number
    checked?: boolean
    defaultChecked?: boolean
    disabled?: boolean
    color?: string
    invert?: string
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

class CheckboxControl extends React.Component<ICheckboxControlProps> {

    get className() {
        let { color, disabled, invert } = this.props
        let className = 'checkRadio --sq'
        className += color ? ` --${color}` : ''
        className += disabled ? ` --disabled` : ''
        className += disabled ? ` --disabled` : ''
        className += invert ? ` --invert` : ''
        return className
    }

    render() {
        let { title, ...rest} = this.props
        return (
            <label className={this.className}>
                <input type="checkbox" {...rest}/>
                <i><IconComponent icon="ok"/></i>
                <span>{title}</span>
            </label>
        )
    }
}

export default CheckboxControl
