import * as React from 'react'
import { Link } from 'react-router-dom'

import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    className?: string
    title: string
    subtit?: string
    btns?: Array<{
        label: string
        url: string
    }>
}

class CardIconbtnComponent extends React.Component<IProps, {}> {
    render() {
        let { className, title, subtit, btns } = this.props
        return (
            <div className={`card-iconbtn ${className ? className : ''}`}>
                <div className="_icon --yellow">
                    <IconComponent icon="calendar"/>
                </div>
                <div className="_text">
                    <div className="_tit">{title}</div>
                    {subtit ? <div className="_subtit">{subtit}</div> : ''}
                </div>
                {btns ?
                    <div className="_btns">
                        {btns.map((el, i) => { return(
                            <Link 
                                key={i} 
                                to={el.url} 
                                className="_btn --blue"
                            >{el.label}</Link>
                        )})}
                    </div>
                : ''}
            </div>
        )
    }
}

export default CardIconbtnComponent