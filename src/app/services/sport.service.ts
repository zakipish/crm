import Sport from '../models/sport'
import { reqGet } from '../core/api'

export const getSports = async () => {
    try {
        const response = await reqGet(`/sports`)
        const sports = response.data.map(sport => new Sport(sport))

        return sports
    } catch(e) {
        console.log(e.message)
        return Promise.reject(e.errors)
    }
}
