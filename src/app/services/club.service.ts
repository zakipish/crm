import Club from '../models/club'

import { reqGet } from '../core/api'

export const getClubs = async () => {
    try {
        const response = await reqGet(`/clubs`)
        const clubs = response.data.map(clubRaw => new Club(clubRaw))

        return clubs
    } catch(e) {
        console.log(e.message)
        return null
    }
}
