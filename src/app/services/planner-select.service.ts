import { PlannerLayers } from './../reducers/planner.reducer'
import { setGame } from './planner-game.service'
import store from '../../store'
import { reqGet, reqPost, reqPatch } from '../core/api'

import * as PlannerAction from '../actions/planner.actions'
import * as PlannerSelectAction from '../actions/planner-select.actions'
import Game from '../models/game';
import { reqPrices, toggleSettingDay } from './planner.service';
import { selectedPriceReducer } from '../components/planner/planner.util'

export const onCroodClear = () => {
    let rows = document.querySelectorAll('.rowSelect')
    rows.forEach(el => el.classList.remove('rowSelect'))
    let cells = document.querySelectorAll('.cellSelect')
    cells.forEach(el => el.classList.remove('cellSelect'))
}

export const onShow = () => {
    let { hide } = store.getState().plannerSelectState
    if (hide === true) {
        onCroodClear()
        store.dispatch(PlannerSelectAction.show())
    }
}

export const onHide = () => {
    onCroodClear()
    let { hide } = store.getState().plannerSelectState
    if (hide === false) {
        store.dispatch(PlannerSelectAction.hide())
        setGame()
    }
}

export const onStartMove = () => {
    let { endMoveState } = store.getState().plannerSelectState
    if (endMoveState === true) {
        store.dispatch(PlannerSelectAction.startMove())
    }
}

export const onEndMove = (endCrood) => {
    let { endMoveState, hide } = store.getState().plannerSelectState
    if (endMoveState === false && hide === false) {
        store.dispatch(PlannerSelectAction.endMove(endCrood))
    }
}

export const setPrice = (price, state) => {
    let { prices } = store.getState().plannerState
    let { x1, x2, y1, y2 } = store.getState().plannerSelectState.crood
    for (let x = x1; x <= x2; x++) {
        for (let y = y1; y <= y2; y++) {
            prices[x].prices[y].value = price
            prices[x].prices[y].state = state
        }
    }
    store.dispatch(PlannerAction.setPrices(prices))
    onHide()
}

export const onSetList = (list, crood) => {
    let { layerKey } = store.getState().plannerState
    if (layerKey === PlannerLayers.game) {
        let item = list[0]
        let date = item.date
        let duration = item.durations[0]
        let interval = [duration.from, duration.to]
        let price = list.reduce(selectedPriceReducer, 0)
        console.log({price})
        let game = new Game({ date, interval, price, is_kipish: false })
        setGame(game)
    }
    store.dispatch(PlannerSelectAction.setCrood(crood))
    store.dispatch(PlannerSelectAction.setSelectList(list))
}

export const sendSetPrice = async (price: number, state: string) => {
    let { id } = store.getState().plannerState.reqParam
    let { list } = store.getState().plannerSelectState
    try {
        await reqPost(`/playgrounds/${id}/playground-schedules`, JSON.stringify({ price, state, list }))
        await reqPrices()
        onHide()
    } catch (e) {
        console.log(e.message)
    }
}

export const sendDaySchedules = async (date: string, from: string, to: string, price: number = 0) => {
    let { id } = store.getState().plannerState.reqParam
    let list = [{ date, durations: [{ from, to, price }] }]
    let state = 'open'
    try {
        await reqPost(`/playgrounds/${id}/playground-schedules`, JSON.stringify({ price, state, list }))
        await reqPrices()
        onHide()
        toggleSettingDay()
    } catch (e) {
        console.log(e.message)
    }
}
