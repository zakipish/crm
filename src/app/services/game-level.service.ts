import GameLevel from '../models/game-level'

import { reqGet } from '../core/api'

export const getGameLevels = async () => {
    try {
        const response = await reqGet(`/game_levels`)
        const gameLevels = response.data.map(gameLevelRaw => new GameLevel(gameLevelRaw))

        return gameLevels
    } catch(e) {
        console.log(e.message)
        return null
    }
}
