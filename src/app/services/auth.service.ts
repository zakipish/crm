import store from '../../store'
import { reqGet, reqPost, reqDelete } from '../core/api'
import { authLogin, authClear } from '../actions/auth.actions'
import { history } from './../core/history'

export const initialLogin = async () => {
    try {
        const user = await reqGet('/auth')
        store.dispatch(authLogin(user.data))
    } catch(e) {
        console.log(e.message)
    }
}

export const login = async (login, password) => {
    try {
        const user = await reqPost('/auth', JSON.stringify({ login, password }))
        store.dispatch(authLogin(user.data));
        return user.data
    } catch(e) {
        console.log(e.message)
        return e
    }
}

export const logout = async () => {
    try {
        await reqDelete('/auth')
        store.dispatch(authClear())
        history.push('/')
    } catch(e) {
        console.log(e.message)
    }
}
