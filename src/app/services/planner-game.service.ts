import { reqGet, reqPatch, reqPost } from '../core/api'
import { onShow, onHide } from './planner-select.service'
import { reqPrices } from './planner.service'
import Game from '../models/game'
import User from '../models/user'

import store from '../../store'
import * as PlannerAction from '../actions/planner.actions'
import * as PlannerSelectAction from '../actions/planner-select.actions'

export const reqGames = async () => {
    let { id, dateStart, dateEnd } = store.getState().plannerState.reqParam
    let res = await reqGet(`/playgrounds/${id}/games`, {dateStart, dateEnd})
    await store.dispatch(PlannerAction.setGames(res.data))
}

export const saveGame = async () => {
    let playgId = store.getState().plannerState.reqParam.id
    let { game } = store.getState().plannerSelectState
    let gameId = game.id
    try {
        if (gameId != null) {
            await reqPatch(`/playgrounds/${playgId}/games/${gameId}`, JSON.stringify(game))
        } else {
            await reqPost(`/playgrounds/${playgId}/games`, JSON.stringify(game))
        }
        await reqPrices()
        await reqGames()
        onHide()
    } catch (e) {
        game.errors = e.errors
        await setGame(game)
        console.log(e.message)
    }
}

export const setGame = async (game: Game = null) => {
    if (!game) {
        game = new Game({
            owner: new User({
                name: '',
                phone: '',
            }),
        })
    }
    await store.dispatch(PlannerSelectAction.setSelectGame(game))
}

export const showSetGame = async (game: Game = null, crood) => {
    let to = game.interval[0]
    if (to === '24:00') to = "23:59:59"
    let list = [{
        date: game.date,
        durations: [{
            from: game.interval[0],
            to: to,
            price: game.price,
        }]
    }]
    await setGame(game)
    await store.dispatch(PlannerSelectAction.setSelectList(list))
    await store.dispatch(PlannerSelectAction.endMove(crood))
    onShow()
}
