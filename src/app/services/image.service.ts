import Image from '../models/image'
import { reqPost } from '../core/api'

export const upload = async (file: any) => {
    let formData = new FormData()
    formData.append('image', file)

    try {
        const response = await reqPost('/images', formData)
        const image = new Image(response.data)

        return image
    } catch(e) {
        console.log(e.message)
        return null
    }
}
