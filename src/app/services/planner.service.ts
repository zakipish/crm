import moment from 'moment'
import { reqGet } from '../core/api'
import { reqGames } from './planner-game.service'

import store from '../../store'
import * as PlannerAction from '../actions/planner.actions'

export const setReqParam = async (id, date) => {
    cleanData()
    let format = 'YYYY-MM-DD'
    let monday = moment(date).startOf('week')
    let dateStart = monday.format(format)
    let dateEnd = monday.add(6, 'd').format(format)
    await store.dispatch(PlannerAction.setReqParam({id, dateStart, dateEnd}))
}

export const initDate = () => {
    let date = moment().startOf('week')
    store.dispatch(PlannerAction.setDate(date))
}

export const initGridSize = async ({duration = 1440, interval = 60, timeStart = '00:00', timeEnd = '24:00'}) => {
    let { grid } = store.getState().plannerState
    grid.durationMin = interval
    grid.lineLen = duration / grid.durationMin
    grid.rowLen = 7
    grid.timeStart = timeStart
    grid.timeEnd = timeEnd
    await store.dispatch(PlannerAction.setGrid(grid))
}

export const setCellSize = (cellWidth, cellHeight) => {
    let { grid } = store.getState().plannerState
    grid.cellWidth = cellWidth
    grid.cellHeight = cellHeight
    store.dispatch(PlannerAction.setGrid(grid))
}

export const setDate = async (addNum, key) => {
    let { date } = store.getState().plannerState
    if (addNum) {
        if (addNum > 0) {
            date.add(addNum, key)
        } 
        else if (addNum < 0) {
            date.subtract(Math.abs(addNum), key)
        }
        if (key === 'w') date.startOf('week')
        if (key === 'M') {
            date.startOf('month')
            if (date.day() !== 1) {
                let addDay = 7 - date.day() + 1
                date.add(addDay, 'd')
            }
        }
    }
    await store.dispatch(PlannerAction.setDate(date))
    let { id } = store.getState().plannerState.reqParam
    await setReqParam(id, date)
    await reqPrices()
    await reqGames()
}

export const cleanData = () => {
    store.dispatch(PlannerAction.setPrices([]))
    store.dispatch(PlannerAction.setGames([]))
}

export const toggleViewPrice = () => {
    let { view } = store.getState().plannerState
    view.hideCellPrice = !view.hideCellPrice
    store.dispatch(PlannerAction.setView(view))
}

export const toggleSettingDay = (index = -1) => {
    let { showSettingDay } = store.getState().plannerState
    if (showSettingDay === index) {
        index = -1
    }
    store.dispatch(PlannerAction.setShowSettingDay(index))
}

export const reqPrices = async () => {
    let { id, dateStart, dateEnd } = store.getState().plannerState.reqParam
    let res = await reqGet(`/playgrounds/${id}/prices`, {dateStart, dateEnd})
    let { timeStart, timeEnd, interval } = res.data
    let dateStr = moment().format('YYYY-MM-DD')
    let duration = moment(`${dateStr} ${timeEnd}`).diff(moment(`${dateStr} ${timeStart}`), 'minutes') 
    await setPriceLimit(res.data.dates)
    await initGridSize({duration, interval, timeStart, timeEnd})
    await store.dispatch(PlannerAction.setPrices(res.data.dates))
}

export const setPriceLimit = async (dates) => {
    let from = null
    let to = null
    dates.forEach(date => {
        date.prices.forEach(price => {
            let { value } = price
            if (value !== null) {
                if (value < from || from == null) from = value
                if (value > to || to == null) to = value
            }
        })
    })
    if(from == null) from = 0
    if(to == null) to = 0
    await store.dispatch(PlannerAction.setPriceLimit({from, to}))
}