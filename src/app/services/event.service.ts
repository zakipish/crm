import Event from '../models/event'

import store from '../../store'
import { loadEvents } from '../actions/event.actions'
import { reqGet, reqPost, reqPatch } from '../core/api'

export const downloadEvents = async () => {
    reqGet('/events')
    .then(response => {
        const events = response.data.map(event => new Event(event))
        store.dispatch(loadEvents(events))
    })
}

export const getEvent = async (slug: number | string) => {
    try {
        const response = await reqGet(`/events/${slug}`)
        const event = new Event(response.data)

        return event
    } catch(e) {
        console.log(e.message)
        return null
    }
}

export const createEvent = async (event: Event) => {
    try {
        const response = await reqPost(`/events`, JSON.stringify(event))
        const newEvent = new Event(response.data)

        return newEvent
    } catch(e) {
        console.log(e.message)
        return Promise.reject(e.errors)
    }
}

export const updateEvent = async (slug: number | string, event: Event) => {
    try {
        const response = await reqPatch(`/events/${slug}`, JSON.stringify(event))
        const newEvent = new Event(response.data)

        return newEvent
    } catch(e) {
        console.log(e.message)
        return Promise.reject(e.errors)
    }
}
