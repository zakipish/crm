import * as React from 'react'
import { match } from 'react-router'
import moment from 'moment'
import { reqGet } from '../core/api'
import Playground from '../models/playground'

import SectionComponent from '../components/layout/section.component'
import TopHeaderComponent from '../components/layout/top-header.component'
import IconComponent from '../components/blocks/icon/icon.component'
import CardIconbtnComponent from '../components/cards/card-iconbtn.component'
import ClubHeaderComponent from '../components/layout/club-header.component'
import ChartProgressComponent from '../components/blocks/charts/chart-progress.component'
import CartLinerComponent from '../components/blocks/charts/cart-liner.component'
import Loader from '../components/layout/loader'

const points2 = [
    {name: 'A1', p1: 100},
    {name: 'A2', p1: 100},
    {name: 'B1', p1: 80},
    {name: 'B2', p1: 200},
];

const points3 = [
    {name: 'Group A', p1: 400}, 
    {name: 'Group B', p1: 300},
    {name: 'Group C', p1: 300}, 
    {name: 'Group D', p1: 200},
    {name: 'Group E', p1: 278}, 
    {name: 'Group F', p1: 189}
]

interface IProps {
    match: match<any>
}

interface IState {
    playground: Playground
    loading: boolean
    loadingStatistics: boolean
    statistics: any
}

class PlaygDashboardComponent extends React.Component<IProps, IState> {
    state: IState = {
        playground: null,
        loading: true,
        statistics: null,
        loadingStatistics: true
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        const { id } = this.props.match.params
        const idPrev = prevProps.match.params.id
        if (id !== idPrev)
            this.bootstrap()
    }

    bootstrap = () => {
        this.getPlayground()
        this.getStatistics()
    }

    getPlayground = () => {
        const { id } = this.props.match.params

        this.setState({ playground: null, loading: true }, () => {
            reqGet(`/playgrounds/${id}`)
            .then(response => this.setState({ playground: response.data, loading: false }))
        })
    }

    getStatistics = () => {
        const { id } = this.props.match.params

        this.setState({ statistics: null, loadingStatistics: true }, () => {
            reqGet(`/playgrounds/${id}/statistics`)
            .then(response => this.setState({ statistics: response.data, loadingStatistics: false }))
        })
    }

    get activityChartPoints() {
        const { statistics } = this.state

        if (statistics && statistics.games) {
            return Object.keys(statistics.games).map(key => ({
                name: moment(key).format('D MMM'),
                booked: statistics.games[key].booked,
                pending: statistics.games[key].pending,
                complete: statistics.games[key].complete
            }))
        }

        return []
    }

    render() {
        let { id } = this.props.match.params
        const { playground, loading, loadingStatistics, statistics } = this.state

        return (
            <Loader state={loading}>
                { !loading ?
                    <div className="wrap">
                        <TopHeaderComponent
                            img={playground.club.logo ? playground.club.logo.url : null}
                            title={playground.club.name}
                            datetime={moment(playground.updated_at).format('HH:mm, dddd, DD.MM.YYYY')}
                        />
                        <div className="section-half">
                            <ClubHeaderComponent
                                img={playground.avatar_url}
                                className="section-half__item"
                                subtit={`Зал №${playground.position}`}
                                title={playground.name}
                            />
                            <CardIconbtnComponent
                                className="section-half__item"
                                title="Расписание"
                                subtit={moment().format('DD MMMM')}
                                btns={[{label: 'Редактировать', url: `/playground/planner/${id}`}]}
                            />
                        </div>
                        <SectionComponent>
                            <div className="section__title">График активностей</div>
                            <Loader state={loadingStatistics}>
                                {statistics &&
                                    <CartLinerComponent height={450} data={this.activityChartPoints}/>
                                }
                            </Loader>
                        </SectionComponent>
                        <div className="section-half">
                            <SectionComponent className="section-half__item">
                                <div className="section__title --red --bold">
                                    <span>Заполните расписание</span>
                                    <button type="button">
                                        <IconComponent icon="queryCir" fill="#4DA1FF"/>
                                    </button>
                                </div>
                                <Loader state={loadingStatistics}>
                                    {statistics &&
                                        <ChartProgressComponent
                                            num={`${statistics.schedules.days}`} subtitle="дней"
                                            percent={statistics.schedules.percents} color="#FF6464" lineState
                                        />
                                    }
                                </Loader>
                            </SectionComponent>
                        </div>
                    </div>
                : null }
            </Loader>
        )
    }
}

export default PlaygDashboardComponent
