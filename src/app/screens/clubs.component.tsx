import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import User from '../models/user'

interface IProps extends RouteComponentProps {
    user: User
}

class ClubsComponent extends React.Component<IProps, {}> {
    componentDidMount() {
        const { history, user } = this.props

        if (user.moderated_clubs && user.moderated_clubs.length > 0) {
            history.push(`/club/${user.moderated_clubs[0].id}`)
        }
    }

    render() {
        const { moderated_clubs } = this.props.user

        return (
            <div className="wrap">
                <h1>ClubsPage</h1>
                <div className="clubs--list">
                    { moderated_clubs.map(club => (
                        <Link key={club.id} to={`/club/${club.id}`}>{club.name}</Link>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user
})

export default withRouter(connect(mapStateToProps)(ClubsComponent))
