import * as React from 'react'
import moment from 'moment'
import { match } from 'react-router'

import TopPlanner from '../components/planner/top.planner'
import TablePlanner from '../components/planner/table.planner'
import FooterPlanner from '../components/planner/footer.planner'

import * as PlannerService from '../services/planner.service'
import { reqGet } from '../core/api'

interface IProps {
    match: match<any>
}

interface IState {
    name: string
    updated_at: string
}

class PlannerComponent extends React.Component<IProps, IState> {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            updated_at: '',
        } 
        let { id } = this.props.match.params
        PlannerService.setReqParam(id, moment())
    }
    

    componentDidMount() {
        let { id } = this.props.match.params
        reqGet(`/playgrounds/${id}`)
            .then(res => {
                this.setState({
                    name: res.data.name,
                    updated_at: res.data.updated_at,
                })
            })
            .catch(e => console.log(e.message))
    }

    render() {
        let { id } = this.props.match.params
        let { name, updated_at } = this.state
        let statusStr = updated_at ? moment(updated_at).format('HH:mm, dddd, DD.MM.YYYY') : ''
        return (
            <div className="pl">
                <TopPlanner 
                    subtit={`Зал №${id}`} 
                    title={name}
                    status={statusStr}
                />
                <TablePlanner/>
                <FooterPlanner/>
            </div>
        )
    }
}

export default PlannerComponent