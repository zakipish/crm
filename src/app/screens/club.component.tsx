import * as React from 'react'
import moment from 'moment'
import { YMaps, Map, GeoObject } from 'react-yandex-maps';
import { History } from 'history'

import Club from '../models/club'
import { ContactSocTypes } from '../models/contact-type'
import { reqGet } from '../core/api'
import Contact from '../models/contact'
import Sport from '../models/sport'
import { ITree } from '../components/blocks/tags.component'


import PageHeaderComponent from '../components/layout/page-header.component'
import SectionComponent from '../components/layout/section.component'
import ClubHeaderComponent from '../components/layout/club-header.component'
import IconComponent, { IconList, getSqSocIcon } from '../components/blocks/icon/icon.component'
import GalleryComponent from '../components/blocks/gallery/gallery.component'
import ToggleSectionComponent from '../components/layout/toggle-section.component'
import PriceListComponent from '../components/blocks/price-list.component'
import CheckList from '../components/blocks/check-list'
import TabSectionComponent, { ITabNavItem } from '../components/layout/tab-section.component'
import PlaygPageComponent from '../components/layout/playg-page.component'
import Loader from '../components/layout/loader'


interface IProps {
    history: History
    location: any
    match: any
}

interface IState {
    club: Club
    loading: boolean
    playgIndex: number
    sports: Sport[]
    coordinates?: number[]
}

class ClubComponent extends React.Component<IProps, IState> {
    private geoPlace: React.RefObject<any>;
    constructor(props) {
        super(props)
        this.state = {
            club: null,
            loading: true,
            playgIndex: 0,
            sports: null,
        }
        this.geoPlace = React.createRef();
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        const { clubId } = this.props.match.params
        const clubIdPrev = prevProps.match.params.clubId
        if (clubId !== clubIdPrev)
            this.bootstrap()
    }

    bootstrap = async () => {
        this.getDate()
        this.getSports()
    }

    getDate = async () => {
        let { clubId } = this.props.match.params
        this.setState({ loading: true })
        let res = await reqGet(`/clubs/${clubId}`)
        this.setState({ club: new Club(res.data), loading: false })
    }

    getSports = () => {
        reqGet('/sports')
        .then(response => this.setState({ sports: response.data }))
        .catch(e => console.log(e.message))
    }

    get sportsCollection() {
        const { sports } = this.state

        let itemsByLabel = {}
        let rootItem = new ITree(null)
        for (let sport of sports) {
            var item = new ITree(sport)

            if (itemsByLabel.hasOwnProperty(sport.label)) {
                if (Array.isArray(itemsByLabel[sport.label])) {
                    item.items = itemsByLabel[sport.label]
                } else {
                    throw new Error()
                }
            }

            itemsByLabel[sport.label] = item

            var arrayLabel = sport.label.split('.')
            if (arrayLabel.length === 1) {
                rootItem.items.push(item)
            } else {
                var parentLabel = arrayLabel.slice(0, arrayLabel.length - 1).join('.')
                if (itemsByLabel.hasOwnProperty(parentLabel)) {
                    if (Array.isArray(itemsByLabel[parentLabel])) {
                        itemsByLabel[parentLabel].push(item)
                    } else {
                        itemsByLabel[parentLabel].items.push(item)
                    }
                } else {
                    itemsByLabel[parentLabel] = [item]
                }
            }
        }

        return rootItem.items
    }

    get imagesCollection() {
        const { club } = this.state

        return club.images.map(image => ({
            order: image.order,
            url: image.url,
            id: image.id
        })).sort((a,b)=>a.order - b.order)
    }

    get phones(): Contact[] {
        let phones = null
        let { club } = this.state
        if (club && club.contacts && club.contacts.length) {
            phones = club.contacts.filter(el => el.contact_type.slug === 'telefon')
        }
        return phones
    }
    get sites(): Contact[] {
        let sites = null
        let { club } = this.state
        if (club && club.contacts && club.contacts.length) {
            sites = club.contacts.filter(el => el.contact_type.slug === 'sajt')
        }
        return sites
    }
    get mails(): Contact[] {
        let mails = null
        let { club } = this.state
        if (club && club.contacts && club.contacts.length) {
            mails = club.contacts.filter(el => el.contact_type.slug === 'e-mail')
        }
        return mails
    }

    get soc() {
        let soc = null
        let { club } = this.state
        if (club && club.contacts && club.contacts.length) {
            let contacts = club.contacts.filter(el => ContactSocTypes.includes(el.contact_type.slug))
            soc = contacts.map(el => ({ url: el.value, icon: getSqSocIcon(el.contact_type.slug) }))
        }
        return soc
    }
    get coordinates(): number[] {
        const { club } = this.state
        if (club && club.coordinates) {
            const { coordinates } = club
            const result = coordinates.match(/\(([\d,\.]*),([\d,\.]*)\)/i)

            if (result)
                return [parseFloat(result[1]), parseFloat(result[2])]
        }

        return null
    }
    setPlaygIndex = (i: number) => () => {
        this.setState({ playgIndex: i })
    }

    getPlaygTabBtn = (): ITabNavItem[] => {
        let { club } = this.state
        let arr: ITabNavItem[] = club.playgrounds.map((playg, i) =>({
            label: `Зал №${i + 1}`,
            onClick: this.setPlaygIndex(i),
        }))
        arr.push({
            icon: IconList.plus,
            url: `/club/${club.id}/playground/new`,
        })
        return arr
    }

    render() {
        let { loading, club, sports, playgIndex } = this.state
        let { history } = this.props
        let { clubId } = this.props.match.params
        loading = loading ? loading : !club ? true : false
        let playgState = (!loading && club.playgrounds && club.playgrounds.length) ? true : false
        const playg = club ? club.playgrounds[playgIndex] : null
        const mapUrl = this.coordinates ? `https://static-maps.yandex.ru/1.x/?ll=${this.coordinates[1]},${this.coordinates[0]}&size=580,203&z=13&l=map&pt=${this.coordinates[1]},${this.coordinates[0]},home` : null

        return (
            <Loader state={loading}>
                { !loading ?
                    <div className="wrap">
                        <PageHeaderComponent
                            title="Клуб"
                            status={`Последнее обновление: ${moment(club.updated_at).format('DD.MM.Y')}`}
                            buttons={[
                                { label: 'Редактировать', url: `/club/${clubId}/edit/`, bg: 'blue' },
                            ]}
                        />
                        <SectionComponent>
                            <ClubHeaderComponent
                                subtit={club.club_chain ? `Сеть клубов: ${club.club_chain.name}` : ''}
                                title={club.name}
                                img={club.logo ? club.logo.url : ""}
                                soc={this.soc}
                            />
                            <br/><br/><hr/>
                            <div className="section-geo">
                                <div className="section-geo__wrap">
                                    <div className="section-geo__param">
                                        <div className="section__title">Контакты</div>
                                        <ul>
                                            { this.phones ? 
                                                <li>
                                                    <i><IconComponent icon={IconList.phone}/></i>
                                                    <div className="_text">
                                                        { this.phones.map((el, i) => (
                                                            <span key={i} className="_span">{el.value}</span>
                                                        )) }
                                                    </div>
                                                </li>
                                            : null }
                                            { this.mails ?
                                                <li>
                                                    <i><IconComponent icon={IconList.mail}/></i>
                                                    <div className="_text">
                                                        { this.mails.map((el, i) => (
                                                            <span key={i} className="_span">{el.value}</span>
                                                        )) }
                                                    </div>
                                                </li>
                                                : null }
                                            { this.sites ?
                                                <li>
                                                    <i><IconComponent icon={IconList.pc}/></i>
                                                    <div className="_text">
                                                        { this.sites.map((el, i) => (
                                                            <span key={i} className="_span">{el.value}</span>
                                                        )) }
                                                    </div>
                                                </li>
                                                : null }
                                            { club.address ?
                                                <li>
                                                    <i><IconComponent icon={IconList.geo2}/></i>
                                                    <div className="_text">
                                                        {club.address}
                                                    </div>
                                                </li>
                                                : null }
                                        </ul>
                                    </div>
                                    <div className="section-geo__place" ref={this.geoPlace}>
                                        {mapUrl &&
                                            <img src={mapUrl} />
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="text-area-section">
                                <div className="section__title">О клубе</div>
                                <div className="text-area-section__content" dangerouslySetInnerHTML={{__html: club.description}}>
                                </div>
                            </div>
                            <GalleryComponent
                                title="Фото клуба"
                                id="up3"
                                list={this.imagesCollection}
                                demo
                                scroll
                            />
                            <ToggleSectionComponent
                                title="Инфраструктура"
                                icon={IconList.house}
                                scroll
                            >
                                <PriceListComponent
                                    list={club.infrastructures.map(inf => ({ title: inf.name }))}
                                />
                            </ToggleSectionComponent>
                            <ToggleSectionComponent
                                title="Контакты юридического лица"
                                icon={IconList.persons}
                            >
                                <CheckList
                                    list={[
                                        {key: 'Полное наименование', value: club.company_name},
                                        {key: 'ОГРН', value: club.OGRN},
                                        {key: 'ИНН', value: club.INN},
                                        {key: 'КПП', value: club.KPP},
                                        {key: 'Адрес', value: club.address},
                                        {key: 'Почтовый адрес', value: `${club.post_index}, ${club.entity_address}`},
                                        {key: 'Генеральный директор', value: club.entity_boss_name},
                                        {key: 'Свидетельство о регистрации', value: club.registration_certificate},
                                        {key: 'Дата регистрации', value: club.registration_date},
                                        {key: 'Наименование банка', value: club.bank_name},
                                        {key: 'БИК ', value: club.BIK},
                                        {key: 'Корсчет', value: club.company_account},
                                        {key: 'Номер Р\\С', value: club.number_rs},
                                        {key: 'НДС', value: club.ndsName},
                                    ]}
                                />
                            </ToggleSectionComponent>
                        </SectionComponent>
                        { playgState ?
                            <TabSectionComponent
                                title={club.playgrounds.length>1?"Залы":"Зал"}
                                history={history}
                                nav={this.getPlaygTabBtn()}
                                activeIndex={playgIndex}
                            >
                                <PlaygPageComponent
                                    id={playg.id}
                                    title={playg.name}
                                    subtit={`Зал №${playgIndex+1}`}
                                    logo={playg.avatar && playg.avatar.url}
                                    params={[
                                        {
                                            key: 'Тип помещения',
                                            value: playg.type == 0 ? 'Открытый' : 'Закрытый',
                                        },
                                        {
                                            key: 'Тип покрытия',
                                            value: playg.surfaces.map(s => s.name).join(', '),
                                        },
                                    ]}
                                    services={playg.services}
                                    equipments={playg.equipments}
                                    descriptions={playg.description}
                                    images={playg.images.map(image => ({
                                        order: image.order,
                                        url: image.url
                                    })).sort((a,b)=>a.order - b.order)}
                                    sports={sports ? this.sportsCollection : []}
                                    selectedSports={playg.sport_ids}
                                />
                            </TabSectionComponent>
                        : null }
                    </div>
                : null }
            </Loader>
        )
    }
}

export default ClubComponent
