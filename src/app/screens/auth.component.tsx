import * as React from 'react'
import { Link } from 'react-router-dom'
import * as AuthService from '../services/auth.service'

interface IState {
    hideLogin: boolean
    hideReg: boolean
    login: string,
    password: string,
    [x: string]: any,
}

class AuthComponent extends React.Component<{}, IState> {

    constructor(props) {
        super(props);
        this.state = {
            hideLogin: false,
            hideReg: true,
            login: '',
            password: '',
            lastName:"",
            name:""
        }
        this.handleChange = this.handleChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    showLogin() {
        let {hideLogin} = this.state;
        if (hideLogin) {
            this.setState({ hideLogin: false, hideReg: true})
        }
    }

    showReg() {
        let {hideReg} = this.state;
        if (hideReg) {
            this.setState({ hideLogin: true, hideReg: false })
        }
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit = async (e: any) => {
        if(!!e) {
            e.preventDefault()
        }

        let { login, password } = this.state
        await AuthService.login(login, password)
    }

    render() {
        let { hideLogin, hideReg, login, password } = this.state
        return (
            <div className="authPage">
                <div className="_wrap">
                    <div
                        className={`_popup --login ${hideLogin ? '--hide' : ''}`}
                        onClick={() => this.showLogin()}
                    >
                        <div className="_popup__wrap">
                            <Link to="#" className="_logo">
                                <img src="/assets/images/auth-logo.png" alt="#"/>
                            </Link>
                            <form className="_form --login" onSubmit={this.onSubmit}>
                                <input 
                                    type="text"
                                    className="_input" 
                                    name="login"
                                    value={login}
                                    onChange={this.handleChange} 
                                    placeholder="Логин"
                                />
                                <input 
                                    type="password"
                                    className="_input" 
                                    name="password"
                                    value={password}
                                    onChange={this.handleChange} 
                                    placeholder="Пароль"
                                />
                                <div className="_remeber">
                                    <label className="checkRadio --cir --blue">
                                        <input type="checkbox" name="remember" value="1"/>
                                        <i></i>
                                        <span>Запомнить меня</span>
                                    </label>
                                </div>
                                <button type="submit" className="_btn --blue">
                                    <span>Вход</span>
                                </button>
                                <div className="_afterLink">Забыли пароль?</div>
                            </form>
                        </div>
                    </div>
                    <div
                        className={`_popup --reg ${hideReg ? '--hide' : ''}`}
                        onClick={() => this.showReg()}
                    >
                        <div className="_text">
                            <div className="_title">Зарегистрироваться</div>
                            <div className="_offer">Создавайте клубы, добавляйте их в сети, и следите за прибылью с
                                помощью удобных виджетов которые мы сделали для Вас.
                            </div>
                            <div className="_btn --border">
                                <span>Регистрация</span>
                            </div>
                        </div>
                        <div className="_form --reg">
                            <div className="_title">Регистрация</div>
                            <input className="_input" type="text" placeholder="Фамилия"/>
                            <input className="_input" type="text" placeholder="Имя"/>
                            <input className="_input" type="text" placeholder="Отчество"/>
                            <input className="_input" type="text" placeholder="Компания"/>
                            <input className="_input" type="text" placeholder="Телефон"/>
                            <input className="_input" type="password" placeholder="Пароль"/>
                            <input className="_input" type="text" placeholder="E-mail"/>
                            <div className="_btn --border">
                                <span>Отправить</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AuthComponent