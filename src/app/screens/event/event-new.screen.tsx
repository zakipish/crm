import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

import User from '../../models/user'
import Event from '../../models/event'
import EventForm from './event-form'
import { createEvent } from '../../services/event.service'

interface Props extends RouteComponentProps{
    user?: User
}

interface State {
    event?: Event
    saving: boolean
    errors?: {
        [x: string]: string[]
    }
}

class EventNewScreen extends React.Component<Props,State> {
    state: State = {
        saving: false
    }

    componentDidMount() {
        this.bootstrap()
    }

    bootstrap = () => {
        const event = new Event({})
        this.setState({ event })
    }

    handleChange = (key: string) => (value: any) => {
        const { event: oldEvent } = this.state
        let event = new Event({...oldEvent})
        event[key] = value
        this.setState({ event })
    }

    handleSubmit = () => {
        const { event } = this.state
        this.setState({ saving: true }, () => {
            createEvent(event)
            .then(newEvent => {
                const { history } = this.props
                history.push(`/event/${newEvent.id}`)
            })
            .catch(errors => {
                this.setState({ errors: errors, saving: false })
            })
        })
    }

    render() {
        const { event, saving, errors } = this.state

        return (
            <EventForm event={event} loading={event === undefined || saving} onChange={this.handleChange} onSubmit={this.handleSubmit} errors={errors} />
        )
    }
}

export default withRouter(EventNewScreen)
