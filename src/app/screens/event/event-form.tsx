import * as React from 'react'
import moment from 'moment'

import Config from '../../core/config'

import Sport from '../../models/sport'
import Club from '../../models/club'
import Image from '../../models/image'
import Event, { EventType } from '../../models/event'
import GameLevel from '../../models/game-level'

import Loader from '../../components/layout/loader'
import PageHeaderComponent from '../../components/layout/page-header.component'
import TabLinkPage from '../../components/layout/page/tab-link.page'
import UploadBanners from '../../components/blocks/upload.banners'
import FieldComponent from '../../components/forms/field.component'
import FieldSelectcomponent from '../../components/forms/field-select.component'
import RitchTextareaComponent from '../../components/forms/ritch-field.component'
import Button from '../../components/blocks/button'
import SectionComponent from '../../components/layout/section.component'
import ClubHeaderComponent from '../../components/layout/club-header.component'
import FromToComponent from '../../components/blocks/from-to.component'
import PriceBlock from '../../components/blocks/price.block'
import CheckboxControl from '../../components/forms/checkbox.control'
import DisabledChat from '../../components/blocks/chat/disabled.chat'
import FromToTime from '../../components/blocks/from-to.time'
import InputDatepicker from '../../components/blocks/datepicker/input.datepicker'
import MapComponent from '../../components/map.component'
import GeoBallon from '../../components/blocks/geo.ballon'

import { getClubs } from '../../services/club.service'
import { getSports } from '../../services/sport.service'
import { getGameLevels } from '../../services/game-level.service'

interface Props {
  event: Event
  loading: boolean
  onChange: (key: string) => (value: any) => void
  onSubmit: () => void
  errors?: Errors
}

interface Errors {
  [x: string]: string[]
}

interface State {
  tabActiveIndex: number
  clubs?: Club[]
  gameLevels?: GameLevel[]
  errors: Object
  enableSave: boolean
  sports?: Sport[]
}

const ErrorLabel = (props: { errors: Errors, field: string }) => {
  const errors = props.errors && props.errors[props.field]

  if (errors) {
    return <ul className="errorsList">
      {errors.map((error, i) => <li key={i}><span className="--error">{error}</span></li>)}
    </ul>
  } else {
    return null
  }
}

class EventForm extends React.Component<Props, State> {
  mapWrapperRef = React.createRef<HTMLDivElement>()

  state: State = {
    tabActiveIndex: 0,
    errors: {},
    enableSave: true
  }

  componentDidMount() {
    this.bootstrap()
  }

  componentDidUpdate(prevProps) {
    const { event } = this.props

    if (prevProps.event != event)
    this.validate()
  }

  bootstrap = () => {
    getClubs()
    .then(clubs => this.setState({ clubs }))

    getGameLevels()
    .then(gameLevels => this.setState({ gameLevels }))

    getSports()
    .then(sports => this.setState({ sports }))
  }

  validate = () => {
    const { event } = this.props
    let { enableSave } = this.state

    let errors = {}
    if (!event)
    return true

    if (event.interval && event.interval[0] && event.interval[1])
    if (event.intervalStartMoment.isAfter(event.intervalEndMoment))
    errors['interval'] = 'Конец не может быть раньше начала'

    if (event.max_players && event.min_players)
    if (+event.max_players < +event.min_players)
    errors['players_count'] = 'Максимум не может быть меньше минимума'


    if (event.age_start && event.age_end)
    if(+event.age_end < +event.age_start)
    errors['ages'] = 'Максимум не может быть меньше минимума'


    enableSave = !Object.keys(errors).length && event.images && event.images.length > 0 && !!event.club_id
    this.setState({ errors, enableSave })
  }

  mapOnChange = (point: { lat: number, lon: number }, address: string) => {
    const { event } = this.props

    if (event.type == EventType.Game)
    return true

    const coordinates = [point.lat, point.lon]

    this.handleChange('playground_id', null)
    .then(() => {
      this.handleChange('address', address)
    })
    .then(() => {
      this.handleChange('coordinates', coordinates)
    })
  }

  setTabActive = (i: number) => {
    this.setState({ tabActiveIndex: i })
  }

  setTypeEvent = (i: EventType) => () => {
    this.handleChange('type', i)
  }

  handleChange = async (name: string, value: any) => {
    const { onChange } = this.props
    await onChange(name)(value)

    if (name == 'type') {
      if (value == EventType.Game) {
        this.club && this.club.playgrounds && this.club.playgrounds.length > 0 && this.handleChange('playground_id', this.club.playgrounds[0].id)
      } else {
        this.handleChange('playground_id', null)
      }
    }

    if (name == 'playground_id' && !!value) {
      this.handleChange('coordinates', null)
      .then(() => {
        this.handleChange('address', null)
      })
    }
  }

  handleSubmit = () => {
    const { onSubmit, event } = this.props
    const { errors } = this.state

    if (!Object.keys(errors).length && event.images && event.images.length > 0)
    onSubmit()
  }

  get address() {
    const { event } = this.props
    let address

    if (!this.club)
    return address

    if (event.type == EventType.Game || event.playground_id) {
      if (this.club.address) {
        address = this.club.address
      } else {
        address = `Клуб «${this.club.name}»`
      }
    } else {
      address = event.address
    }

    return address
  }

  get point() {
    const { event } = this.props
    let coordinates
    if (event.type == EventType.Game || !event.coordinates) {
      if (this.club.coordinates) {
        coordinates = this.club.coordinatesArr
      } else {
        return null
      }
    } else {
      coordinates = event.coordinates
    }

    return {
      lat: +coordinates[0],
      lon: +coordinates[1]
    }
  }

  get timeStart() {
    const { event } = this.props
    return event && event.interval ? event.interval[0].split(':') : null
  }

  get timeEnd() {
    const { event } = this.props
    return event && event.interval ? event.interval[1].split(':') : null
  }

  get club() {
    const { event } = this.props
    const { clubs } = this.state
    return clubs && event && event.club_id ? clubs.find(club => club.id == event.club_id) : null
  }

  get datesCollection() {
    let dates = {}
    for (let i = moment(); i.isBefore(moment().add(30, 'days')); i.add(1, 'day')) {
      dates[i.format('YYYY-MM-DD')] = i.format('D MMM YY')
    }

    return dates
  }

  get agesCollection() {
    let ages = {}

    for(let i = 1; i < 100; i++) {
      ages[i] = '' + i
    }

    return ages
  }

  get hoursCollection() {
    let hours = []

    for(let i = 0; i < 24; i++) {
      const str = (i < 10 ? '0' : '') + i
      hours.push(str)
    }

    return hours
  }

  get minutesCollection() {
    let minutes = []

    for(let i = 0; i < 60; i++) {
      const str = (i < 10 ? '0' : '') + i
      minutes.push(str)
    }

    return minutes
  }

  get playgroundsCollection() {
    const club = this.club

    if (club && club.playgrounds)
    return club.playgrounds.reduce((arr, playground) => {
      arr[playground.id] = playground.name
      return arr
    }, {})

    return {}
  }

  get sportsCollection() {
    const { event } = this.props
    let sports = []

    if (event.type == EventType.Game) {
      const club = this.club

      if (club && club.sports)
      sports = club.sports
    } else {
      sports = this.state.sports
    }

    return sports.reduce((arr, sport) => {
      arr[sport.id] = sport.name
      return arr
    }, {})
  }

  render() {
    const { loading, event, errors: apiErrors } = this.props
    const { clubs, gameLevels, tabActiveIndex, errors, enableSave } = this.state
    const club = this.club
    const typeEvent = event && event.type

    return (
      <Loader state={loading}>
      {!loading && (
        <div className="event wrap">
        <PageHeaderComponent
          title="Кипиш"
          subtitle="Создание спортивного мероприятия"
          status={`Последнее обновление: ${moment(event.updated_at).format('DD.MM.Y')}`}
        />
        <TabLinkPage
          active={tabActiveIndex}
          onSelect={this.setTabActive}
        >
        <span>Оформление</span>
        <span>Информация</span>
        </TabLinkPage>

        {(tabActiveIndex === 0) && (<>
          <UploadBanners
            title={event.title}
            value={event.images}
            onChange={(images) => this.handleChange('images', images)}
          />
          <FieldComponent
            name="title"
            placeholder="Заголовок очень важен для рекламы"
            value={event.title}
            onChange={(e) => this.handleChange('title', e.target.value)}
          />
          <ErrorLabel errors={apiErrors} field="title" />
          <br />
          <RitchTextareaComponent
            title="Описание кипиша"
            name="description"
            defaultValue={event.description}
            onChange={(e) => this.handleChange('description', e.target.value)}
          />
          <ErrorLabel errors={apiErrors} field="description" />
          <div className="event__btns">
            <br />
            <Button
              color="green"
              onClick={() => this.setTabActive(1)}
            >Далее</Button>
          </div>
        </>)}
        {(tabActiveIndex === 1) && (<>
          <SectionComponent>
            { club && (<>
              <ClubHeaderComponent
                subtit={club && club.club_chain ? `Сеть клубов: ${event.club.club_chain.name}` : ''}
                title={club.name}
                img={club.logo ? club.logo.url : ""}
              />
              <hr />
            </>)}
            <FieldSelectcomponent
              title="Клуб"
              name="club_id"
              options={clubs.reduce((arr, club) => {
                arr[club.id] = club.name
                return arr
              }, {})}
              value={event.club_id + ''}
              onChange={this.handleChange}
            />
            <ErrorLabel errors={apiErrors} field="club_id" />
            <div className="event__line">
              <div className="event__colLeft">
                <FromToComponent title="Число участников" error={errors['players_count']}>
                  <FieldComponent
                    name="min_players"
                    value={event.min_players}
                    onChange={(e) => this.handleChange('min_players', e.target.value)}
                  />
                  <FieldComponent
                    name="max_players"
                    value={event.max_players}
                    onChange={(e) => this.handleChange('max_players', e.target.value)}
                  />
                </FromToComponent>
              </div>
              <div className="event__colRight">
                <PriceBlock
                  title="Цена"
                  value={'' + event.price}
                  onChange= { (value) => this.handleChange('price', +value) }
                  disabled={event.freeEnterCheckbox}
                />
                <CheckboxControl
                  title="Свободный вход"
                  name="value"
                  defaultChecked={event.freeEnterCheckbox}
                  color="blue"
                  onChange={(e) => this.handleChange('freeEnterCheckbox', e.target.checked)}
                />
              </div>
            </div>
          </SectionComponent>
          {(typeEvent == null) && (<SectionComponent>
            <div className="section-half">
              <div className="section-half__item">
                <DisabledChat
                  image="/assets/images/event-type-game.png"
                  text="Чтобы пользоваться функцией чата пройдите 2-х минутную регистрация на сайте ZaKipish, и откройте для себя безграничный мир развлечений"
                >
                  <Button
                    color="orange"
                    onClick={this.setTypeEvent(EventType.Game)}
                  >Игра</Button>
                </DisabledChat>
              </div>
              <div className="section-half__item">
                <DisabledChat
                  image="/assets/images/event-type-game.png"
                  text="Чтобы пользоваться функцией чата пройдите 2-х минутную регистрация на сайте ZaKipish, и откройте для себя безграничный мир развлечений"
                >
                  <Button
                    color="green"
                    onClick={this.setTypeEvent(EventType.Meeting)}
                  >Встреча</Button>
                </DisabledChat>
              </div>
            </div>
          </SectionComponent>)}
          {(typeEvent != null) && (<>
            <SectionComponent>
              <div className="section-half">
                <div className="section-half__item">
                  <div className="formRow --titLarge">
                    <div className="formRow__item">
                      {(typeEvent == EventType.Game) && (<>
                        <FieldSelectcomponent
                          title="Зал"
                          name="playground_id"
                          options={this.playgroundsCollection}
                          value={event.playground_id + ''}
                          onChange={this.handleChange}
                        />
                        <ErrorLabel errors={apiErrors} field="playground_id" />
                      </>)}
                      {(typeEvent == EventType.Meeting) && (<>
                        <FieldSelectcomponent
                          title="Место"
                          name="playground_id"
                          forceTitle={event.type == EventType.Meeting && event.address}
                          options={this.playgroundsCollection}
                          value={event.playground_id + ''}
                          onChange={this.handleChange}
                        />
                        <ErrorLabel errors={apiErrors} field="place" />
                      </>)}
                    </div>
                  </div>
                  <div className="formRow --titLarge --topStart">
                    <div className="formRow__item">
                      <FromToTime title="Время" error={errors['interval']}>
                        <FieldSelectcomponent
                          name="intervalStartHours"
                          options={this.hoursCollection}
                          value={event.intervalStartHours}
                          onChange={this.handleChange}
                        />
                        <FieldSelectcomponent
                          name="intervalStartMinutes"
                          options={this.minutesCollection}
                          value={event.intervalStartMinutes}
                          onChange={this.handleChange}
                        />
                        <FieldSelectcomponent
                          name="intervalEndHours"
                          options={this.hoursCollection}
                          value={event.intervalEndHours}
                          onChange={this.handleChange}
                        />
                        <FieldSelectcomponent
                          name="intervalEndMinutes"
                          options={this.minutesCollection}
                          value={event.intervalEndMinutes}
                          onChange={this.handleChange}
                        />
                      </FromToTime>
                      <ErrorLabel errors={apiErrors} field="interval" />
                    </div>
                    <div className="formRow__item">
                      <InputDatepicker
                        autoHide={true}
                        title="Дата"
                        date={event.date}
                        onChange={(value) => this.handleChange('date', value)}
                        format="DD.MM.YYYY"
                        apiFormat="YYYY-MM-DD"
                        disablePast={true}
                      />
                      <ErrorLabel errors={apiErrors} field="date" />
                      {(typeEvent == EventType.Meeting) && event.playground_id && (<>
                        <br />
                        <CheckboxControl
                          title="Блокировать время"
                          name="blockSchedule"
                          defaultChecked={event.blockSchedule}
                          color="blue"
                          onChange={(e) => this.handleChange('blockSchedule', e.target.checked)}
                        />
                      </>)}
                    </div>
                  </div>
                  <hr />
                  <div className="formRow">
                    <div className="formRow__item">
                      <FieldSelectcomponent
                        title="Спорт"
                        name="sport_id"
                        options={this.sportsCollection}
                        value={event.sport_id + ''}
                        onChange={this.handleChange}
                      />
                      <ErrorLabel errors={apiErrors} field="sport_id" />
                    </div>
                  </div>
                  {(typeEvent == EventType.Game) && (<div className="formRow">
                    <div className="formRow__item">
                      <FieldSelectcomponent
                        title="Уровень игры"
                        name="game_level_id"
                        options={gameLevels.reduce((arr, gameLevel) => {
                          arr[gameLevel.id] = gameLevel.name
                          return arr
                        }, {})}
                        value={event.game_level_id + ''}
                        onChange={this.handleChange}
                      />
                      <ErrorLabel errors={apiErrors} field="game_level_id" />
                    </div>
                    <div className="formRow__item">
                      <FromToComponent
                        title="Возраст"
                        className="--age"
                        error={errors['ages']}
                      >
                        <FieldSelectcomponent
                          name="age_start_str"
                          value={event.age_start_str}
                          options={this.agesCollection}
                          onChange={this.handleChange}
                        />
                        <FieldSelectcomponent
                          name="age_end_str"
                          value={event.age_end_str}
                          options={this.agesCollection}
                          onChange={this.handleChange}
                        />
                      </FromToComponent>
                    </div>
                  </div>)}
                  {(typeEvent == EventType.Meeting) && (<div className="event__chatRadio">
                    <img src="/assets/images/chat.svg" alt="#" />
                    <CheckboxControl
                      title="Включить чат в этой встрече"
                      name="chat_disabled"
                      checked={!event.chat_disabled}
                      color="white"
                      onChange={ (e) => this.handleChange('chat_disabled', !e.target.checked) }
                    />
                  </div>)}
                </div>
                <div className="section-half__item">
                  <div className="event__geo" ref={this.mapWrapperRef}>
                    {club &&
                      <MapComponent
                        parentRef={this.mapWrapperRef}
                        point={this.point}
                        pointUrl={`${Config.api_host}/geopoint/club/${club.id}.png`}
                        zoom={14}
                        onChange={this.mapOnChange}
                      />
                    }
                    <GeoBallon>
                      {this.address}
                    </GeoBallon>
                  </div>
                </div>
              </div>
            </SectionComponent>
            <div className="event__btns">
              {(typeEvent == EventType.Meeting) && (<div className="event__typeToggle">
                <span>Перейти:</span>
                <Button
                  color="orange"
                  onClick={this.setTypeEvent(EventType.Game)}
                >Игра</Button>
              </div>)}
              {(typeEvent == EventType.Game) && (<div className="event__typeToggle">
                <span>Перейти:</span>
                <Button
                  color="green"
                  onClick={this.setTypeEvent(EventType.Meeting)}
                >Встреча</Button>
              </div>)}
              <Button
                onClick={this.handleSubmit}
                type="submit"
                color="blue"
                disabled={!enableSave}
              >{event.id ? 'Сохранить' : 'Создать'}</Button>
            </div>
          </>)}
        </>)}
      </div>)}
    </Loader>)
  }
}

export default EventForm
