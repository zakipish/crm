import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

import Config from '../../core/config'
import Event from '../../models/event'
import { getEvent } from '../../services/event.service'

import Loader from '../../components/layout/loader'
import PageHeaderComponent from '../../components/layout/page-header.component'
import Profile from '../../components/blocks/profile'
import IconComponent, { IconList } from '../../components/blocks/icon/icon.component'
import ChatComponent from '../../components/blocks/chat/chat.component'

interface MatchParams {
    slug: string
}

interface Props extends RouteComponentProps<MatchParams> {}

interface State {
    event?: Event
    loading: boolean
}

class EventProfileScreen extends React.Component<Props, State> {
    state: State = {
        loading: true
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.slug != this.props.match.params.slug)
            this.bootstrap()
    }

    bootstrap = () => {
        const { slug } = this.props.match.params
        this.setState({ loading: true }, () => {
            getEvent(slug)
            .then(event => {
                this.setState({ event: event, loading: false })
            })
        })
    }

    render() {
        const { loading, event } = this.state
        return (
            <Loader state={loading}>
            {!loading && (
                <div className="wrap">
                    <PageHeaderComponent
                        title="Кипиш"
                    />
                    <div className="eventProfile">
                        <div className="eventProfile__chat">
                            {(event && event.chat_id) && (
                                <ChatComponent 
                                    chat_id={event.chat_id} 
                                    playersCount={event.defenitelyUsers.length}
                                    disabled={event.chat_disabled}
                                    disabledLink={<span>Включить чат в этой встречи</span>}
                                />
                            )}
                        </div>
                        <div className="eventProfile__profile">
                            <Profile
                                image={event.images && event.images.length > 0 && event.images[0].url}
                                editTo={`/event/${event.slug}/edit`}
                                title={event.title}
                                siteUrl={`${Config.front_domain}/events/${event.slug}`}
                                params={[
                                    { color: 'blue', icon: IconList.personeFill, tit: 'Участники', val: event.defenitelyUsers.length + '' },
                                    { color: 'yellow', icon: IconList.personeFillPlus, tit: 'Возможные', val: event.possibleUsers.length + '' },
                                    { color: 'pink', icon: IconList.personeBord, tit: 'Отказались', val: event.declinedUsers.length + '' },
                                ]}
                                price={event.price}
                                interval={{
                                    from: event.intervalStartMoment,
                                    to: event.intervalEndMoment
                                }}
                            />
                        </div>
                    </div>
                </div>
            )}
            </Loader>
        )
    }
}

export default withRouter(EventProfileScreen)
