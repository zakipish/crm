import * as React from 'react'
import { connect } from 'react-redux'
import PopupListEvent from '../../components/layout/event/popup-list.event'
import CreateCardEvent from '../../components/layout/event/create-card.event'
import CardEvent from '../../components/layout/event/card.event'
import ListEvent from '../../components/layout/event/list.event'
import TabLinkPage from '../../components/layout/page/tab-link.page'
import SearchLine from '../../components/blocks/search.line'
import Event from '../../models/event'
import * as EventService from '../../services/event.service'

interface Props {
    events: Event[]
}

interface State {
    pastEvents?: EventCardCollection[]
    featureEvents?: EventCardCollection[]
    tabActiveIndex: number
    filter?: string
}

interface EventCardCollection {
    id: number
    link: string
    image: string
    title: string
    justLen: number
    likeLen: number
    price: number
}

class EventListScreen extends React.Component<Props, State> {

    state: State = {
        tabActiveIndex: 0
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.events && this.props.events)
            this.bootstrap()
    }

    bootstrap = async () => {
        await EventService.downloadEvents()
        const { events } = this.props
        const pastEvents = events && events.filter(event => event.isPast).map(this.convertEvent)
        const featureEvents = events && events.filter(event => !event.isPast).map(this.convertEvent)
        this.setState({ pastEvents, featureEvents })
    }

    handleFilterChange = (filter: string) => {
        this.setState({ filter })
    }

    convertEvent = (event: Event) => ({
        id: event.id,
        link: `/event/${event.slug}`,
        image: event.images && event.images.length > 0 && event.images[0].url,
        title: event.title,
        justLen: event.defenitelyUsers.length,
        likeLen: event.possibleUsers.length,
        price: event.price,
        intervalStartMoment: event.intervalStartMoment,
    })

    setTabActive = (i: number) => {
        this.setState({ tabActiveIndex: i })
    }

    render() {
        const { tabActiveIndex, pastEvents, featureEvents, filter } = this.state
        const { events } = this.props

        let eventsCollection
        if (tabActiveIndex == 0) {
            eventsCollection = featureEvents
        } else {
            eventsCollection = pastEvents
        }

        if (filter)
            eventsCollection = eventsCollection.filter(event => event.title.toLowerCase().indexOf(filter) > -1)

        return (
            <PopupListEvent>
                <ListEvent top={
                    <React.Fragment>
                        <SearchLine onChange={this.handleFilterChange} />
                        <TabLinkPage
                            offset={20}
                            active={tabActiveIndex}
                            onSelect={this.setTabActive}
                        >
                            <span>Будут</span>
                            <span>Состоялись</span>
                        </TabLinkPage>
                    </React.Fragment>
                }>
                    <CreateCardEvent to="/event/new">Добавить кипиш</CreateCardEvent>
                    {eventsCollection && eventsCollection.map(event => (
                        <CardEvent
                            key={event.id}
                            date={event.intervalStartMoment}
                            to={event.link}
                            image={event.image}
                            title={event.title}
                            justLen={event.justLen}
                            likeLen={event.likeLen}
                            price={event.price}
                        />
                    ))}
                </ListEvent>
            </PopupListEvent>
        )
    }
}

const mapStateToProps = state => ({
    events: state.eventState.events,
})

export default connect(mapStateToProps)(EventListScreen)
