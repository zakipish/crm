import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

import User from '../../models/user'
import Event from '../../models/event'
import EventForm from './event-form'
import { getEvent, updateEvent } from '../../services/event.service'

interface MatchParams {
    slug: string
}

interface Props extends RouteComponentProps<MatchParams> {
    user?: User
}

interface State {
    event?: Event
    saving: boolean
    errors?: {
        [x: string]: string[]
    }
}

class EventEditScreen extends React.Component<Props,State> {
    state: State = {
        saving: false
    }

    componentDidMount() {
        this.bootstrap()
    }

    bootstrap = () => {
        const { slug } = this.props.match.params
        getEvent(slug)
        .then(event => {
            this.setState({ event: event })
        })
    }

    handleChange = (key: string) => async (value: any) => {
        const { event: oldEvent } = this.state
        let event = new Event({...oldEvent})
        event[key] = value
        this.setState({ event })
        if (key === 'images') {
            await updateEvent(event.slug, event)
                .then(newEvent => {
                    // Do nothing for now
                })
                .catch(errors => {
                    this.setState({ errors: errors, saving: false })
                })
        }
    }

    handleSubmit = () => {
        const { event } = this.state
        this.setState({ saving: true }, () => {
            updateEvent(event.slug, event)
            .then(newEvent => {
                const { history } = this.props
                history.push(`/event/${newEvent.id}`)
            })
            .catch(errors => {
                this.setState({ errors: errors, saving: false })
            })
        })
    }

    render() {
        const { event, saving, errors } = this.state

        return (
            <EventForm
                event={event}
                loading={event === undefined || saving}
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                errors={errors}
            />
        )
    }
}

export default withRouter(EventEditScreen)
