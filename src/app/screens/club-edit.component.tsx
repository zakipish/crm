import * as React from 'react'
import { connect } from "react-redux"
import { History } from 'history'
import moment from 'moment'

import { reqGet, reqPatch } from '../core/api'

import Club from '../models/club'
import City from '../models/city'
import Contact from '../models/contact'
import ContactType, { ContactSocTypes } from '../models/contact-type'
import Infrastructure from '../models/infrastructure'

import { IconList, getSqSocIcon } from '../components/blocks/icon/icon.component'
import EditPageComponent, { IEditPageState } from '../components/_shared/edit-page.component'
import Loader from '../components/layout/loader'
import PageHeaderComponent from '../components/layout/page-header.component'
import SectionComponent from '../components/layout/section.component'
import UploadComponent from '../components/forms/upload.component'
import FieldSelectcomponent from '../components/forms/field-select.component'
import FieldComponent from '../components/forms/field.component'
import FieldDadataSelectComponent from '../components/forms/field-dadata-select.component'
import RitchTextareaComponent from '../components/forms/ritch-field.component'
import GalleryComponent from '../components/blocks/gallery/gallery.component'
import ContactFieldComponent from '../components/forms/contact-field.component'
import FieldSocialComponent from '../components/forms/field-social.component'
import {history} from "../core/history";


interface ClubEditProps {
    history: History,
    location: any,
    match: any
}

class ClubEditComponent extends EditPageComponent<ClubEditProps, IEditPageState> {
    state: IEditPageState = {
        item: new Club(),
        loading: true,
        options: {},
    }

    componentDidMount() {
        this.bootstrap()
    }

    bootstrap = () => {
        this.getClub()
        this.getInfrastructures()
        this.getContactTypes()
        this.getCities()
    }

    getContactTypes = () => {
        reqGet('/contact_types')
        .then(response => {
            const { options } = this.state
            options['contactTypes'] = response.data.map(data => new ContactType(data))
        })
    }

    getCities = () => {
        reqGet('/cities')
        .then(response => {
            const { options } = this.state
            options['cities'] = response.data.map(data => new City(data))
        })
    }

    get citiesCollection() {
        const { cities } = this.state.options
        return cities.reduce((res, city) => {
            res[city.id] = city.name
            return res
        }, {})
    }

    get ndsOptions() {
        return Club.ndsOptions
    }

    handleChangeContact = (i: number) => (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const value = e.target.value
        let { item } = this.state
        item.contacts[i].value = value
        this.setState({ item })
    }

    getContactTypeId = (slug: string): number => {
        const { contactTypes } = this.state.options
        const index = contactTypes.findIndex(type => type.slug === slug)

        if (index > -1) {
            return contactTypes[index].id
        } else {
            return null
        }
    }

    getContactType = (slug: string): ContactType => {
        const { contactTypes } = this.state.options
        const index = contactTypes.findIndex(type => type.slug === slug)
        if (index > -1) {
            return contactTypes[index]
        } else {
            return null
        }
    }

    getContactTypeById = (id: number): ContactType => {
        const { contactTypes } = this.state.options
        const index = contactTypes.findIndex(type => type.id === id)
        if (index > -1) {
            return contactTypes[index]
        } else {
            return null
        }
    }

    getContactCollection = (contact_type_id: number) => {
        const { contacts } = this.state.item

        return contacts.map((contact, i) => {
            if (contact.contact_type_id === contact_type_id) {
                return {
                    name: 'value',
                    value: contact.value,
                    onChange: this.handleChangeContact(i),
                    onRemove: this.removeContact(i)
                }
            }
        }).filter(el => el)
    }

    addContact = (contact_type_id: number) => () => {
        let { item } = this.state
        let contact_type = this.getContactTypeById(contact_type_id)
        item.contacts.push(new Contact({ 
            contact_type_id: contact_type_id,
            contact_type
        }))
        this.setState({ item })
    }

    removeContact = (i: number) => () => {
        let { item } = this.state
        let { contact_type_id } = item.contacts[i]
        item.contacts.splice(i, 1)
        if (item.contacts.filter(contact => contact.contact_type_id === contact_type_id ).length < 1) {
            let contact_type = this.getContactTypeById(contact_type_id)
            item.contacts.push(new Contact({ 
                contact_type_id: contact_type_id,
                contact_type
            }))
        } 
        this.setState({ item })
    }

    getInfrastructures = () => {
        reqGet('/infrastructures')
        .then(response => {
            const { options } = this.state
            options['infrastructures'] = response.data.map(data => new Infrastructure(data))
        })
    }

    getClub = () => {
        const { clubId } = this.props.match.params
        let { options } = this.state
        reqGet(`/clubs/${clubId}`)
        .then(response => {
            options['company_name'] = response.data.company_name
            options['address'] = ''

            if (response.data.city)
                options['address'] += `г ${response.data.city.name}`

            if (response.data.post_street)
                options['address'] += `, ${response.data.post_street}`

            if (response.data.post_building)
                options['address'] += `, ${response.data.post_building}`

            if (response.data.post_building_number)
                options['address'] += `, ${response.data.post_building_number}`

            this.setState(
                { item: response.data, loading: false, options },
                this.setDefaultContacts
            )
        })
        .catch(e => console.log(e.message))
    }

    setDefaultContacts = () => {
        const { contactTypes } = this.state.options

        if (!contactTypes) {
            return null
        }

        let contact_types = [
            this.getContactType('telefon'),
            this.getContactType('e-mail'),
            this.getContactType('sajt'),
        ]

        contact_types.forEach(this.setDefaultContact)
    }

    setDefaultContact = (contact_type) => {
        let { item } = this.state
        if (item.contacts.filter(contact => contact.contact_type_id === contact_type.id ).length < 1) {
            item.contacts.push(new Contact({ 
                contact_type_id: contact_type.id,
                contact_type
            }))
        }
        this.setState({ item })
    }

    saveClub = () => {
        const { clubId } = this.props.match.params
        const { item } = this.state

        this.setState({ loading: true },
            () => reqPatch(`/clubs/${clubId}`, JSON.stringify(item))
            .then(response =>{
                this.setState(
                    { item: response.data, loading: false },
                    this.setDefaultContacts
                )
                history.push(`/club/${clubId}`)
            })
            .catch(e => console.log(e.message))
         )
    }

    getInfrastructureName = (id: number) => {
        const { infrastructures } = this.state.options
        const infIndex = infrastructures.findIndex(inf => (inf.id === id))
        if (infIndex > -1) {
            return infrastructures[infIndex].name
        } else {
            return ''
        }
    }

    get infrastructuresSelectCollection() {
        let { infrastructures } = this.state.options
        let { infrastructure_ids } = this.state.item
        let filterList = infrastructures.filter(el => !infrastructure_ids.includes(el.id)) 
        return filterList.reduce((infs, inf) => {
            infs[inf.id] = inf.name
            return infs
        }, {})
    }

    selectInfrastucture = (name: string,  id: string) => {
        const { infrastructures } = this.state.options
        let { item } = this.state

        const infrastructure = infrastructures.find(option => option.id == id)
        if (infrastructure) {
            item.infrastructures.push(infrastructure);
            this.setState({ item })
        }
    }

    createInfrastructure = (name: string, value: string) => {
        let { item } = this.state
        item.infrastructures.push({ id: null, name: value })
        this.setState({ item })
    }

    deleteInfrastructure = (i: number) => (e: any) => {
        let { item } = this.state
        item.infrastructure_ids = item.infrastructure_ids.splice(0, i)
        this.setState({ item })
    }

    get soc() {
        let soc = null
        let club = this.state.item
        if (club && club.contacts && club.contacts.length) {
            let contacts = club.contacts.filter(el => ContactSocTypes.includes(el.contact_type.slug))
            soc = contacts.map(el => ({ 
                value: el.value, 
                slug: el.contact_type.slug 
            }))
        }
        return soc
    }

    onSocAdd = (soc) => {
        let { item } = this.state
        let { contacts } = item
        let contact_type = this.getContactType(soc.slug)
        contacts.push({ 
            value: soc.value, 
            contact_type_id: contact_type.id,
            contact_type
        })
        item.contacts = contacts
        this.setState({item})
    }
    
    onSocRemove = (slug) => {
        let { item } = this.state
        let { contacts } = item
        let contact_type_id = this.getContactTypeId(slug)
        let index = contacts.findIndex(contact => contact.contact_type_id === contact_type_id)
        contacts.splice(index, 1)
        item.contacts = contacts
        this.setState({item})
    }

    bankAutocomplete = (response: any) => {
        const data = response.data
        let { item } = this.state

        if (!data)
            return true

        if (data.name && data.name.short)
            item.bank_name = data.name.short

        if (data.correspondent_account)
            item.company_account = data.correspondent_account

        if (data.bic)
            item.BIK = data.bic

        this.setState({ item })
    }

    companyAutocomplete = (response: any) => {
        const data = response.data
        let { item } = this.state

        if (!data)
            return true

        if (data.address && data.address.unrestricted_value)
            item.entity_address = data.address.unrestricted_value

        if (data.inn)
            item.INN = data.inn

        if (data.kpp)
            item.KPP = data.kpp

        if (data.ogrn)
            item.OGRN = data.ogrn

        if (data.management) {
            if (data.management.name)
                item.entity_boss_name = data.management.name

            if (data.management.post)
                item.entity_boss_position = data.management.post
        }

        if (data.name && data.name.short_with_opf) {
            item.company_name = data.name.short_with_opf
            const ipParse = data.name.short_with_opf.match(/ИП\s(.*)/)
            if (ipParse) {
                item.entity_boss_name = ipParse[1]
                item.entity_boss_position = 'Индивидуальный предприниматель'
            }
        }

        if (data.state && data.state.registration_date)
            item.registration_date = moment(data.state.registration_date).format('DD.MM.YYYY')

        this.setState({ item })
    }

    shortAddressAutocomplete = (response: any) => {
        const value = response.value
        if (!value)
            return true

        let { item } = this.state
        item.address = value

        const { data } = response
        if (data.geo_lat && data.geo_lon)
            item.coordinates = `(${data.geo_lat},${data.geo_lon})`

        this.setState({ item })
    }

    addressAutocomplete = (response: any) => {
        const data = response.data
        if (!data)
            return true

        let { item, options } = this.state

        if (data.city && options.cities) {
            const city = options.cities.find(city => city.name == data.city)
            if (city)
                item.city_id = city.id
        }

        if (data.postal_code)
            item.post_index = data.postal_code

        if (data.region_with_type)
            item.region = data.region_with_type

        if (data.street_with_type)
            item.post_street = data.street_with_type

        if (data.house && data.house_type)
            item.post_building = `${data.house_type} ${data.house}`

        if (data.flat && data.flat_type)
            item.post_building_number = `${data.flat_type} ${data.flat}`

        this.setState({ item })
    }

    render() {
        let { item, loading } = this.state
        let { clubId } = this.props.match.params
        let { infrastructures, cities, contactTypes, company_name, address } = this.state.options
        loading = loading ? loading : !item ? true : false
        return (
            <Loader state={loading}>
                { !loading ?
                    <div className="wrap">
                        <PageHeaderComponent
                            title="Карточка клуба"
                            buttons={[
                                {label: 'Отмена', url: `/club/${clubId}/`},
                                {
                                    label: 'Сохранить',
                                    url: `/club/${clubId}/`,
                                    bg: 'blue',
                                    onClick: this.saveClub
                                },
                            ]}
                        />
                        <SectionComponent>
                            <div className="formRow">
                                <div className="formRow__item --auto">
                                    <UploadComponent
                                        id="up1"
                                        title="Загрузить лого"
                                        src={item.logo ? item.logo.url : ''}
                                        logo={item.logo}
                                        onUpload={(arrFiles, files) => this.onUploadImage(`/clubs/${clubId}`, "logo", arrFiles, files)}
                                        onDelete={this.asyncDeleteImage(`/clubs/${clubId}`, 'logo')}
                                        size={{width: 90, height: 90}}
                                    />
                                </div>
                                <div className="formRow__item">
                                    <FieldComponent
                                        title="Название клуба"
                                        name="name"
                                        value={item.name}
                                        placeholder="Введите название клуба"
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                            <div className="formRow">
                                <div className="formRow__item">
                                    <RitchTextareaComponent
                                        title="Описание клуба"
                                        name="description"
                                        defaultValue={item.description}
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__title">Адрес Клуба</div>
                                <div className="formRow__item --cols12">
                                    <FieldDadataSelectComponent
                                        type="address"
                                        onSelect={this.shortAddressAutocomplete}
                                        placeholder="Введите точный адрес"
                                        value={address}
                                    />
                                </div>

                                {/*<div className="formRow__item --cols12">
                                    <div className="geocard">
                                        <div className="geocard__place"></div>
                                    </div>
                                    <div className="label-list">
                                        <div className="label --orange">
                                            <div className="label__icon">
                                                <IconComponent icon={IconList.rocket}/>
                                            </div>
                                            <div className="label__text">
                                                <div className="label__tit">Лиговский проспект</div>
                                                <div className="label__val">500 метров</div>
                                            </div>
                                        </div>
                                        <div className="label --red">
                                            <div className="label__icon">
                                                <IconComponent icon={IconList.rocket}/>
                                            </div>
                                            <div className="label__text">
                                                <div className="label__tit">Лиговский проспект</div>
                                                <div className="label__val">500 метров</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>*/}

                            </div>

                            { contactTypes ?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__title">Контакты</div>

                                    <div className="formRow__item --col4">
                                        <ContactFieldComponent
                                            type="telefon"
                                            contact_type_id={this.getContactTypeId('telefon')}
                                            list={this.getContactCollection(this.getContactTypeId('telefon'))}
                                            onAdd={this.addContact(this.getContactTypeId('telefon'))}
                                        />
                                    </div>

                                    <div className="formRow__item --col4">
                                        <ContactFieldComponent
                                            type="e-mail"
                                            contact_type_id={this.getContactTypeId('e-mail')}
                                            list={this.getContactCollection(this.getContactTypeId('e-mail'))}
                                            onAdd={this.addContact(this.getContactTypeId('e-mail'))}
                                        />
                                    </div>
                                    <div className="formRow__item --col4">
                                        <ContactFieldComponent
                                            type="sajt"
                                            contact_type_id={this.getContactTypeId('sajt')}
                                            list={this.getContactCollection(this.getContactTypeId('sajt'))}
                                            onAdd={this.addContact(this.getContactTypeId('sajt'))}
                                        />
                                    </div>

                                </div>
                            : '' }

                            
                            <div className="formRow">
                                <div className="formRow__item --col12">
                                    <FieldSocialComponent 
                                        list={this.soc}
                                        onAdd={this.onSocAdd}
                                        onRemove={this.onSocRemove}
                                    />
                                </div>
                            </div>

                            { infrastructures ?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__item --col12">
                                        <FieldSelectcomponent
                                            title="Инфраструктура"
                                            name="infrastructures"
                                            placeholder="Добавьте свои варианты или выберите из списка..."
                                            options={this.infrastructuresSelectCollection}
                                            onChange={this.selectInfrastucture}
                                            onClickEmpty={this.createInfrastructure}
                                            emptyTitle={'Создать'}
                                            disableKeysSelect={true}
                                        />
                                    </div>
                                    <div className="formGrid">
                                        {item.infrastructures.map((infrastructure, i) => (
                                            <React.Fragment key={i}>
                                                <FieldComponent
                                                    value={infrastructure.name}
                                                    onChange={() => {}}
                                                    viewOnly={true}
                                                    icons={[{ icon: IconList.del, bg: 'red', onClick: this.deleteInfrastructure(i) }]}
                                                />
                                            </React.Fragment>
                                        ))}
                                    </div>
                                </div>
                            : '' }

                            <GalleryComponent
                                title="Фото клуба"
                                id="up2"
                                url={`/clubs/${clubId}`}
                                list={this.imagesCollection}
                                onChange={(arrFiles) => this.onChangeGallery(arrFiles, 'images')}
                                onAdd={this.loadFiles(`/clubs/${clubId}`)}
                                onRemove={this.deleteUploadedImage(`/clubs/${clubId}`)}
                            />
                        </SectionComponent>
                        <SectionComponent>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__title">Организация или ИП</div>
                                <div className="formRow__item --col12">
                                    <FieldDadataSelectComponent
                                        type="company"
                                        onSelect={this.companyAutocomplete}
                                        value={company_name}
                                        placeholder="Введите название юр. лица или ИНН"
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldDadataSelectComponent
                                        title="БИК"
                                        type="bank"
                                        field="bic"
                                        value={item.BIK}
                                        placeholder="БИК"
                                        onSelect={this.bankAutocomplete}
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldComponent
                                        title="Номер Р\С"
                                        name="number_rs"
                                        value={item.number_rs}
                                        placeholder="Номер Р\С"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldSelectcomponent
                                        title="НДС"
                                        name="NDS"
                                        options={this.ndsOptions}
                                        onChange={this.selectSelect}
                                        value={item.NDS}
                                    />
                                </div>
                                <hr/>
                                <div className="formRow__item --col12">
                                    <FieldComponent
                                        title="Наименование"
                                        name="company_name"
                                        value={item.company_name}
                                        placeholder="Наименование организации"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldComponent
                                        title="ИНН"
                                        name="INN"
                                        value={item.INN}
                                        placeholder="ИНН организации"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldComponent
                                        title="КПП"
                                        name="KPP"
                                        value={item.KPP}
                                        placeholder="КПП организации"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col4">
                                    <FieldComponent
                                        title="ОГРН"
                                        name="OGRN"
                                        value={item.OGRN}
                                        placeholder="ОГРН организации"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col9">
                                    <FieldComponent
                                        title="Юридический адрес"
                                        name="entity_address"
                                        value={item.entity_address}
                                        placeholder="Город, улица, дом, корпус, строение.."
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col3">
                                    <FieldComponent
                                        title="Дата регистрации"
                                        name="registration_date"
                                        value={item.registration_date}
                                        placeholder="дд мммм гггг"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col7">
                                    <FieldComponent
                                        title="ФИО руководителя"
                                        name="entity_boss_name"
                                        value={item.entity_boss_name}
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col5">
                                    <FieldComponent
                                        title="Должность руководителя"
                                        name="entity_boss_position"
                                        value={item.entity_boss_position}
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="БИК"
                                        name="BIK"
                                        value={item.BIK}
                                        placeholder="БИК банка"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Корсчет"
                                        name="company_account"
                                        value={item.company_account}
                                        placeholder="Корсчет банка"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col12">
                                    <FieldComponent
                                        title="Наименование банка"
                                        name="bank_name"
                                        value={item.bank_name}
                                        placeholder="Наименование банка"
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                        </SectionComponent>
                        <SectionComponent>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__title">Почтовый адрес</div>
                                <div className="formRow__item --col12">
                                    <FieldDadataSelectComponent
                                        type="address"
                                        onSelect={this.addressAutocomplete}
                                        placeholder="Введите точный адрес"
                                        value={address}
                                    />
                                </div>
                                <hr/>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Индекс"
                                        name="post_index"
                                        value={item.post_index}
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Регион\ район"
                                        name="region"
                                        value={item.region}
                                        onChange={this.changeField}
                                    />
                                </div>
                                {cities &&
                                    <div className="formRow__item --col6">
                                        <FieldSelectcomponent
                                            title="Город"
                                            name="city_id"
                                            options={this.citiesCollection}
                                            onChange={this.selectSelect}
                                            value={item.city_id}
                                        />
                                    </div>
                                }
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Улица"
                                        name="post_street"
                                        value={item.post_street}
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Дом\ Строение\ Корпус"
                                        name="post_building"
                                        value={item.post_building}
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        title="Квартира\ Помещение\ Офис"
                                        name="post_building_number"
                                        value={item.post_building_number}
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                        </SectionComponent>
                    </div>
                : null }
            </Loader>
        )
    }
}

const mapStateToProps = state => ({
    club: state.clubState.club
})

export default connect(mapStateToProps)(ClubEditComponent)
