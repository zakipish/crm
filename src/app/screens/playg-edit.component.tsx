import * as React from 'react'
import { match } from 'react-router'
import {history} from "../core/history"

import Sport from '../models/sport'
import Playground from '../models/playground'
import { reqGet, reqPatch, reqPost } from '../core/api'
import { ITree } from '../components/blocks/tags.component'

import {DataSportsTree, DataSportsList} from '../_data/sports.data'
import { onChangeSelected } from '../components/blocks/tree/tree.util'

import EditPageComponent, { IEditPageState } from '../components/_shared/edit-page.component'
import PageHeaderComponent from '../components/layout/page-header.component'
import Loader from '../components/layout/loader'
import SectionComponent from '../components/layout/section.component'

import UploadComponent from '../components/forms/upload.component'
import { IconList } from '../components/blocks/icon/icon.component'
import FieldSelectcomponent from '../components/forms/field-select.component'
import FieldComponent from '../components/forms/field.component'
import RitchTextareaComponent from '../components/forms/ritch-field.component'
import GalleryComponent from '../components/blocks/gallery/gallery.component'
import TreeSelectcomponent from '../components/blocks/tree/tree-select.component'
import TagsComponent from '../components/blocks/tags.component'
import VerifiField from '../components/forms/verifi.field'


interface IProps {
    history: History
    match: match<any>
}

class PlaygroundEditComponent extends EditPageComponent<IProps, IEditPageState> {

    apiurl = 'playgrounds'

    constructor(props) {
        super(props)

        this.state = {
            item: new Playground(),
            loading: true,
            services: [],
            equipments:[],
            options: {},
            min_time_inteval:false
        }
    }

    componentDidMount() {
        window.scrollTo(0,0)
        this.bootstrap()
    }

    bootstrap = () => {
        this.getDate()

        this.getOptions('surfaces', '/surfaces')
        this.getOptions('services', '/services')
        this.getOptions('sports', '/sports')
        this.getOptions('equipments', '/equipments')
    }

    getDate = async () => {
        let { id } = this.props.match.params
        this.setState({ loading: true })
        let item = new Playground()
        item.sport_ids = []
        item.min_interval = null
        if (id !== 'new') {
            let res = await reqGet(`/${this.apiurl}/${id}`)
            item = res.data
        }
        this.setState({ item, loading: false })
    }

    getOptions = (name: string, url: string) => {
        reqGet(url)
            .then(response => {
                let { options } = this.state
                options[name] = response.data
                this.setState({ options })
            })
    }

    getNameByOptionId = (name: string, id: number): string => {
        const { options } = this.state
        const option = options[name].find(o => o.id == id)
        return option ? option.name : null
    }

    getOptionsCollection = (name: string) => {
        const { options } = this.state
        return options[name].reduce((acc, option) => {
            acc[option.id] = option.name
            return acc
        }, {})
    }

    changeOptionField = (option: string, optionInd: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        let {item} = this.state
        const {name, value} = e.target
        item[option][optionInd][name] = value
        this.setState({item})
    }

    changeOptionFieldNumber = (option: string, optionInd: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        //получить стейт
        let {item} = this.state
        const {name, value} = e.target
        item[option][optionInd][name] = parseInt(value)
        this.setState({item})
    }

    createOption = (name: string, value: string) => {
        let { item } = this.state
        item[name].push({ id: null, name: value })
        this.setState({ item })
    }

    addOption = (name: string, id: number) => {
        let { item } = this.state
        const { options } = this.state
        const option = options[name].find(option => option.id == id)
        console.log({id, option, name, item})
        if (option) {
            item[name].push(option)
            this.setState({ item })
        }
    }

    deleteOption = (option: string, optionInd: number) => () => {
        let {item} = this.state
        item[option].splice(optionInd,1)
        this.setState({item})
    }

    saveData = async () => {
        let { id, clubId } = this.props.match.params
        let { item,min_time_inteval } = this.state
        this.setState({ loading: true })
        if(min_time_inteval){
            item.isMinIntervalSetted = true
            this.setState({item})
        }
        let res
        if (id !== 'new') {
            res = await reqPatch(`/${this.apiurl}/${id}`, JSON.stringify(item))
            this.setState({ item: res.data, loading: false })
        } else {
            res = await reqPost(`/clubs/${clubId}/${this.apiurl}`, JSON.stringify(item))
        }
        history.push(`/club/${res.data.club_id}`)
    }

    onChangeSport = (obj) => {
        let { item } = this.state
        item.sport_ids = onChangeSelected(item.sport_ids, obj)
        this.setState({ item })
    }

    get sportsCollection() {
        const { sports } = this.state.options
        let itemsByLabel = {}
        let rootItem = new ITree(null)
        for (let sport of sports) {
            var item = new ITree(sport)

            if (itemsByLabel.hasOwnProperty(sport.label)) {
                if (Array.isArray(itemsByLabel[sport.label])) {
                    item.items = itemsByLabel[sport.label]
                } else {
                    throw new Error()
                }
            }

            itemsByLabel[sport.label] = item

            var arrayLabel = sport.label.split('.')
            if (arrayLabel.length === 1) {
                rootItem.items.push(item)
            } else {
                var parentLabel = arrayLabel.slice(0, arrayLabel.length - 1).join('.')
                if (itemsByLabel.hasOwnProperty(parentLabel)) {
                    if (Array.isArray(itemsByLabel[parentLabel])) {
                        itemsByLabel[parentLabel].push(item)
                    } else {
                        itemsByLabel[parentLabel].items.push(item)
                    }
                } else {
                    itemsByLabel[parentLabel] = [item]
                }
            }
        }
        return rootItem.items
    }

    get imagesCollection() {
        const { item } = this.state
        if (!item.images) return []
        return item.images.map(image => ({
            order: image.order,
            url: image.url,
            id: image.id
        })).sort((a,b)=>a.order-b.order)
    }

    render() {
        let { item, loading, options,min_time_inteval } = this.state
        let { id, clubId } = this.props.match.params

        const sports = options ? options.sports : null
        const services = options ? options.services : null
        const equipments = options ? options.equipments : null
        const surfaces = options ? options.surfaces : null

        loading = loading ? loading : !item ? true : false
        return (
            <Loader state={loading}>
                { !loading ?
                    <div className="wrap">
                        <PageHeaderComponent
                            title="Карточка зала"
                            buttons={[
                                { label: 'Отмена', url: `/club/${clubId}` },
                                { label: 'Сохранить', url: `/club/${clubId}`, onClick: this.saveData, bg: 'blue' },
                            ]}
                        >
                            <VerifiField state={min_time_inteval || item.isMinIntervalSetted}>
                                {item.isMinIntervalSetted ? Playground.min_intervals[item.min_interval]
                                    :<FieldSelectcomponent
                                        title="Элемент бронирования"
                                        name="min_interval"
                                        value={item.min_interval + ''}
                                        options={Playground.min_intervals}
                                        onChange={this.selectSelect}
                                    />}
                            </VerifiField>
                        </PageHeaderComponent>
                        <SectionComponent>
                            <div className="formRow">
                                {id !== 'new' && (
                                    <div className="formRow__item --auto">
                                        <UploadComponent
                                            id="up1"
                                            title="Загрузить лого"
                                            src={item.avatar_url}
                                            logo={item.avatar}
                                            multiple={true}
                                            onUpload={(arrFiles, files) => this.onUploadImage(`/${this.apiurl}/${item.id}`, 'avatar', arrFiles, files)}
                                            onDelete={this.asyncDeleteImage(`/${this.apiurl}/${item.id}`, 'avatar')}
                                            size={{width: 90, height: 90}}
                                        />
                                    </div>
                                )}
                                <div className="formRow__item">
                                    <FieldComponent
                                        title="Название зала"
                                        name="name"
                                        value={item.name}
                                        placeholder="Введите название зала"
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                            <div className="formRow">
                                <div className="formRow__item">
                                    <RitchTextareaComponent
                                        title="Описание зала"
                                        name="description"
                                        defaultValue={item.description}
                                        onChange={this.changeField}
                                    />
                                </div>
                            </div>
                            <div className="formRow formRow--wrap">
                                <div className="formRow__title">Характеристика зала</div>
                                <div className="formRow__item --col3">
                                    <FieldSelectcomponent
                                        title="Тип зала"
                                        name="type"
                                        value={item.type + ''}
                                        options={Playground.types}
                                        onChange={this.selectSelect}
                                    />
                                </div>
                            </div>
                            { surfaces ?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__item --col12">
                                        <FieldSelectcomponent
                                            title="Покрытие"
                                            name="surfaces"
                                            placeholder="Добавьте свои варианты или выберите из списка..."
                                            options={this.getOptionsCollection('surfaces')}
                                            onChange={this.addOption}
                                            onClickEmpty={this.createOption}
                                            emptyTitle={'Создать'}
                                            disableKeysSelect={true}
                                        />
                                    </div>
                                    <div className="formGrid">
                                        {item.surfaces && item.surfaces.map((surface, i) => (
                                            <React.Fragment key={i}>
                                                <FieldComponent
                                                    value={surface.name}
                                                    onChange={() => {}}
                                                    viewOnly={true}
                                                    icons={[{ icon: IconList.del, bg: 'red', onClick: this.deleteOption('surfaces', i) }]}
                                                />
                                            </React.Fragment>
                                        ))}
                                    </div>
                                </div>
                            : '' }
                            { sports ?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__title">
                                        <span>Виды спорта</span>
                                        <ul className="levelList">
                                            <li className="--orange">1 уровень</li>
                                            <li className="--green">2 уровень</li>
                                            <li className="--yellow">3 уровень</li>
                                            <li className="--blue">4 уровень</li>
                                        </ul>
                                    </div>
                                    <div className="formRow__item --col12">
                                        <TreeSelectcomponent
                                            name="sportFilter"
                                            placeholder="Поиск по дереву"
                                            listTree={this.sportsCollection}
                                            selected={item.sport_ids}
                                            onSelect={this.onChangeSport}
                                        />
                                    </div>
                                    <div className="formRow__item --col12">
                                        <TagsComponent
                                            listTree={this.sportsCollection}
                                            selected={item.sport_ids}
                                            onRemove={this.onChangeSport}
                                        />
                                    </div>
                                </div>
                            : '' }
                            {services?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__title">Услуги зала</div>
                                    <div className="formRow__item --col12">
                                        <FieldSelectcomponent
                                            name="services"
                                            placeholder="Добавьте свои варианты или выберите из списка..."
                                            options={this.getOptionsCollection('services')}
                                            onChange={this.addOption}
                                            onClickEmpty={this.createOption}
                                            emptyTitle={'Создать'}
                                            disableKeysSelect={true}
                                        />
                                    </div>
                                    {item.services && item.services.map((service,i) =>
                                        <div className="formRow--wraper" key={i}>
                                            <div className="formRow__item --col6">
                                                <FieldComponent
                                                    value={service.name}
                                                    onChange={()=>null}
                                                />
                                            </div>
                                            <div className="formRow__item --col2">
                                                <FieldComponent
                                                    name="price"
                                                    value={service.price}
                                                    placeholder="999 999 руб"
                                                    icons={[{icon: IconList.rub}]}
                                                    onChange={this.changeOptionFieldNumber("services",i,)}
                                                />
                                            </div>
                                            <div className="formRow__item --col3">
                                                <FieldComponent
                                                    name="unit"
                                                    value={service.unit}
                                                    placeholder="За 1 час"
                                                    onChange={this.changeOptionField("services",i)}
                                                />
                                            </div>
                                            <div className="formRow__item --col1">
                                                <FieldComponent
                                                    icons={[{icon: IconList.del, bg: 'red',onClick:this.deleteOption("services", i)}]}
                                                />
                                            </div>

                                        </div>
                                    )}
                                </div>
                            : null}
                            {equipments ?
                                <div className="formRow formRow--wrap">
                                    <div className="formRow__title">Инвентарь</div>
                                    <div className="formRow__item --col12">
                                        <FieldSelectcomponent
                                            name="equipments"
                                            placeholder="Добавьте свои варианты или выберите из списка..."
                                            options={this.getOptionsCollection('equipments')}
                                            onChange={this.addOption}
                                            onClickEmpty={this.createOption}
                                            emptyTitle={'Создать'}
                                            disableKeysSelect={true}
                                        />
                                    </div>
                                    {item.equipments && item.equipments.map((equipment,i) =>
                                        <div className="formRow--wraper" key={i}>
                                            <div className="formRow__item --col6">
                                                <FieldComponent
                                                    value={equipment.name}
                                                    onChange={()=>null}
                                                />
                                            </div>
                                            <div className="formRow__item --col2">
                                                <FieldComponent
                                                    name="price"
                                                    value={equipment.price}
                                                    placeholder="999 999 руб"
                                                    icons={[{icon: IconList.rub}]}
                                                    onChange={this.changeOptionFieldNumber("equipments",i,)}
                                                />
                                            </div>
                                            <div className="formRow__item --col3">
                                                <FieldComponent
                                                    name="unit"
                                                    value={equipment.unit}
                                                    placeholder="За 1 час"
                                                    onChange={this.changeOptionField("equipments",i)}
                                                />
                                            </div>
                                            <div className="formRow__item --col1">
                                                <FieldComponent
                                                    icons={[{icon: IconList.del, bg: 'red',onClick:this.deleteOption("equipments",i)}]}
                                                />
                                            </div>

                                        </div>
                                    )}
                                </div>
                            : null }
                            {/* <div className="formRow formRow--wrap">
                                <div className="formRow__title">Инфраструктура</div>
                                <div className="formRow__item --col12">
                                    <FieldSelectcomponent
                                        name="infrVar"
                                        value={infrVar}
                                        placeholder="Добавьте свои варианты или выберите из списка..."
                                        options={{1: 'Вариант 1', 2: 'Вариант 2', 3: 'Вариант 3'}}
                                        onChange={this.selectSelect}
                                    />
                                </div>
                                <div className="formRow__item --col6">
                                    <FieldComponent
                                        name="addresPlayg"
                                        value={addresPlayg}
                                        placeholder="Парковка"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col2">
                                    <FieldComponent
                                        name="addresPlayg"
                                        value={addresPlayg}
                                        placeholder="999 999 руб"
                                        onChange={this.changeField}
                                    />
                                </div>
                                <div className="formRow__item --col3">
                                    <FieldSelectcomponent
                                        name="infrDur"
                                        value={infrDur}
                                        placeholder="Нет"
                                        options={{1: 'За время', 2: 'За един. товара', 3: 'Период', 4: 'Дни'}}
                                        onChange={this.selectSelect}
                                    />
                                </div>
                                <div className="formRow__item --col1">
                                    <FieldComponent
                                        icons={[{icon: IconList.del, bg: 'red'}]}
                                    />
                                </div>
                            </div> */}
                            {id !== 'new' && (
                                <GalleryComponent
                                    title="Фото зала"
                                    id="up2"
                                    url={`/playgrounds/${id}`}
                                    list={this.imagesCollection}
                                    onChange={(arrFiles) => this.onChangeGallery(arrFiles, 'images')}
                                    onAdd={this.loadFiles(`/playgrounds/${item.id}`)}
                                    onRemove={this.deleteUploadedImage(`/playgrounds/${item.id}`)}
                                />
                            )}
                        </SectionComponent>
                    </div>
                : null }
            </Loader>
        )
    }
}

export default PlaygroundEditComponent
