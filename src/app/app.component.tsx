import * as React from 'react'
import { connect } from 'react-redux'
import { Router as BaseRouter, Redirect, Switch, Route } from 'react-router-dom'
import { history } from '../app/core/history'

import * as AuthService from './services/auth.service'

import PlannerComponent from './screens/planner.component'
import PlaygDashboardComponent from './screens/playg-dashboard.component'
import PlaygroundEditComponent from './screens/playg-edit.component'
import ClubEditComponent from './screens/club-edit.component'
import ClubComponent from './screens/club.component'
import ClubsComponent from './screens/clubs.component'
import AuthComponent from './screens/auth.component'
import LayoutPage from './components/layout/page/layout.page'
import EventNewScreen from './screens/event/event-new.screen'
import EventEditScreen from './screens/event/event-edit.screen'
import EventProfileScreen from './screens/event/event-profile.screen'

interface IProps {
    user: any
}

interface IState {
    loading: boolean
}

class AppComponent extends React.Component<IProps, IState> {

    state = {
        loading: true
    }

    componentDidMount() {
        this.bootstrap()
    }

    bootstrap = async () => {
        await AuthService.initialLogin()
        this.setState({ loading: false })
    }

    render() {
        let { loading } = this.state
        let { user } = this.props
        return (
            <BaseRouter history={history}>
                { loading ? (
                    <span>Загрузка</span>
                ) : (
                    !user ?
                        <Switch>
                            <Route exact path="/" component={AuthComponent}/>
                            <Route path="*">
                                <Redirect to="/"/>
                            </Route>
                        </Switch>
                    :
                        <LayoutPage>
                            <Switch>
                                <Route path="/event/new" component={EventNewScreen}/>
                                <Route path="/event/:slug/edit" component={EventEditScreen}/>
                                <Route path="/event/:slug" component={EventProfileScreen}/>
                                <Route path="/playground/planner/:id" component={PlannerComponent}/>
                                <Route path="/playground/dashbord/:id" component={PlaygDashboardComponent}/>
                                <Route path="/playground/edit/:id" component={PlaygroundEditComponent}/>
                                <Route path="/club/:clubId/playground/:id" component={PlaygroundEditComponent}/>
                                <Route path="/club/:clubId/edit" component={ClubEditComponent}/>
                                <Route path="/club/:clubId" component={ClubComponent}/>
                                <Route path="/clubs" component={ClubComponent}/>
                                <Route exact path="/" component={ClubsComponent}/>
                                <Route path="*">
                                    <Redirect to="/"/>
                                </Route>
                            </Switch>
                        </LayoutPage>
                )}
            </BaseRouter>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user,
})

export default connect(mapStateToProps)(AppComponent)
