import { AssignableObject } from '../core/utils'

class Image extends AssignableObject {
    id: number
    url: string
    default?: string
    order?: number
}

export default Image
