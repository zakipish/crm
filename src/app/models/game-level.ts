import { AssignableObject } from '../core/utils';

export default class GameLevel extends AssignableObject {
    id: number
    slug: string
    name: string
    engName: string
    color: string
}
