import { AssignableObject } from '../core/utils'
import moment from 'moment'

class Game extends AssignableObject {
    id: number
    name: string
    description: string
    kipish_description: string
    date: string
    interval: string[]
    is_kipish: boolean
    owner: any
    price: string
    owner_id: number
    sport_id: number
    viewState?: any
    duration?: string

    errors: any

    get fromStr() {
        return `${this.date} ${this.interval[0]}`
    }

    get toStr() {
        return `${this.date} ${this.interval[1]}`
    }

    get fromDate() {
        return moment(this.fromStr)
    }

    get toDate() {
        return moment(this.toStr)
    }
}

export default Game
