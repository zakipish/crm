import { AssignableObject } from '../core/utils'
import Price from './price'

class DatePrice extends AssignableObject {
    date: string
    prices: Price[]
}

export default DatePrice
