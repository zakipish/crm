import { AssignableObject } from '../core/utils'
import Club from './club'
import Playground from './playground'
import Sport from './sport'
import GameLevel from './game-level'
import User, { EventParticipationType } from './user'
import Image from './image'
import moment from 'moment-timezone'

export enum EventType {
    Meeting = 1,
    Game
}

export default class Event extends AssignableObject {
    id: number
    slug: string
    owner_id: number
    club_id: number
    club: Club
    playground_id: number
    playground: Playground
    sport_id: number
    sport: Sport
    game_level_id: number
    gameLevel: GameLevel
    chat_disabled: boolean
    chat_id: number
    users: User[]
    max_players: number
    min_players: number
    age_start: number
    age_end: number
    title: string
    description: string
    price: number
    type: EventType
    coordinates: number[]
    images: Image[]
    interval: string[]
    date: string
    blockSchedule: boolean
    updated_at: string
    address: string
    freeEnter: boolean

    constructor(args) {
        super(args)

        if(!args.price)
            this.price = 0

        if(!args.chat_disabled)
            this.chat_disabled = false

        if (!(args.club instanceof Club)) {
            this.club = new Club(args.club)
        }

        if (args.playground && !(args.playground instanceof Playground)) {
            this.playground = new Playground(args.playground)
        }

        if (args.users && args.users.length > 0 && !(args.users[0] instanceof User)) {
            this.users = args.users.map(user => new User(user))
        }
    }

    get freeEnterCheckbox() {
        return this.freeEnter == null ? this.price == 0 : this.freeEnter
    }

    set freeEnterCheckbox(value: boolean) {
        this.freeEnter = value
        if (value)
            this.price = 0
    }

    get intervalStartMoment(): moment.Moment {
        if (!this.interval)
            return null

        return moment.tz(this.date + ' ' + this.interval[0], 'Europe/Moscow')
    }

    get intervalEndMoment(): moment.Moment {
        if (!this.interval)
            return null

        return moment.tz(this.date + ' ' + this.interval[1], 'Europe/Moscow')
    }

    get intervalStartHours(): string {
        let interval = []

        if (this.interval && this.interval[0]) {
            interval = this.interval[0].split(':')
        }

        return interval[0]
    }

    get intervalStartMinutes(): string {
        let interval = []

        if (this.interval && this.interval[0]) {
            interval = this.interval[0].split(':')
        }

        return interval[1]
    }

    get intervalEndHours(): string {
        let interval = []

        if (this.interval && this.interval[1]) {
            interval = this.interval[1].split(':')
        }

        return interval[0]
    }

    get intervalEndMinutes(): string {
        let interval = []

        if (this.interval && this.interval[1]) {
            interval = this.interval[1].split(':')
        }

        return interval[1]
    }

    set intervalStartHours(hours: string) {
        let interval = []

        if (this.interval && this.interval[0]) {
            interval = this.interval[0].split(':')
        }

        if (interval.length < 1)
            interval = ['00', '00']

        interval[0] = hours

        if (!this.interval)
            this.interval = []

        this.interval[0] = `${interval[0]}:${interval[1]}`
    }

    set intervalStartMinutes(minutes: string) {
        let interval = []

        if (this.interval && this.interval[0]) {
            interval = this.interval[0].split(':')
        }

        interval[1] = minutes

        if (!interval[0])
            interval[0] = '00'

        if (!this.interval)
            this.interval = []

        this.interval[0] = `${interval[0]}:${interval[1]}`
    }

    set intervalEndHours(hours: string) {
        let interval = ['00', '00']

        if (this.interval && this.interval[1]) {
            interval = this.interval[1].split(':')
        }

        interval[0] = hours

        if (!interval[1])
            interval[1] = '00'

        if (!this.interval)
            this.interval = []

        if (this.interval.length < 1)
            this.interval[0] = '00:00'

        this.interval[1] = `${interval[0]}:${interval[1]}`
    }

    set intervalEndMinutes(minutes: string) {
        let interval = []

        if (this.interval && this.interval[1]) {
            interval = this.interval[1].split(':')
        }

        interval[1] = minutes

        if (!interval[0])
            interval[0] = '00'

        if (!this.interval)
            this.interval = []

        if (this.interval.length < 1)
            this.interval[0] = '00:00'

        this.interval[1] = `${interval[0]}:${interval[1]}`
    }

    get age_start_str(): string {
        return '' + this.age_start
    }

    get age_end_str(): string {
        return '' + this.age_end
    }

    set age_start_str(value: string) {
        this.age_start = +value
    }

    set age_end_str(value: string) {
        this.age_end = +value
    }

    get isPast() {
        return this.intervalStartMoment.isBefore(moment())
    }

    get possibleUsers() {
        return this.users && this.users.filter(user => user.type == EventParticipationType.Possible) || []
    }

    get defenitelyUsers() {
        return this.users && this.users.filter(user => user.type == EventParticipationType.Defenitely) || []
    }

    get declinedUsers() {
        return this.users && this.users.filter(user => user.type == EventParticipationType.Declined) || []
    }
}
