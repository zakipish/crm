import { AssignableObject } from '../core/utils'
import User from './user'

class ClubChain extends AssignableObject {
    id: number
    slug: string
    name: string
    owner: User
}

export default ClubChain
