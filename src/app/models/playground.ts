import { AssignableObject } from '../core/utils'
import Club from './club'
import Surface from './surface'
import Service from './service'
import Sport from './sport'
import Equipment from './equipment'
import Image from './image'

class Playground extends AssignableObject {
    id: number = null
    position: number = null
    name: string = ''
    type: number = null
    description: string = ''
    avatar: Image = null
    avatar_url: string = null
    surfaces: Surface[] = []
    surface_ids: number[] = []
    services: Service[] = []
    images: Image[] = []
    sports: Sport[] = []
    sport_ids: number[] = []
    equipments: Equipment[] = []
    min_interval: number = null
    updated_at: string = ''
    club: Club = null

    static types = {
        1: 'Открытая',
        2: 'Закрытая',
    }

    static min_intervals = {
        15: '15 минут',
        30: '30 минут',
        45: '45 минут',
        60: '1 час',
    }

    get typeName() {
        return Playground.types[this.type]

    }
}

export default Playground
