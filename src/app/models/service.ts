import {AssignableObject} from '../core/utils'

class Service extends AssignableObject {
    service_id: number
    name: string
    payable: boolean
    price: number
    unit: string
}

export default Service
