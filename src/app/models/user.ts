import { AssignableObject } from '../core/utils'

export enum EventParticipationType {
    Possible = 1,
    Defenitely,
    Declined
}

class User extends AssignableObject {
    id: number
    email: string
    name: string
    lastName: string
    midName: string
    role: number
    moderated_clubs: any[]
    phone: string
    type: EventParticipationType

    get fullName() {
        let name = this.name
        if (this.lastName != null) name = `${this.lastName} ${name}`
        if (this.midName != null) name = `${name} ${this.midName}`
        return name
    }
}

export default User
