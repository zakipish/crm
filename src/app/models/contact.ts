import { AssignableObject } from '../core/utils'
import ContactType from './contact-type'

class Contact extends AssignableObject {
    id: number
    value: string
    contact_type_id: number
    contact_type: ContactType
}

export default Contact
