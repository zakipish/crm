import { AssignableObject } from '../core/utils'

class Price extends AssignableObject {
    timeStart?: string
    timeEnd?: string
    state?: string // [open, closed]
    value: number
}

export default Price
