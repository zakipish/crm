import { AssignableObject } from '../core/utils'

class Sport extends AssignableObject {
    id: number
    name: string
    label: string
    description: string
    game_level: number
    game_exp: number
}

export default Sport
