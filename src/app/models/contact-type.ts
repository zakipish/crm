import { AssignableObject } from '../core/utils'

export default class ContactType extends AssignableObject {
    id: number
    slug: string
    name: string
}

export const ContactSocTypes = [
    'mailru',
    'odnoklassniki',
    'facebook',
    'instagram',
    'vkontakte',
]

export const ContactSocShorts = {
    [ContactSocTypes[0]]: 'Mail.ru',
    [ContactSocTypes[1]]: 'Ok',
    [ContactSocTypes[2]]: 'Fb',
    [ContactSocTypes[3]]: 'In',
    [ContactSocTypes[4]]: 'Vk',
}

export const ContactSocDomain = {
    [ContactSocTypes[0]]: ['mail.ru'],
    [ContactSocTypes[1]]: ['ok.ru'],
    [ContactSocTypes[2]]: ['facebook.com'],
    [ContactSocTypes[3]]: ['instagram.com'],
    [ContactSocTypes[4]]: ['vk.com'],
}
