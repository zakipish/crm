import { AssignableObject } from '../core/utils';

class Surface extends AssignableObject {
    id: number
    name: string
}

export default Surface
