import { AssignableObject } from '../core/utils'

class Equipment extends AssignableObject {
    id: number
    equipment_id: number
    name: string

    // pivot
    unit: string
    price: number
}

export default Equipment
