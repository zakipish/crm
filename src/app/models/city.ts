import { AssignableObject } from '../core/utils'

class City extends AssignableObject {
    id: number
    slug: string
    name: string
}

export default City
