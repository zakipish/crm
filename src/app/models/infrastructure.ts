import { AssignableObject } from '../core/utils'

class Infrastructure extends AssignableObject {
    id: number
    name: string
}

export default Infrastructure
