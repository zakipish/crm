import { AssignableObject } from '../core/utils'
import Sport from './sport'
import ClubChain from './club-chain'
import Image from './image'
import Contact from './contact'
import Infrastructure from './infrastructure'
import Playground from './playground'

class Club extends AssignableObject {
    id: number
    name: string
    updated_at: string
    club_chain: ClubChain
    logo: Image
    description: string
    address: string
    region: string
    city_id: number
    images: Image[]
    contacts: Contact[]
    infrastructures: Infrastructure[]
    infrastructure_ids: number[]
    coordinates: string
    company_name: string
    OGRN: string
    INN: string
    KPP: string
    entity_address: string
    sports: Sport[]

    post_index: string
    post_street: string
    post_building: string
    post_building_number: string

    entity_boss_name: string
    entity_boss_position: string
    registration_date: string
    registration_certificate: string
    bank_name: string
    BIK: string
    company_account: string
    number_rs: string
    NDS: number

    playgrounds: Playground[]

    static ndsOptions = {
        null: 'Без НДС',
        0: '0%',
        1: '10%',
        2: '18%',
        3: '20%'
    }

    get ndsName() {
        return Club.ndsOptions[this.NDS]
    }

    get coordinatesArr(): number[] {
        if (this.coordinates) {
            const result = this.coordinates.match(/\(([\d,\.]*),([\d,\.]*)\)/i)

            if (result)
                return [parseFloat(result[1]), parseFloat(result[2])]

            return null
        }

        return null
    }
}

export default Club
