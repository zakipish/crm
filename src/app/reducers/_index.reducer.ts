import { combineReducers } from 'redux'

// Reducers
import authReducer from './auth.reducer'
import plannerReducer from './planner.reducer'
import plannerSelectReducer from './planner-select.reducer'
import clubReducer from './club.reducer'
import eventReducer from './event.reducer'

const appReducer = combineReducers({
    authState: authReducer,
    plannerState: plannerReducer,
    plannerSelectState: plannerSelectReducer,
    clubState: clubReducer,
    eventState: eventReducer,
})

const rootReducer = (state, action) => {
    // AUTH CLEAN
    return appReducer(state, action)
}

export default rootReducer
