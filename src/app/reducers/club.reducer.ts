import types from '../actions/_types.actions'
import Club from '../models/club'

const initialState = {
    club: null
}

const clubReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CLUB_GET:
            return {
                ...state,
                club: new Club(action.payload)
            }
        default:
            return state
    }
}

export default clubReducer
