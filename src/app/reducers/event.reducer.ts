import types from '../actions/_types.actions'

const initialState = {
    events: null
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.EVENTS_LOAD:
            return {
                ...state,
                events: action.payload
            }
        default:
            return state
    }
}

export default authReducer
