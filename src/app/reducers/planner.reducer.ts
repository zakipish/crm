import moment from 'moment'

import types from '../actions/_types.actions'
import DatePrice from '../models/datePrice'
import Game from '../models/game'

export interface PlannerState {
    date: moment.Moment
    layerKey: PlannerLayers | string
    view: PlannerView
    grid: PlannerGrid
    prices: DatePrice[]
    games: Game[]
    priceLimit: PlannerPriceLimit
    colors: PlannerPriceColor[]
    showSettingDay: number
    [x: string]: any
}

export interface PlannerGrid {
    timeStart: string
    timeEnd: string
    durationMin: number
    lineLen: number
    rowLen: number
    cellWidth: number
    cellHeight: number
}

export interface PlannerView {
    hideCellPrice: boolean
}

export interface PlannerPriceColor {
    color: string
    percent: number
}

export interface PlannerPriceLimit {
    from: number
    to: number
}

export enum PlannerLayers {
    price = 'price',
    game = 'game',
}

const initialState: PlannerState = {
    reqParam: null,
    date: null,
    layerKey: PlannerLayers.price,
    view: {
        hideCellPrice: false
    },
    grid: {
        timeStart: '00:00',
        timeEnd: '24:00',
        durationMin: 60,
        lineLen: 0,
        rowLen: 7,
        cellWidth: 0,
        cellHeight: 0,
    },
    prices: [],
    games: [],
    priceLimit: {
        from: 300,
        to: 900,
    },
    colors: [
        { color: 'rgba(255, 87, 34, 0.3)', percent: 0.00 },
        { color: 'rgba(255, 193, 7, 0.3)', percent: 8.29 },
        { color: 'rgba(255, 235, 59, 0.3)', percent: 19.34 },
        { color: 'rgba(205, 220, 57, 0.3)', percent: 27.62 },
        { color: 'rgba(76, 175, 80, 0.3)', percent: 36.46 },
        { color: 'rgba(0, 150, 136, 0.3)', percent: 45.30 },
        { color: 'rgba(0, 188, 212, 0.3)', percent: 55.25 },
        { color: 'rgba(44, 152, 240, 0.3)', percent: 66.30 },
        { color: 'rgba(64, 84, 178, 0.3)', percent: 74.03 },
        { color: 'rgba(103, 58, 183, 0.3)', percent: 82.87 },
        { color: 'rgba(145, 43, 178, 0.3)', percent: 93.36 },
        { color: 'rgba(156, 39, 176, 0.3)', percent: 93.37 },
        { color: 'rgba(233, 30, 99, 0.3)', percent: 100.00 },
    ],
    showSettingDay: -1
}


const plannerReducer = (state: PlannerState = initialState, action) => {
    let { playload } = action
    switch (action.type) {
        case types.PLANNER_SET_REQ_PARAM:
            return { ...state, reqParam: {...playload.reqParam} }

        case types.PLANNER_SET_DATE:
            return { ...state, date: moment(playload.date) }

        case types.PLANNER_SET_VIEW:
            return { ...state, view: {...playload.view} }

        case types.PLANNER_SET_LAYER:
            return { ...state, layerKey: playload.layerKey }

        case types.PLANNER_SET_GRID:
            return { ...state, grid: {...playload.grid} }

        case types.PLANNER_SET_PRICES:
            return {
                ...state, 
                prices: [...playload.prices]
            }

        case types.PLANNER_SET_GAMES:
            return {
                ...state, 
                games: [...playload.games]
            }

        case types.PLANNER_SET_SHOW_SETTING_DAY:
            return {
                ...state, 
                showSettingDay: playload.showSettingDay
            }
        
        case types.PLANNER_SET_PRICELIMIT:
            return { ...state, priceLimit: {...playload.priceLimit} }

        default: return state
    }
}

export default plannerReducer
