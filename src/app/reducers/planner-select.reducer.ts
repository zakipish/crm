import types from '../actions/_types.actions'
import Game from '../models/game'

export interface PlannerSelectState {
    list: PlannerSelectDay[],
    game?: Game
    endCrood?: {
        x: number
        y: number
    }
    crood?: {
        x1: number
        x2: number
        y1: number
        y2: number
    }, 
    endMoveState: boolean
    hide: boolean
    selectPrices: Array<string | number>
}

export interface PlannerSelectDay {
    date: string
    durations: Array<{
        from?: string
        to?: string
        price?: number
    }>
}

const initialState = {
    list: [],
    endMoveState: false,
    hide: true,
    price: 0,
    selectPrices: []
}

const plannerSelectReducer = (state: PlannerSelectState = initialState, action) => {
    let { playload } = action
    switch (action.type) {
        case types.PLANNER_SELECT_SHOW:
            return {
                ...state, 
                hide: false,
                // endMoveState: false,
                // endCrood: null,
            }

        case types.PLANNER_SELECT_HIDE:
            return {
                ...state, 
                hide: true,
                endMoveState: false,
                endCrood: null,
                game: null,
            }

        case types.PLANNER_SELECT_STARTMOVE:
            return {
                ...state, 
                endMoveState: false,
                endCrood: null,
            }

        case types.PLANNER_SELECT_ENDMOVE:
            return {
                ...state, 
                endMoveState: true,
                endCrood: {...playload.endCrood}
            }

        case types.PLANNER_SELECT_SET_CROOD:
            return {
                ...state, 
                crood: {...playload.crood}
            }

        case types.PLANNER_SELECT_SET_LIST:
            return {
                ...state, 
                list: [...playload.list]
            }

        case types.PLANNER_SELECT_GAME:
            return {
                ...state, 
                game: playload.game
            }

        default: return state
    }
}

export default plannerSelectReducer
