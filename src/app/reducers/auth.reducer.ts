import types from '../actions/_types.actions'
import User from '../models/user'

const initialState = {
    user: null
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.AUTH_LOGIN:
            return {
                ...state,
                user: new User(action.payload)
            }
        case types.AUTH_CLEAR:
            return {
                ...state,
                user: null
            }
        default:
            return state
    }
}

export default authReducer
