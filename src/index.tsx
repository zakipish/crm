import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from './store'

import './style.sass'
import AppComponent from './app/app.component'

import moment from 'moment'
import momentTimezone from 'moment-timezone'
moment.locale('ru')
momentTimezone.locale('ru')

ReactDOM.render(
    <Provider store={store}>
        <AppComponent/>
    </Provider>,
    document.getElementById("root")
);
