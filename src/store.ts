import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './app/reducers/_index.reducer'

import types from './app/actions/_types.actions'

let lastActionType = ''
const logerSpamFilter = [
    types.PLANNER_SELECT_SET_CROOD,
]

const getStateLogger = (type) => {
    let prodState = process.env.NODE_ENV === 'production'
    let possibleSpam = logerSpamFilter.includes(type)
    let spamState = possibleSpam ? type === lastActionType : false
    let logState = prodState || spamState ? false : true
    lastActionType = type
    if (!prodState && spamState) console.log('spam', type)
    return logState
}

const logger = store => next => action => {
    let state = getStateLogger(action.type)
    if (state) {
        console.groupCollapsed('dispatching', action.type)
        console.log('prev state', store.getState())
        console.log('action', action)
    }
    let result = next(action)
    if (state) {
        console.log('next state', store.getState())
        console.groupEnd()
    }
    return result
}

const store = createStore(
    rootReducer,
    applyMiddleware(
        thunk, 
        // logger,
    )
)

export default store